--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actividad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE actividad (
    id_actividad integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    nombre_a text NOT NULL,
    descripcion text NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    hora_inicio character varying(12) NOT NULL,
    hora_fin character varying(12) NOT NULL,
    estatus character varying(20) NOT NULL,
    fecha_reg character varying(25),
    fecha_act character varying(25)
);


ALTER TABLE public.actividad OWNER TO postgres;

--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE actividad_id_actividad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.actividad_id_actividad_seq OWNER TO postgres;

--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE actividad_id_actividad_seq OWNED BY actividad.id_actividad;


--
-- Name: area; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE area (
    id_area integer NOT NULL,
    codigo character varying(10) NOT NULL,
    area text NOT NULL
);


ALTER TABLE public.area OWNER TO postgres;

--
-- Name: autor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE autor (
    id_libro integer NOT NULL,
    autor1 text NOT NULL,
    autor2 text,
    autor3 text
);


ALTER TABLE public.autor OWNER TO postgres;

--
-- Name: bibliotecario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bibliotecario (
    id_bibliotecario integer NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    cedula integer NOT NULL,
    tipo character varying(20) NOT NULL,
    direccion text NOT NULL,
    telefono character varying(12) NOT NULL,
    sexo character varying(20) NOT NULL,
    cuenta character varying(20) NOT NULL,
    clave character varying(40) NOT NULL,
    estatus character varying(10) NOT NULL,
    fecha date
);


ALTER TABLE public.bibliotecario OWNER TO postgres;

--
-- Name: bibliotecario_id_bibliotecario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bibliotecario_id_bibliotecario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bibliotecario_id_bibliotecario_seq OWNER TO postgres;

--
-- Name: bibliotecario_id_bibliotecario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bibliotecario_id_bibliotecario_seq OWNED BY bibliotecario.id_bibliotecario;


--
-- Name: biografias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE biografias (
    id_biografia integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    titulo text NOT NULL,
    autor text NOT NULL,
    contenido text NOT NULL,
    foto character varying(200) NOT NULL,
    fecha date
);


ALTER TABLE public.biografias OWNER TO postgres;

--
-- Name: editorial; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE editorial (
    id_editorial integer NOT NULL,
    nombre text NOT NULL,
    ciudad character varying(30) NOT NULL,
    pais character varying(30) NOT NULL
);


ALTER TABLE public.editorial OWNER TO postgres;

--
-- Name: editorial_id_editorial_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE editorial_id_editorial_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.editorial_id_editorial_seq OWNER TO postgres;

--
-- Name: editorial_id_editorial_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE editorial_id_editorial_seq OWNED BY editorial.id_editorial;


--
-- Name: id_seccion_id_seccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_seccion_id_seccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_seccion_id_seccion_seq OWNER TO postgres;

--
-- Name: id_seccion_id_seccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE id_seccion_id_seccion_seq OWNED BY area.id_area;


--
-- Name: inmueble; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE inmueble (
    id_inmueble integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    nombre_i character varying(20) NOT NULL,
    fecha date NOT NULL,
    cantidad_activo integer NOT NULL,
    cantidad_inactivo character varying(10),
    fecha_reg date
);


ALTER TABLE public.inmueble OWNER TO postgres;

--
-- Name: inmueble_id_inmueble_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inmueble_id_inmueble_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inmueble_id_inmueble_seq OWNER TO postgres;

--
-- Name: inmueble_id_inmueble_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE inmueble_id_inmueble_seq OWNED BY inmueble.id_inmueble;


--
-- Name: libro; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE libro (
    id_libro integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    isbn character varying(20) NOT NULL,
    titulo character varying(30) NOT NULL,
    cota character varying(30) NOT NULL,
    ejemplares integer NOT NULL,
    edicion character varying(30) NOT NULL,
    pag_vol character varying(30) NOT NULL,
    anio character varying(10) NOT NULL,
    observaciones text NOT NULL,
    estatus character varying(15) NOT NULL,
    sala character varying(15) NOT NULL,
    id_area integer NOT NULL,
    id_editorial integer NOT NULL,
    cant_inactivos character varying(4),
    fecha date
);


ALTER TABLE public.libro OWNER TO postgres;

--
-- Name: prestamo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE prestamo (
    id_prestamo integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    id_usuario integer NOT NULL,
    tipo character varying(20) NOT NULL,
    fecha_prestamo date NOT NULL,
    fecha_entrega date NOT NULL
);


ALTER TABLE public.prestamo OWNER TO postgres;

--
-- Name: prestamo_id_prestamo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE prestamo_id_prestamo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.prestamo_id_prestamo_seq OWNER TO postgres;

--
-- Name: prestamo_id_prestamo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE prestamo_id_prestamo_seq OWNED BY prestamo.id_prestamo;


--
-- Name: prestamo_libro; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE prestamo_libro (
    id_prestamo_libro integer NOT NULL,
    id_prestamo integer NOT NULL,
    id_libro integer NOT NULL,
    prestamo_estatus character varying(30) NOT NULL,
    fecha_devolucion date
);


ALTER TABLE public.prestamo_libro OWNER TO postgres;

--
-- Name: sancion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sancion (
    id_sancion integer NOT NULL,
    id_prestamo integer NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    estatus text NOT NULL
);


ALTER TABLE public.sancion OWNER TO postgres;

--
-- Name: sancion_id_sancion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sancion_id_sancion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sancion_id_sancion_seq OWNER TO postgres;

--
-- Name: sancion_id_sancion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sancion_id_sancion_seq OWNED BY sancion.id_sancion;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id_usuario integer NOT NULL,
    id_bibliotecario integer NOT NULL,
    cedula integer NOT NULL,
    primer_nombre character varying(20) NOT NULL,
    segundo_nombre character varying(20) NOT NULL,
    primer_apellido character varying(20) NOT NULL,
    segundo_apellido character varying(20) NOT NULL,
    sexo character varying(20) NOT NULL,
    correo character varying(30) NOT NULL,
    ocupacion character varying(30) NOT NULL,
    tipo character varying(20) NOT NULL,
    programa character varying(20) NOT NULL,
    discapacidad character varying(50),
    fecha_nac date NOT NULL,
    referencia text NOT NULL,
    direccion1 text NOT NULL,
    direccion2 text,
    telefono1 character varying(12) NOT NULL,
    telefono2 character varying(12),
    fecha date,
    codigo_carnet character varying(20)
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_id_usuario_seq OWNED BY usuario.id_usuario;


--
-- Name: id_actividad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY actividad ALTER COLUMN id_actividad SET DEFAULT nextval('actividad_id_actividad_seq'::regclass);


--
-- Name: id_area; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY area ALTER COLUMN id_area SET DEFAULT nextval('id_seccion_id_seccion_seq'::regclass);


--
-- Name: id_bibliotecario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bibliotecario ALTER COLUMN id_bibliotecario SET DEFAULT nextval('bibliotecario_id_bibliotecario_seq'::regclass);


--
-- Name: id_editorial; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY editorial ALTER COLUMN id_editorial SET DEFAULT nextval('editorial_id_editorial_seq'::regclass);


--
-- Name: id_inmueble; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inmueble ALTER COLUMN id_inmueble SET DEFAULT nextval('inmueble_id_inmueble_seq'::regclass);


--
-- Name: id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN id_usuario SET DEFAULT nextval('usuario_id_usuario_seq'::regclass);


--
-- Data for Name: actividad; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO actividad VALUES (12, 1, 'lectura de libros', 'Lecturas de libros en sala', '2016-11-14', '2016-11-14', '05:15 PM', '05:15 PM', 'Finalizada', '14-11-2016-05:15:58', NULL);
INSERT INTO actividad VALUES (13, 1, 'taller de cultura y deporte', 'taller de cultura y deporte', '2016-12-12', '2016-12-12', '10:45 AM', '04:00 PM', 'Iniciada', '12-12-2016-07:06:27', NULL);
INSERT INTO actividad VALUES (14, 1, 'canto ', 'parrandon navideño', '2016-12-21', '2016-12-22', '10:00 AM', '06:00 PM', 'Iniciada', '12-12-2016-07:07:46', NULL);
INSERT INTO actividad VALUES (15, 1, 'lectura de libros', 'Lectura de libros infaltiles', '2017-02-08', '2017-02-08', '03:15 PM', '03:30 PM', 'Iniciada', '07-02-2017-03:24:34', NULL);


--
-- Name: actividad_id_actividad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('actividad_id_actividad_seq', 15, true);


--
-- Data for Name: area; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO area VALUES (7, '001', 'Informatica');
INSERT INTO area VALUES (8, '002', 'Castellano');


--
-- Data for Name: autor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO autor VALUES (1, 'Patrick Quentin', 'FEDERICO LARA PEINADO', 'FEDERICO LARA PEINADO');
INSERT INTO autor VALUES (3, 'Patrick Quentin', 'Patrick Quentin', 'FEDERICO LARA PEINADO');
INSERT INTO autor VALUES (2, 'Patrick Quentin', 'kai hon', 'yuton hasn');
INSERT INTO autor VALUES (4, 'jesus garate', 'julia pereda', 'carolina garate');
INSERT INTO autor VALUES (5, 'Patrick Quentin', 'FEDERICO LARA PEINADO', 'FEDERICO LARA PEINADO');
INSERT INTO autor VALUES (6, 'Patrick Quentin', 'kai hon', 'FEDERICO LARA PEINADO');


--
-- Data for Name: bibliotecario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO bibliotecario VALUES (2, 'yugdelis', 'Garate', 20022992, 'Administrador', 'blascoa', '04263852666', 'Femenino', 'yugde', 'ade8617270ac0473e7d79075e765272e', 'Activo', NULL);
INSERT INTO bibliotecario VALUES (1, 'robert', 'rodriguez', 20373816, 'Administrador', 'Cariaco', '04120819276', 'Masculino', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Activo', NULL);
INSERT INTO bibliotecario VALUES (3, 'santa', 'cova', 20373890, 'Bibliotecario', 'campoma', '042638512345', 'Femenino', 'santa', '98809c3f491160651ce442f657c62218', 'Activo', NULL);
INSERT INTO bibliotecario VALUES (4, 'Luis ', 'Lara', 22074876, 'Bibliotecario', 'Cariaco, C. Sucre', '02945551768', 'Masculino', 'luis2', '827ccb0eea8a706c4c34a16891f84e7b', 'Activo', '2016-11-13');


--
-- Name: bibliotecario_id_bibliotecario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bibliotecario_id_bibliotecario_seq', 4, true);


--
-- Data for Name: biografias; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO biografias VALUES (1, 1, 'C0DIGO DE HAMMURABI', 'Felix Bejarano', ' Saludos cordiales estimados colegas. Adjuntos les remito comunicación emitida por la Sección de Investigación y Proyecto de la Sede, donde se solicita, entre otras cosas, se remita información referente a la proyección del grupo de investigación I.D.I. para este 2017, así como los recursos necesarios para su ejecución. También se pide un listado de las bibliografías requeridas para las investigaciones en cursos. Por tal razón es necesario me envíen a la brevedad posible esta información para poder remitirla a la jefatura de Investigación. En este sentido, les adjunto también documento con la PROYECCIÓN 2017, a modo que se revise la propuesta y se complete la información. Pueden anexar algún otro evento o actividad que a bien deseen realizar para engrosar y fortalecer las actividades del grupo investigación. Aunado a esto, estoy recolectando la información relacionada con las actividades individuales de los miembros del grupo, para lo cual requiero se complete el formato anexo en el archivo "ActividadesIndependientes.docx". Les recuerdo que se debe actualizar el grupo de investigación con los miembros activos, por lo cual también adjunto el archivo con el formato de los temas de investigación que están trabajando los miembros del grupo y faltan por incorporarse la información de los profesores: Alexander Martínez, Ana Bejarano y Elauris Villalba (por favor ponerse al día). Es importante destacar que en función a este listado, se depurarán los miembros del grupo. Además que debemos ser miembros activos para poder incluir las horas de investigación en nuestros horarios de permanencia. Agradeciendo de antemano su atención y en espera de sus prontas respuestas, se despide.', 'public/biografias/biografia-1.JPEG', '2017-02-12');
INSERT INTO biografias VALUES (2, 1, 'Templo de Cariaco', 'Biblioteca Eduardo Blanco', 'Conocido popular como la iglesia san felipe Apostol, para el año 1935,iniciaron los buenos oficios para la construccion del nuevo y tercer templo de cariaco , esta obra alcanzo su culminacion para el año1956 gracias a las bondades de un saserdote de nombre Enrique Brekelmas, su estructura de concreto, su  techo de madera y teja de tres naves amplias separadas por ocho pilares de cada lado ,dos campanarios de aproximadamente de 15 metros de alto, una capula , una entrada frontal y dos laterales. en ellas resguardadas imagenes de San Felipe Apostol, San Isidro Larador, un santo sepulcro de madera, San Pedro la Virgen de la coromoto, Virgen del Carmen, la milagrosa , la inmaculada, un sagrado corazon de jesus, un cristo de madera en el  altar que aun se conserva el antiguo retablo, la  imagen de San Vicente de Paúl, y san jose que fue traido de cumana. 
la existencia de un tenplo parroquial en cariaco, fue refugio de la necesidad de santralizar ritos y crencias religiosas hace 10 años den la poblacion de cariaco ocurrio un sismo  el cual deterioro  sus estructura pero se callo el piso se agretio las paredes, tambien alunos santos se calleron y a otros se le desfiguro el rostro pero ni a si la fuerza de la naturaleza pero con la creencia religiosa de ste pueblo y a si 4en esemismo estado de destruccion la gente asistia ala iglecia .
', 'public/biografias/biografia-2.JPEG', '2017-02-13');
INSERT INTO biografias VALUES (3, 1, 'Caja de Agua', 'Biblioteca Eduardo Blanco', 'La caja de Agua de Cariaco, ubicada en el sector la Cruz, fue contruida en el año 1941 ( ase casi 67 años), por el señor Lesme Caraballo durante la presidencia de Isaias Mediana Angarita y quien gobernaba para esa epoca el estado sucre era el doctor Alberto Diaz. esta obra esta conformada por un tanque techado que servia de almcenaminto de agua y provenia del rio atraves de un sistema de bombeo y desde alli se distribuia el preciado liquido atraves de la poblacion de cariaco ', 'public/biografias/biografia-3.JPEG', '2017-02-13');
INSERT INTO biografias VALUES (4, 1, 'Puente Viejo ', 'Biblioteca Eduardo Blanco', 'Esta obra fue construida bajo el Gobierno de Juan Vicente Goméz un puente de   gran importancia por que alli trancitaba los vehiculos proveniente de Carupano , Cumaná y pueblos vecinos.cariaco era en aquel entonces un pueblo con gran importacia agricola y los campecinos transportaban sus productos por este puente, hasta los principales mercados del paiz.', 'public/biografias/biografia-4.JPEG', '2017-02-13');


--
-- Data for Name: editorial; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO editorial VALUES (5, 'El Sapo y La Rana', 'MADRID', 'ESPAÑA');


--
-- Name: editorial_id_editorial_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('editorial_id_editorial_seq', 5, true);


--
-- Name: id_seccion_id_seccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_seccion_id_seccion_seq', 8, true);


--
-- Data for Name: inmueble; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO inmueble VALUES (3, 1, 'computadoras', '2016-07-02', 34, '30', NULL);
INSERT INTO inmueble VALUES (4, 1, 'Tornillos', '2016-07-02', 150, '123', NULL);
INSERT INTO inmueble VALUES (2, 1, 'mesas de escritorios', '2016-07-20', 33, '7', NULL);
INSERT INTO inmueble VALUES (1, 1, 'ventanas', '2016-07-03', 12, '8', NULL);
INSERT INTO inmueble VALUES (5, 1, 'Gabetines', '2016-08-29', 12, '0', NULL);
INSERT INTO inmueble VALUES (7, 1, 'Ventilador de pared', '2016-11-07', 9, '4', '2016-11-13');
INSERT INTO inmueble VALUES (6, 1, 'Sillones de plastico', '2016-08-01', 99, '4', NULL);
INSERT INTO inmueble VALUES (8, 1, 'laptos', '2016-11-13', 9, '0', '2016-11-13');
INSERT INTO inmueble VALUES (9, 3, 'ventanas marcellas', '2016-11-13', 8, '0', '2016-11-13');


--
-- Name: inmueble_id_inmueble_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inmueble_id_inmueble_seq', 9, true);


--
-- Data for Name: libro; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO libro VALUES (1, 1, 'iool-lol-lapk', 'Programacion en C', '1212-09582', 3, 'PRIMERA EDICION', '229', '2012', 'Ninguna', 'Activo', 'General', 7, 5, '0', '2017-02-12');
INSERT INTO libro VALUES (2, 1, 'iool-lol-yt', 'CODIGO DE HAMMURABI', '346.0093500', 2, 'PRIMERA EDICION', '229', '2016', 'Ninguna', 'Activo', 'General', 7, 5, '0', '2017-02-12');
INSERT INTO libro VALUES (3, 1, 'iool-lol-oi', 'Programacion en java', '346.0093588', 2, 'PRIMERA EDICION', '229', '1986', 'Ninguna', 'Activo', 'General', 7, 5, '0', '2017-02-12');
INSERT INTO libro VALUES (5, 1, 'iool-lol-loiu', 'Programacion en C++', '346.0093599', 3, 'PRIMERA EDICION', '500', '2012', 'Ninguna', 'Activo', 'General', 7, 5, '0', '2017-02-15');
INSERT INTO libro VALUES (4, 1, '123yugde', 'el perro loco', '1234garate', 1, 'Primera', '509', '1972', 'tine algunas paginas dañadas', 'Activo', 'General', 8, 5, '0', '2017-02-13');
INSERT INTO libro VALUES (6, 1, 'iool-lol-loiuo', 'Programacion en PHP', '1212-095828', 5, 'PRIMERA EDICION', '229', '1986', 'Ninguna', 'Activo', 'General', 7, 5, '0', '2017-03-07');


--
-- Data for Name: prestamo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO prestamo VALUES (1, 1, 1, 'Circulante', '2017-03-10', '2017-03-11');
INSERT INTO prestamo VALUES (2, 1, 2, 'Circulante', '2017-03-10', '2017-03-12');
INSERT INTO prestamo VALUES (3, 1, 2, 'Circulante', '2017-03-16', '2017-03-17');
INSERT INTO prestamo VALUES (4, 1, 1, 'Circulante', '2017-03-17', '2017-03-18');


--
-- Name: prestamo_id_prestamo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('prestamo_id_prestamo_seq', 1, false);


--
-- Data for Name: prestamo_libro; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO prestamo_libro VALUES (1, 1, 6, 'Retornado', '2017-03-13');
INSERT INTO prestamo_libro VALUES (2, 2, 6, 'Retornado', '2017-03-14');
INSERT INTO prestamo_libro VALUES (3, 3, 6, 'Retornado', '2017-03-20');
INSERT INTO prestamo_libro VALUES (4, 4, 6, 'Retornado', '2017-03-21');


--
-- Data for Name: sancion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sancion VALUES (1, 1, '2017-03-13', '2017-03-15', 'ACTIVO');
INSERT INTO sancion VALUES (2, 2, '2017-03-14', '2017-03-16', 'ACTIVO');
INSERT INTO sancion VALUES (3, 3, '2017-03-20', '2017-03-23', 'ACTIVO');
INSERT INTO sancion VALUES (4, 4, '2017-03-21', '2017-03-24', 'ACTIVO');


--
-- Name: sancion_id_sancion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sancion_id_sancion_seq', 2, true);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES (1, 1, 20022889, 'Fabian', 'Eduardo', 'Espinoza', 'Marquez', 'Masculino', 'fabian@gmail.com', 'Estudiante', 'Circulante', 'Ninguno', 'Ninguna', '1993-03-02', 'Cariaco', 'Cariaco', 'Cariaco', '04126387654', '02948880626', '2017-03-12', '1');
INSERT INTO usuario VALUES (2, 1, 4296074, 'Luisa', 'Del Carmen', 'Espinoza', 'Centeno', 'Femenino', 'luisa@gmail.com', 'Administradora', 'Circulante', 'Ninguno', 'Ninguna', '1988-03-03', 'Cariaco', 'Cariaco', 'Cariaco', '04126387654', '02948880626', '2017-03-12', '2');
INSERT INTO usuario VALUES (3, 1, 16842085, 'Carolina', 'Maria', 'Espinoza', 'Centeno', 'Femenino', 'carolina@gmail.com', 'Estudiante', 'Visitante', 'Ninguno', 'Ninguna', '1990-11-07', 'Cariaco', 'Cariaco', 'Cariaco', '04126387654', '02948880626', '2017-03-12', '3');


--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_usuario_seq', 16, true);


--
-- Name: actividad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY actividad
    ADD CONSTRAINT actividad_pkey PRIMARY KEY (id_actividad);


--
-- Name: bibliotecario_cedula_cuenta_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bibliotecario
    ADD CONSTRAINT bibliotecario_cedula_cuenta_key UNIQUE (cedula, cuenta);


--
-- Name: bibliotecario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bibliotecario
    ADD CONSTRAINT bibliotecario_pkey PRIMARY KEY (id_bibliotecario);


--
-- Name: biografias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY biografias
    ADD CONSTRAINT biografias_pkey PRIMARY KEY (id_biografia);


--
-- Name: editorial_nombre_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY editorial
    ADD CONSTRAINT editorial_nombre_key UNIQUE (nombre);


--
-- Name: inmueble_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY inmueble
    ADD CONSTRAINT inmueble_pkey PRIMARY KEY (id_inmueble);


--
-- Name: libro_isbn_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libro
    ADD CONSTRAINT libro_isbn_key UNIQUE (isbn);


--
-- Name: libro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY libro
    ADD CONSTRAINT libro_pkey PRIMARY KEY (id_libro);


--
-- Name: prestamo_libro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY prestamo_libro
    ADD CONSTRAINT prestamo_libro_pkey PRIMARY KEY (id_prestamo_libro);


--
-- Name: prestamo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY prestamo
    ADD CONSTRAINT prestamo_pkey PRIMARY KEY (id_prestamo);


--
-- Name: sancion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sancion
    ADD CONSTRAINT sancion_pkey PRIMARY KEY (id_sancion);


--
-- Name: seccion_codigo_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY area
    ADD CONSTRAINT seccion_codigo_key UNIQUE (codigo);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

