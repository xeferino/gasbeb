<?php

function calcular_retraso($primera, $segunda)
 {
  $valoresPrimera = explode ("-", $primera);   
  $valoresSegunda = explode ("-", $segunda); 

  $diaPrimera    = $valoresPrimera[0];  
  $mesPrimera  = $valoresPrimera[1];  
  $anyoPrimera   = $valoresPrimera[2]; 

  $diaSegunda   = $valoresSegunda[0];  
  $mesSegunda = $valoresSegunda[1];  
  $anyoSegunda  = $valoresSegunda[2];

  $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);  
  $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     

  if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
    // "La fecha ".$primera." no es v&aacute;lida";
    /*echo "<script type=\"text/javascript\"> alert(\"Verifique. La Fecha Primera Está Mala\");</script>";*/
	return "INVALIDA";
  }elseif(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
    // "La fecha ".$segunda." no es v&aacute;lida";
    /*echo "<script type=\"text/javascript\"> alert(\"Verifique. La Fecha Segunda Está Mala\");</script>";*/
	return "INVALIDA";
  }else{
  	$resultado = $diasPrimeraJuliano - $diasSegundaJuliano;
    return $resultado;
  } 
	 
}

//$fecha_prestamo = "4/02/2013";
//$fecha_devolucion = "4/02/2013";




		
?>