<?php
include_once ("motor.php");
include_once ("comparar_fechas.php");
include_once ("calcular_retraso.php");


	$prestamo = new Prestamo();
	$prestamo->ejemplares();
	$prestamo->ejemplares_prestados();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'prestamo') {
					
		$id_usuario = $_POST['id_usuario'];		
		$id_bibliotecario=$_SESSION['conectado'];
		
		$tipo_prestamo = $_POST['tipo_prestamo'];
		$fecha_prestamo = $_POST['fecha_prestamo'];
		$fecha_entrega = $_POST['fecha_entrega'];
		$seleccionados = $_POST['select'];
		if($tipo_prestamo=='Interno'){$fecha_entrega = $fecha_prestamo;}

		$dias = compararFechas ($fecha_entrega, $fecha_prestamo);
		
		if(($dias == "IGUALES") || ($dias >= 1))
		{	
			if($dias != "INVALIDA")
			{
		
				$prestamo->inicializar_datos($tipo_prestamo, $fecha_prestamo, $fecha_entrega);
				$prestamo->registrar_prestamo($id_bibliotecario, $id_usuario, $seleccionados);
				
				$query = pg_query("select * from prestamo order by id_prestamo DESC");
								$row = pg_fetch_array($query);
								$id_prestamo  = $row['id_prestamo'];
								$id_prestamo_libro = pg_num_rows (pg_query("select * from prestamo_libro"));
								if($id_prestamo_libro == 0){$id_prestamo_libro=1;}else{$id_prestamo_libro=$id_prestamo_libro+1;}
				$n = count($seleccionados);
				//echo $n;
				if($prestamo->validar==1)
				{
				$i=0;
					while($i<$n){					  
						$id_prestamo_libro = pg_num_rows (pg_query("select * from prestamo_libro"));
						if($id_prestamo_libro == 0){$id_prestamo_libro=1;}else{$id_prestamo_libro=$id_prestamo_libro+1;}
						$cons = pg_query("insert into prestamo_libro 
										  values ('$id_prestamo_libro','$id_prestamo','$seleccionados[$i]','Pendiente')");
					$i++;
					}//fin de while
				}
			}
		}
		if($dias <= -1){$mensaje=4;}
	}
	
	if(isset($_GET['id_prestamo']) && isset($_GET['retraso']) && isset($_GET['id']))
	{
		$id_prestamo=$_GET['id_prestamo'];
		$retraso = $_GET['retraso'];
		$id = $_GET['id'];
						
		$prestamo->registrar_devolucion($id_prestamo, $retraso, $id);
		
		header('location:../vistas/devoluciones.php');
	}
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'fecha_rango') {
	
		$desde =  $_POST['desde'];
		$hasta =  $_POST['hasta'];
		$estatus =  $_POST['estatus'];
		

		$prestamo->reporte_prestamo_fecha_rango($desde, $hasta, $estatus);
	}
	
	
?>
