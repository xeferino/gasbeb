<?php
include_once ("motor.php");
	$ejemplar = new Ejemplar();
	if (isset($_POST['submit']) && $_POST['submit'] == 'new') {
	
		$titulo = $_POST['titulo'];
		$edicion = $_POST['edicion'];
		$isbn = $_POST['isbn'];
		$anio = $_POST['anio'];
		$cota = $_POST['cota'];
		$paginas = $_POST['paginas'];
		$editoria = $_POST['editoria'];
		$areas = $_POST['areas'];
		$autor1 = $_POST['autor1'];
		$autor2 = $_POST['autor2'];
		$autor3 = $_POST['autor3'];
		$sala = $_POST['sala'];
		$cant_ejemplar = $_POST['cant_ejemplar'];
		$cant_inactivos = '0';
		$estatus = $_POST['estatus'];
		$observacion = $_POST['observacion'];
		
		$id = $_SESSION['conectado'];
		
	$ejemplar->inicializar_datos_ejemplar ($id_libro, $titulo, $edicion, $isbn, $anio, $cota, $paginas, $editoria, $areas, $autor1, $autor2, $autor3, $sala, $cant_ejemplar, $estatus, $observacion, $cant_inactivos);
	$ejemplar->registrar_ejemplar($id);
	}
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'editorial') {
	
		$editorial = $_POST['editorial'];
		$ciudad = $_POST['ciudad'];
		$pais = $_POST['pais'];
		
		$ejemplar->inicializar_datos_editorial ($editorial, $ciudad, $pais);
		$ejemplar->registrar_editorial();
	}
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'area') {
	
		$codigo = $_POST['codigo'];
		$area = $_POST['area'];
		
		$ejemplar->inicializar_datos_area ($codigo, $area);
		$ejemplar->registrar_area();
	}
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'fecha_rango') {
	
		$desde =  $_POST['desde'];
		$hasta =  $_POST['hasta'];
		$tipo =  $_POST['tipo'];
		
		$ejemplar->reporte_ejemplares_fecha_rango($desde, $hasta, $tipo);
	}
		$ejemplar->listado_ejemplares();
		$ejemplar->listado_editoriales();
		$ejemplar->listado_categorias();
?>
