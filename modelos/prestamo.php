<?php
	class Prestamo
	{				
		
		public $tipo_prestamo;
		public $fecha_prestamo;
		public $fecha_entrega;
		
		public $mensaje;
		
		public function inicializar_datos ($tipo_prestamo, $fecha_prestamo, $fecha_entrega)
		{
			$this->tipo_prestamo=$tipo_prestamo;
			$this->fecha_prestamo=$fecha_prestamo;
			$this->fecha_entrega=$fecha_entrega;
		}//fin function
		
		public function registrar_prestamo($id_bibliotecario, $id_usuario, $seleccionados)
		{
		
			$this->id_bibliotecario=$id_bibliotecario;
			$this->id_usuario=$id_usuario;
			$this->seleccionados=$seleccionados;
			
			if(($this->tipo_prestamo!='0') && ($this->id_usuario!='0') && ($this->seleccionados!='') )
			{
				$this->id_prestamo = pg_num_rows (pg_query("select id_prestamo from prestamo"));
				if($this->id_prestamo == 0){$this->id_prestamo=1;}else{$this->id_prestamo=$this->id_prestamo+1;}
					
					if($this->tipo_prestamo=='Interno')
					{	
						$usuario=pg_query("select *from usuario, prestamo, sancion where usuario.id_usuario=prestamo.id_usuario 
						and prestamo.id_prestamo=sancion.id_prestamo and  usuario.id_usuario='$this->id_usuario' and 
						sancion.estatus='Suspendido'");
						$validar=pg_num_rows($usuario);
					
						if ($validar==0)
						{
							$query= pg_query("SELECT count(*) from prestamo, prestamo_libro, libro, usuario
							 where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
							 and prestamo.id_usuario=usuario.id_usuario and prestamo_libro.prestamo_estatus='Pendiente' 
							 and usuario.id_usuario='$this->id_usuario'");
							 
							 $row=pg_fetch_array($query);
							 $this->maximo=$row['0'];
				
								if($this->maximo==3)
								{
									   
									$this->mensaje=7;
								}
								else{
										$insert_prestamo =  pg_query("insert into prestamo 
										values ('$this->id_prestamo','$this->id_bibliotecario','$this->id_usuario',
										'$this->tipo_prestamo','$this->fecha_prestamo','$this->fecha_entrega')");
											$this->mensaje=1;
											$this->validar = pg_affected_rows($insert_prestamo);
									}
						}//validar
						else
						{
							$this->mensaje=8;
						}
					}//fin interno
					else
					{
						$usuario=pg_query("select *from usuario, prestamo, sancion where usuario.id_usuario=prestamo.id_usuario 
						and prestamo.id_prestamo=sancion.id_prestamo and  usuario.id_usuario='$this->id_usuario' and 
						sancion.estatus='Suspendido'");
						$validar=pg_num_rows($usuario);
					
						if ($validar==0)
						{
							$user_query = pg_num_rows(pg_query("SELECT * FROM usuario WHERE  id_usuario='$this->id_usuario' and
							tipo='Visitante'"));
							if ($user_query==0)
							{
								
									$query= pg_query("SELECT count(*) from prestamo, prestamo_libro, libro, usuario
									 where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
									 and prestamo.id_usuario=usuario.id_usuario and prestamo_libro.prestamo_estatus='Pendiente' 
									 and usuario.id_usuario='$this->id_usuario'");
									 
									 $row=pg_fetch_array($query);
									 $this->maximo=$row['0'];
						
										if($this->maximo==3)
										{
											   
											$this->mensaje=7;
										}
										else{
												$insert_prestamo =  pg_query("insert into prestamo 
												values ('$this->id_prestamo','$this->id_bibliotecario','$this->id_usuario',
												'$this->tipo_prestamo','$this->fecha_prestamo','$this->fecha_entrega')");
													$this->mensaje=1;
													$this->validar = pg_affected_rows($insert_prestamo);
											}
							}
							else
							{
								$this->mensaje=4;
							}
						}//validar
						else
						{
							$this->mensaje=8;
						}
					}//fin de else principal
			}
			else
			{
				$this->mensaje=2;	
			}
			
		}//fin de function
		
		public function registrar_devolucion($id_prestamo, $retraso, $id)
		{
			$this->id_prestamo=$id_prestamo;
			$this->retraso=$retraso;
			$this->id=$id;
			
			$query = pg_num_rows(pg_query("select *from prestamo_libro where id_prestamo_libro='$this->id_prestamo'"));
			
			if($query==1)
			{
				$fecha_actual=date('Y-m-d');
				$devolucion=pg_query("update prestamo_libro set prestamo_estatus='Retornado', fecha_devolucion='$fecha_actual' 
				where id_prestamo_libro='$this->id_prestamo'");
				
			}
			if($this->retraso>0)
			{
				$query = pg_query("select *from prestamo where id_usuario='$this->id'");
				$datos = pg_fetch_array($query);
				$id_usuario = $datos['id_usuario'];
			
				$usuario_sancionados=pg_query("select * from usuario, prestamo, sancion where 		              
				usuario.id_usuario=prestamo.id_usuario and prestamo.id_prestamo=sancion.id_prestamo 
				and usuario.id_usuario='$id_usuario' and sancion.estatus='Suspendido'");
				$sancion = pg_num_rows($usuario_sancionados);
				if($sancion==0)
				{
				
					$mes= date("m");
					$dia= date("d");
					$ano= date("Y");
					
					while($this->retraso > 0)
					{
						if(($mes==1) && ($dia<31)){$dia++;}
						if(($mes==1) && ($dia==31)){$dia=01; $mes=2;}
						
						if(($mes==2) && ($dia<28)){$dia++;}
						if(($mes==2) && ($dia==28)){$dia="01"; $mes="03";}
						
						if(($mes==3) && ($dia<31)){$dia++;}
						if(($mes==3) && ($dia==31)){$dia=01; $mes=4;}
						
						if(($mes==4) && ($dia<30)){$dia++;}
						if(($mes==4) && ($dia==30)){$dia=01; $mes=5;}
						
						if(($mes==5) && ($dia<31)){$dia++;}
						if(($mes==5) && ($dia==31)){$dia=01; $mes=6;}
						
						if(($mes==6) && ($dia<30)){$dia++;}
						if(($mes==6) && ($dia==30)){$dia=01; $mes=7;}
						
						if(($mes==7) && ($dia<31)){$dia++;}
						if(($mes==7) && ($dia==31)){$dia=01; $mes=8;}
						
						if(($mes==8) && ($dia<31)){$dia++;}
						if(($mes==8) && ($dia==31)){$dia=01; $mes=9;}
						
						if(($mes==9) && ($dia<30)){$dia++;}
						if(($mes==9) && ($dia==30)){$dia=01; $mes=10;}
						
						if(($mes==10) && ($dia<31)){$dia++;}
						if(($mes==10) && ($dia==31)){$dia=01; $mes=11;}
						
						if(($mes==11) && ($dia<30)){$dia++;}
						if(($mes==11) && ($dia==30)){$dia=01; $mes=12;}
						
						if(($mes==12) && ($dia<31)){$dia++;}
						if(($mes==12) && ($dia==31)){$dia=01; $mes=1;}
						$this->retraso--;
					}//fin de while
					
					if($dia == 1){$dia="01";}
					if($dia == 2){$dia="02";}
					if($dia == 3){$dia="03";}
					if($dia == 4){$dia="04";}
					if($dia == 5){$dia="05";}
					if($dia == 6){$dia="06";}
					if($dia == 7){$dia="07";}
					if($dia == 8){$dia="08";}
					if($dia == 9){$dia="09";}
					
					if($mes == 1){$mes="01";}
					if($mes == 2){$mes="02";}
					if($mes == 3){$mes="03";}
					if($mes == 4){$mes="04";}
					if($mes == 5){$mes="05";}
					if($mes == 6){$mes="06";}
					if($mes == 7){$mes="07";}
					if($mes == 8){$mes="08";}
					if($mes == 9){$mes="09";}
	
					$inicio = date('Y-m-d');
					$fin = $ano."/".$mes."/".$dia;
					
					$id_sancion = pg_num_rows (pg_query("select id_sancion from sancion"));
					if($id_sancion == 0){$id_sancion=1;}else{$id_sancion=$id_sancion+1;}
					
					$sancion = pg_query("insert into sancion values ('$id_sancion','$this->id_prestamo',
										 '$inicio ','$fin','SUSPENDIDO')");
				}//fin de sanciones				
			}
		}//fin de function
		
		public function buscar_ejemplares($palabra)
		{
			$this->palabra=$palabra;
										 
			$this->busqueda=pg_query("SELECT * FROM libro 
									  WHERE ((titulo like '%$this->palabra%') or (isbn like '%$this->palabra%'))");
			$this->valida_busqueda = pg_num_rows ($this->busqueda);
													
		}//fin de function
		
		public function ejemplares()
		{
			$this->ejemplares = pg_query("SELECT	
			                             libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala, 
										 libro.cant_inactivos, area.codigo, area.area, editorial.nombre, editorial.ciudad, 
										 editorial.pais, autor.autor1, autor.autor2, autor.autor3
                                         FROM libro, area, editorial, autor 
										 WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro order by libro.id_libro desc");												
		}//fin de function
		
		public function ejemplares_prestados()
		{
			$this->ejemplares_prestados = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.id_usuario, usuario.primer_nombre,                                                                 usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo order by                                                  prestamo_libro.id_prestamo_libro desc");
												  
			/*$this->ejemplares_prestados= pg_query("select * from prestamo
								LEFT JOIN usuario ON prestamo.id_usuario = usuario.id_usuario
								LEFT JOIN prestamo_libro ON prestamo.id_prestamo = prestamo_libro.id_prestamo
								LEFT JOIN libro on prestamo_libro.id_libro =  libro.id_libro 
								ORDER BY prestamo_libro.id_prestamo_libro DESC");*/
		}//fin de function
		
		public function prestamos_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM prestamo");
			$this->prestamo = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function devoluciones_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM prestamo_libro where prestamo_estatus='Retornado'");
			$this->devoluciones = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function reporte_prestamo_fecha_rango($desde, $hasta, $estatus)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->estatus=$estatus;
			
			if($this->estatus!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->estatus=="Todos")
				{ 
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
				
				if($this->estatus=="Entregados")
				{
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                   
												  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' AND 
												  prestamo_libro.prestamo_estatus='Retornado' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
				
				if($this->estatus=="Pendientes")
				{
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' AND 
												  prestamo_libro.prestamo_estatus='Pendiente' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		
		public function auditoria_prestamo()
		{	
			
			$this->auditoria= pg_query("select * from bibliotecario, prestamo 
										where bibliotecario.id_bibliotecario=prestamo.id_bibliotecario");
		}//fin de function
		
		public function auditoria_devoluciones()
		{	
			
			$this->auditoria= pg_query("SELECT    bibliotecario.id_bibliotecario, bibliotecario.nombre,bibliotecario.apellido,
												  prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro, bibliotecario
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and 
												  bibliotecario.id_bibliotecario=prestamo.id_bibliotecario order by                                                  prestamo_libro.id_prestamo_libro desc");
		}//fin de function
	}//fin de class
	?>