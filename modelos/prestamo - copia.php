<?php
	class Prestamo
	{				
		
		public $tipo_prestamo;
		public $fecha_prestamo;
		public $fecha_entrega;
		
		public $mensaje;
		
		public function inicializar_datos ($tipo_prestamo, $fecha_prestamo, $fecha_entrega)
		{
			$this->tipo_prestamo=$tipo_prestamo;
			$this->fecha_prestamo=$fecha_prestamo;
			$this->fecha_entrega=$fecha_entrega;
		}//fin function
		
		public function registrar_prestamo($id_bibliotecario, $id_usuario, $seleccionados)
		{
		
			$this->id_bibliotecario=$id_bibliotecario;
			$this->id_usuario=$id_usuario;
			$this->seleccionados=$seleccionados;
			
			if(($this->tipo_prestamo!='0') && ($this->id_usuario!='0') && ($this->seleccionados!='') )
			{
				$this->id_prestamo = pg_num_rows (pg_query("select id_prestamo from prestamo"));
				if($this->id_prestamo == 0){$this->id_prestamo=1;}else{$this->id_prestamo=$this->id_prestamo+1;}
					
					if($this->tipo_prestamo=='Interno')
					{	
								$query= pg_query("SELECT count(*) from prestamo, prestamo_libro, libro, usuario
	                             where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
								 and prestamo.id_usuario=usuario.id_usuario and prestamo_libro.prestamo_estatus='Pendiente' 
								 and usuario.id_usuario='$this->id_usuario'");
								 
								 $row=pg_fetch_array($query);
								 $this->maximo=$row['0'];
					
								 	if($this->maximo==3)
								 	{
								 		   
										$this->mensaje=7;
									}
									else{
											$insert_prestamo =  pg_query("insert into prestamo 
											values ('$this->id_prestamo','$this->id_bibliotecario','$this->id_usuario',
											'$this->tipo_prestamo','$this->fecha_prestamo','$this->fecha_entrega')");
												$this->mensaje=1;
												$this->validar = pg_affected_rows($insert_prestamo);
										}
					}
					else
					{
						$user_query = pg_num_rows(pg_query("SELECT * FROM usuario WHERE  id_usuario='$this->id_usuario' and
						tipo='Visitante'"));
						if ($user_query==0)
						{
							
							    $query= pg_query("SELECT count(*) from prestamo, prestamo_libro, libro, usuario
	                             where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
								 and prestamo.id_usuario=usuario.id_usuario and prestamo_libro.prestamo_estatus='Pendiente' 
								 and usuario.id_usuario='$this->id_usuario'");
								 
								 $row=pg_fetch_array($query);
								 $this->maximo=$row['0'];
					
								 	if($this->maximo==3)
								 	{
								 		   
										$this->mensaje=7;
									}
									else{
											$insert_prestamo =  pg_query("insert into prestamo 
											values ('$this->id_prestamo','$this->id_bibliotecario','$this->id_usuario',
											'$this->tipo_prestamo','$this->fecha_prestamo','$this->fecha_entrega')");
												$this->mensaje=1;
												$this->validar = pg_affected_rows($insert_prestamo);
										}
						}
						else
						{
							$this->mensaje=4;
						}
					}//fin de else principal
			}
			else
			{
				$this->mensaje=2;	
			}
			
		}//fin de function
		
		public function registrar_devolucion($id_prestamo)
		{
			$this->id_prestamo=$id_prestamo;
			//$this->id_libro=$id_libro;
			
			$query = pg_num_rows(pg_query("select *from prestamo_libro where id_prestamo_libro='$this->id_prestamo'"));
			if($query==1)
			{
				$fecha_actual=date('Y-m-d');
				$devolucion=pg_query("update prestamo_libro set prestamo_estatus='Retornado', fecha_devolucion='$fecha_actual' 
				where id_prestamo_libro='$this->id_prestamo'");
			}
		}//fin de function
		
		public function buscar_ejemplares($palabra)
		{
			$this->palabra=$palabra;
			/*$this->ejemplares = pg_query("SELECT	
			                             libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala, 
										 libro.cant_inactivos, area.codigo, area.area, editorial.nombre, editorial.ciudad, 
										 editorial.pais, autor.autor1, autor.autor2, autor.autor3
                                         FROM libro, area, editorial, autor 
										 WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro order by libro.id_libro desc");	*/
										 
			$this->busqueda=pg_query("SELECT * FROM libro 
									  WHERE ((titulo like '%$this->palabra%') or (isbn like '%$this->palabra%'))");
			$this->valida_busqueda = pg_num_rows ($this->busqueda);
													
		}//fin de function
		
		public function ejemplares()
		{
			$this->ejemplares = pg_query("SELECT	
			                             libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala, 
										 libro.cant_inactivos, area.codigo, area.area, editorial.nombre, editorial.ciudad, 
										 editorial.pais, autor.autor1, autor.autor2, autor.autor3
                                         FROM libro, area, editorial, autor 
										 WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro order by libro.id_libro desc");
													
		}//fin de function
		
		public function ejemplares_prestados()
		{
			$this->ejemplares_prestados = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo order by                                                  prestamo_libro.id_prestamo_libro desc");
												  
			/*$this->ejemplares_prestados= pg_query("select * from prestamo
								LEFT JOIN usuario ON prestamo.id_usuario = usuario.id_usuario
								LEFT JOIN prestamo_libro ON prestamo.id_prestamo = prestamo_libro.id_prestamo
								LEFT JOIN libro on prestamo_libro.id_libro =  libro.id_libro 
								ORDER BY prestamo_libro.id_prestamo_libro DESC");*/
		}//fin de function
		
		public function prestamos_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM prestamo");
			$this->prestamo = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function devoluciones_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM prestamo_libro where prestamo_estatus='Retornado'");
			$this->devoluciones = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function reporte_prestamo_fecha_rango($desde, $hasta, $estatus)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->estatus=$estatus;
			
			if($this->estatus!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->estatus=="Todos")
				{ 
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
				
				if($this->estatus=="Entregados")
				{
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                   
												  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' AND 
												  prestamo_libro.prestamo_estatus='Retornado' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
				
				if($this->estatus=="Pendientes")
				{
					$this->cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$this->desde' AND '$this->hasta' AND 
												  prestamo_libro.prestamo_estatus='Pendiente' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$this->valido = pg_num_rows ($this->cons);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		public function auditoria_prestamo()
		{	
			
			$this->auditoria= pg_query("select * from bibliotecario, prestamo 
										where bibliotecario.id_bibliotecario=prestamo.id_bibliotecario");
		}//fin de function
		
		public function auditoria_devoluciones()
		{	
			
			$this->auditoria= pg_query("SELECT    bibliotecario.id_bibliotecario, bibliotecario.nombre,bibliotecario.apellido,
												  prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro, bibliotecario
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and 
												  bibliotecario.id_bibliotecario=prestamo.id_bibliotecario order by                                                  prestamo_libro.id_prestamo_libro desc");
		}//fin de function
	}//fin de class
	?>