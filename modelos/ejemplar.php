<?php
	class Ejemplar
	{
			//datos del ejemplar
			public $id_libro;
			public $titulo;
			public $edicion;
			public $isbn;
			public $anio;
			public $cota;
			public $paginas;
			public $editoria;
			public $areas;
			public $autor1;
			public $autor2;
			public $autor3;
			public $sala;
			public $cant_ejemplar;
			public $estatus;
			public $observacion;
			
			//datos del area del ejemplar
			public $codigo;
			public $area;
			
			//datos del editorial del ejemplar
			public $editorial;
			public $ciudad;
			public $pais;
			
			public $mensaje;
		
		public function inicializar_datos_area ($codigo, $area)
		{
			$this->codigo=$codigo;
			$this->area=$area;
		}//fin de function
			
		public function inicializar_datos_editorial ($editorial, $ciudad, $pais)
		{
			$this->editorial=$editorial;
			$this->ciudad=$ciudad;
			$this->pais=$pais;
		}//fin de function
		
		public function inicializar_datos_ejemplar ($id_libro, $titulo, $edicion, $isbn, $anio, $cota, $paginas, $editoria, $areas, $autor1, $autor2, $autor3, $sala, $cant_ejemplar, $estatus, $observacion, $cant_inactivos)
		{
			$this->id_libro=$id_libro;
			$this->titulo=$titulo;
			$this->edicion=$edicion;
			$this->isbn=$isbn;
			$this->anio=$anio;
			$this->cota=$cota;
			$this->paginas=$paginas;
			$this->editoria=$editoria;
			$this->areas=$areas;
			$this->autor1=$autor1;
			$this->autor2=$autor2;
			$this->autor3=$autor3;
			$this->sala=$sala;
			$this->cant_ejemplar=$cant_ejemplar;
			$this->cant_inactivos=$cant_inactivos;
			$this->estatus=$estatus;
			$this->observacion=$observacion;
		}//fin de function
		
		public function registrar_editorial()
		{
			if(($this->editorial!='') && ($this->ciudad!='') && ($this->pais!=''))
			{
				$nombre = pg_num_rows(pg_query("select nombre from editorial where nombre='$this->editorial'"));
				if($nombre==0)
				{
					$editorial=pg_query("insert into editorial values (default,'$this->editorial','$this->ciudad','$this->pais')");
					$this->mensaje=1;
					$this->editorial='';
					$this->ciudad='';
					$this->pais='';
				}
				else
				{
					$this->mensaje=2;
				}
			}//fin de campos en blanco
			else
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function actualizar_editorial($id,$nombre)
		{
			$this->id=$id;
			$this->nombre=$nombre;
			if(($this->editorial!='') && ($this->ciudad!='') && ($this->pais!=''))
			{
				$nombre = pg_num_rows(pg_query("select nombre from editorial where nombre='$this->editorial' and nombre <> '$this->nombre'"));
				if($nombre==0)
				{
					$editorial=pg_query("update editorial set nombre='$this->editorial', ciudad='$this->ciudad', pais='$this->pais' where id_editorial='$this->id'");
					
					echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
					print"<script>	$(location).attr('href','http:./editoriales.php');</script>";
				}
				else
				{
					$this->mensaje=1;
				}
			}//fin de campos en blanco
			else
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function datos_editorial ($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./editoriales.php');</script>";
			}
			else
			{
				$query=pg_query("select *from editorial where id_editorial='$id'");
				$this->reg=pg_fetch_object($query);
					$this->editorial=$this->reg->nombre;
					$this->ciudad=$this->reg->ciudad;
					$this->pais=$this->reg->pais;
			}
		}//fin de function
		
		public function listado_editoriales()
		{
			$this->editoriales= pg_query("select *from editorial order by id_editorial desc");
		}//fin de function
		
		public function editoriales_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM editorial");
			$this->editoriales = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function registrar_area()
		{
			if(($this->codigo!='') && ($this->area!=''))
			{
				$query = pg_num_rows(pg_query("select codigo from area where codigo='$this->codigo'"));
				if($query==0)
				{
					$query = pg_num_rows(pg_query("select area from area where area='$this->area'"));
					if($query==0)
					{
						$categoria=pg_query("insert into area values (default,'$this->codigo','$this->area')");
						$this->mensaje=1;
						$this->codigo='';
						$this->area='';
					}
					else
					{
						$this->mensaje=2;
					}
				}
				else
				{
					$this->mensaje=3;
				}
			}//fin de campos en blanco
			else
			{
				$this->mensaje=4;
			}
		}//fin de function
		
		public function actualizar_categoria($id,$area1,$cod)
		{
			$this->id=$id;
			$this->area1=$area1;
			$this->cod=$cod;
			
			if(($this->codigo!='') && ($this->area!=''))
			{
				$codigo = pg_num_rows(pg_query("select codigo from area where codigo='$this->codigo' and codigo <> '$this->cod' and id_area='$this->id'"));
				if($codigo==0)
				{
						$nombre = pg_num_rows(pg_query("select area from area where area='$this->area' and area <> '$this->area1' and id_area='$this->id'"));
					if($nombre==0)
					{
						$categoria=pg_query("update area set codigo='$this->codigo', area='$this->area' where id_area='$this->id'");
						echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
						print"<script>	$(location).attr('href','http:./areas.php');</script>";
					}
					else
					{
						$this->mensaje=1;
					}
				}//fin codigo
				else
				{
					$this->mensaje=2;
				}
			}//fin de campos en blanco
			else
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function datos_categoria ($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./areas.php');</script>";
			}
			else
			{
				$query=pg_query("select *from area where id_area='$id'");
				$this->reg=pg_fetch_object($query);
					$this->codigo=$this->reg->codigo;
					$this->area=$this->reg->area;
			}
		}//fin de function
		
		public function listado_categorias()
		{
			$this->categorias= pg_query("select *from area order by id_area desc");
		}//fin de function
		
		public function codigos()
		{
			$this->id_libro=pg_num_rows(pg_query("select id_libro from libro"));
			if($this->id_libro==0){$this->id_libro=1;}else{$this->id_libro=$this->id_libro+1;}
		}
		
		public function categorias_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM area");
			$this->categorias = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function registrar_ejemplar($id)
		{
			$this->id=$id;
			$this->codigos();
			
			if(($this->titulo!='') && ($this->edicion!='') && ($this->isbn!='') && ($this->anio!='') && ($this->cota!='') && ($this->paginas!='') && ($this->editoria!='0') && ($this->areas!='0') && ($this->sala!='0') && ($this->cant_ejemplar!='') && ($this->estatus!='') && ($this->observacion!='') && ($this->autor1!='') && ($this->autor2!='') && ($this->autor3!=''))
			{
				$isbn = pg_num_rows(pg_query("select isbn from libro where isbn='$this->isbn'"));
				
				if($isbn==0)
				{
					$cota = pg_num_rows(pg_query("select cota from libro where cota='$this->cota'"));
					if($cota==0)
					{
						/*'$this->cant_ejemplar',*/
						$fecha = date('d-m-Y');
						$ejemplar=pg_query("insert into libro values ('$this->id_libro','$this->id','$this->isbn','$this->titulo','$this->cota','$this->cant_ejemplar','$this->edicion','$this->paginas','$this->anio','$this->observacion','$this->estatus','$this->sala','$this->areas','$this->editoria','$this->cant_inactivos','$fecha')");
						
						$autores=pg_query("insert into autor values ('$this->id_libro','$this->autor1','$this->autor2','$this->autor3')");
						$this->mensaje=1;
						$this->titulo='';
						$this->edicion='';
						$this->isbn='';
						$this->anio='';
						$this->cota='';
						$this->paginas='';
						$this->editoria='';
						$this->areas='0';
						$this->autor1='';
						$this->autor2='';
						$this->autor3='';
						$this->sala='0';
						$this->cant_ejemplar='';
						$this->estatus='0';
						$this->observacion='';
					}//fin de cota
					else
					{
						$this->mensaje=3;
					}
				}//fin de isbn
				else
				{
					$this->mensaje=2;
				}
			}//fin de campos en blancos
			else
			{
				$this->mensaje=4;
			}
		}//fin de function
		
		public function actualizar_ejemplar($id, $isbn_n, $cota_n, $titulo, $edicion, $isbn, $anio, $cota, $paginas, $autor1, $autor2, $autor3, $sala, $cant_ejemplar, $estatus, $observacion, $categoria, $editorial)
		{
			$this->id=$id;
			$this->isbn_n=$isbn_n;
			$this->cota_n=$cota_n;
			
			$this->titulo=$titulo;
			$this->edicion=$edicion;
			$this->isbn=$isbn;
			$this->anio=$anio;
			$this->cota=$cota;
			$this->paginas=$paginas;
			$this->autor1=$autor1;
			$this->autor2=$autor2;
			$this->autor3=$autor3;
			$this->sala=$sala;
			$this->cant_ejemplar=$cant_ejemplar;
			$this->estatus=$estatus;
			$this->observacion=$observacion;
			$this->categoria=$categoria;
			$this->editorial=$editorial;
			
			if(($this->titulo!='') && ($this->edicion!='') && ($this->isbn!='') && ($this->anio!='') && ($this->cota!='') && ($this->paginas!='') && ($this->autor1!='') && ($this->autor2!='') && ($this->autor3!='') && ($this->sala!='0') && ($this->cant_ejemplar!='') && ($this->estatus!='0') &&($this->observacion!='')  && ($this->categoria!='0') && ($this->editorial!='0'))
			{
				$isbn = pg_num_rows(pg_query("select isbn from libro where isbn='$this->isbn' and isbn <> '$this->isbn_n'"));
				
				if($isbn==0)
				{
					$cota = pg_num_rows(pg_query("select cota from libro where cota='$this->cota' and cota <> '$this->cota_n'"));
					if($cota==0)
					{	
						$ejemplar=pg_query("update libro set isbn='$this->isbn', titulo='$this->titulo', cota='$this->cota', ejemplares='$this->cant_ejemplar', edicion='$this->edicion', pag_vol='$this->paginas', anio='$this->anio', observaciones='$this->observacion', estatus='$this->estatus', sala='$this->sala', id_area='$this->categoria', id_editorial='$this->editorial' where id_libro='$this->id'");	
						$autores=pg_query("update autor set autor1='$this->autor1', autor2='$this->autor2', autor3='$this->autor3' where id_libro='$this->id'");
						
						echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
						print"<script>	$(location).attr('href','http:./ejemplares.php');</script>";
					}
					else
					{
						$this->mensaje=1;
					}
					
				}
				else
				{
					$this->mensaje=2;
				}
			}//fin de campos
			else
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function actualizar_ejemplar_cantidad($id_libro, $ejemplares, $ejemplares_inactivos)
		{
			$this->id_libro=$id_libro;
			$this->ejemplares=$ejemplares;
			$this->ejemplares_inactivos=$ejemplares_inactivos;
			
			if(($this->ejemplares!='') && ($this->ejemplares_inactivos!=''))
			{
				$libro = pg_num_rows(pg_query("select id_libro from libro where id_libro='$this->id_libro'"));
				
				if($libro==1)
				{	
						$ejemplar=pg_query("update libro set ejemplares='$this->ejemplares', 
						cant_inactivos='$this->ejemplares_inactivos' where id_libro='$this->id_libro'");
							
						echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
						print"<script>	$(location).attr('href','http:./ejemplares.php');</script>";
				}//if libro
				else
				{
					$this->mensaje=1;
				}
			}//fin de campos
			else
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function datos_ejemplar ($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./ejemplares.php');</script>";
			}
			else
			{
				$query=pg_query("SELECT	 libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala, 
				                         libro.cant_inactivos, area.id_area, area.codigo, area.area, editorial.id_editorial,
										 editorial.nombre, editorial.ciudad, editorial.pais, autor.autor1, autor.autor2, 
										 autor.autor3
                                        
										 FROM libro, area, editorial, autor

                                         WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro and libro.id_libro='$id'");						
				$reg=pg_fetch_object($query);

				$this->id_libro=$reg->id_libro;
				$this->titulo=$reg->titulo;
				$this->edicion=$reg->edicion;
				$this->isbn=$reg->isbn;
				$this->anio=$reg->anio;
				$this->cota=$reg->cota;
				$this->paginas=$reg->pag_vol;
				$this->cant_ejemplar=$reg->ejemplares;
				$this->cant_inactivos=$reg->cant_inactivos;
				$this->sala=$reg->sala;
				$this->estatus=$reg->estatus;
				$this->observacion=$reg->observaciones;
				$this->id_area=$reg->id_area;
				$this->id_editorial=$reg->id_editorial;
				$this->area=$reg->area;
				$this->editorial=$reg->nombre;
				$this->autor1=$reg->autor1;
				$this->autor2=$reg->autor2;
				$this->autor3=$reg->autor3;					
			}
		}//fin de function
		
		public function listado_ejemplares()
		{
			$this->ejemplares= pg_query("SELECT	
			                             libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala,                                         libro.cant_inactivos, area.codigo, area.area, editorial.nombre, editorial.ciudad, 
										 editorial.pais, autor.autor1, autor.autor2, autor.autor3
                                         FROM libro, area, editorial, autor 
										 WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro order by libro.id_libro desc");
		}//fin de function
		
		public function ejemplares_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM libro");
			$this->libros = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function autores_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*)*3 FROM autor");
			$this->autores = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function auditoria_ejemplar()
		{	
			
			$this->auditoria= pg_query("SELECT	
			                             libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, libro.edicion,                                         libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, libro.sala,                                         libro.cant_inactivos, libro.fecha, area.codigo, area.area, bibliotecario.nombre, 
										 bibliotecario.apellido FROM libro, area, editorial, autor, bibliotecario
										 WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and                                         libro.id_libro=autor.id_libro and bibliotecario.id_bibliotecario =libro.id_bibliotecario
										 order by libro.id_libro desc");
		}//fin de function
		
		public function reporte_ejemplares_fecha_rango($desde,$hasta,$tipo)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->tipo=$tipo;
			
			if($this->tipo!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->tipo=="Todos")
				{ 
					$this->consulta_e = pg_query("SELECT libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, 
												  libro.edicion, libro.pag_vol, libro.anio, libro.observaciones, libro.estatus,                                                  libro.sala, libro.cant_inactivos, libro.fecha, area.codigo, area.area, 
												  editorial.nombre, editorial.ciudad,
												  editorial.pais, autor.autor1, autor.autor2, autor.autor3
												  FROM libro, area, editorial, autor
												  WHERE libro.id_editorial=editorial.id_editorial 
												  and libro.id_area=area.id_area 
												  and fecha BETWEEN '$this->desde' AND '$this->hasta'
												  and libro.id_libro=autor.id_libro order by libro.id_libro desc");
					$this->valido = pg_num_rows ($this->consulta_e);
				}
				else
				{
					$this->consulta_e = pg_query("SELECT libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, 
												  libro.edicion, libro.pag_vol, libro.anio, libro.observaciones, libro.estatus,                                                  libro.sala, libro.cant_inactivos, libro.fecha, area.codigo, area.area, 
												  editorial.nombre, editorial.ciudad,
												  editorial.pais, autor.autor1, autor.autor2, autor.autor3
												  FROM libro, area, editorial, autor
												  WHERE libro.id_editorial=editorial.id_editorial 
												  and libro.id_area=area.id_area 
												  and libro.id_area='$this->tipo'
												  and fecha BETWEEN '$this->desde' AND '$this->hasta'
												  and libro.id_libro=autor.id_libro order by libro.id_libro desc");
					$this->valido = pg_num_rows ($this->consulta_e);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		
	}//fin de class
	?>