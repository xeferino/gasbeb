<?php
	class Biografia
	{
			public $titulo;
			public $autor;
			public $contenido;
			public $imagen;
			public $mensaje;
		
		public function inicializar_datos ($titulo, $autor, $contenido, $imagen)
			{
				$this->titulo=$titulo;
				$this->autor=$autor;
				$this->contenido=$contenido;
				$this->imagen=$imagen;
				$this->mensaje=$mensaje;
			}//fin de function
			
		public function registrar_biografia($id, $ruta, $fecha)
		{
			$this->id=$id;
			$this->ruta=$ruta;
			$this->fecha=$fecha;
			
			if( ($this->titulo !='') && ($this->autor !='') && ($this->contenido !=''))
			{
				$id= pg_num_rows(pg_query("select id_biografia from biografias"));
						if($id==0){ $id=1;} else {$id=$id+1;}
						
				$destino =  "public/biografias/"."biografia-".$id.".JPEG";
																		
				$biografia = pg_query("insert into biografias values ('$id','$this->id','$this->titulo',
				'$this->autor','$this->contenido','$destino','$this->fecha')");
											
				$destino =  "../public/biografias/"."biografia-".$id.".JPEG";
				$copiar = copy($this->ruta,$destino);
				
				$this->titulo='';
				$this->autor='';
				$this->contenido='';
				$this->mensaje=1;
			}//fin de campos blancos
			else 
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function actualizar_biografia($id, $ruta)
		{
			$this->id=$id;
			$this->ruta=$ruta;
			if( ($this->titulo !='') && ($this->autor !='') && ($this->contenido !=''))
			{
							
				$destino =  "public/biografias/"."biografia-".$id.".JPEG";
																					
				$biografia = pg_query("update biografias set titulo='$this->titulo', autor='$this->autor', contenido='$this->contenido',foto='$destino' where id_biografia='$this->id'");
											
				$destino =  "../public/biografias/"."biografia-".$id.".JPEG";
				$copiar = copy($this->ruta,$destino);
					
					$this->titulo='';
					$this->autor='';
					$this->contenido='';
					echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./biografias.php');</script>";
					$this->mensaje=1;
			}//fin de campos blancos
			else 
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function listado_biografias()
		{	
			
			$this->consulta= pg_query("select * from biografias order by id_biografia asc;");
		}//fin de function
		
		public function datos_biografia ($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./biografias.php');</script>";
			}
			else
			{
				$query=pg_query("select *from biografias where id_biografia='$this->id'");
				$this->reg=pg_fetch_object($query);
				$this->titulo=$this->reg->titulo;
				$this->autor=$this->reg->autor;
				$this->contenido=$this->reg->contenido;
				$this->imagen=$this->reg->foto;
			}
		}//fin de function
		
		public function reporte_biografia_fecha_rango($desde,$hasta,$titulo)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->titulo=$titulo;
			
			if($this->titulo!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->titulo=="Todos")
				{ 
					$this->consulta = pg_query("SELECT *FROM biografias WHERE fecha BETWEEN '$this->desde' AND '$this->hasta'");
					$this->valido = pg_num_rows ($this->consulta);
				}
				else
				{
					$this->consulta= pg_query("SELECT *FROM biografias WHERE fecha BETWEEN '$this->desde' AND '$this->hasta' AND titulo='$this->titulo'");
					$this->valido = pg_num_rows ($this->consulta);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		
		public function auditoria_biografia()
		{	
			
			$this->auditoria= pg_query("select  * from bibliotecario, biografias where 
			                            bibliotecario.id_bibliotecario=biografias.id_bibliotecario");
		}//fin de function

	}//fin de class
	?>