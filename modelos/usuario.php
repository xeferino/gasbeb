<?php
	class Usuario
	{
			public $cedula;
			public $primer_nombre;
			public $segundo_nombre;
			public $primer_apellido;
			public $segundo_apellido;
			public $sexo;
			public $correo;
			public $ocupacion;
			public $tipo;
			public $programa;
  			public $discacidad;
			public $fecha_nac;
			public $referencia;
			public $direccion1;
			public $direccion2;
			public $telefono1;
			public $telefono2;
			public $fecha;
  			public $codigo_carnet;
			
			public $mensaje;
		
		public function inicializar_datos ($cedula, $primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $sexo,$correo, $ocupacion, $tipo, $programa, $discapacidad, $fecha_nac, $referencia, $direccion1, $direccion2, $telefono1, $telefono2, $fecha, $codigo_carnet)
			{
				$this->cedula=$cedula;
				$this->primer_nombre=$primer_nombre;
				$this->segundo_nombre=$segundo_nombre;
				$this->primer_apellido=$primer_apellido;
				$this->segundo_apellido=$segundo_apellido;
				$this->sexo=$sexo;
				$this->correo=$correo;
				$this->ocupacion=$ocupacion;
				$this->tipo=$tipo;
				$this->programa=$programa;
				$this->discapacidad=$discapacidad;
				$this->fecha_nac=$fecha_nac;
				$this->referencia=$referencia;
				$this->direccion1=$direccion1;
				$this->direccion2=$direccion2;
				$this->telefono1=$telefono1;
				$this->telefono2=$telefono2;
				$this->fecha=$fecha;
				$this->codigo_carnet=$codigo_carnet;
				
				$this->mensaje=$mensaje;
			}//fin de function
	
		public function registrar_usuario($id)
		{
			$this->id=$id;
			if( ($this->cedula !='') && ($this->primer_nombre !='') && ($this->segundo_nombre !='') && ($this->primer_apellido !='') && ($this->segundo_apellido !='') && ($this->sexo !='0') && ($this->correo !='') && ($this->ocupacion !='') && ($this->tipo !='0') && ($this->fecha_nac !='') && ($this->referencia !='') && ($this->direccion1 !='') && ($this->direccion2 !='') && ($this->telefono1 !='') && ($this->telefono2 !=''))
			{
				$cedula = pg_num_rows (pg_query("select *from usuario where cedula='$this->cedula'"));
				if($cedula==0)
				{
					$this->codigo_carnet = pg_num_rows (pg_query("select codigo_carnet from usuario"));
					if($this->codigo_carnet == 0){$this->codigo_carnet=1;}else{$this->codigo_carnet=$this->codigo_carnet+1;}
					
					$id_usuario = pg_num_rows (pg_query("select id_usuario from usuario"));
					if($id_usuario == 0){$id_usuario=1;}else{$id_usuario=$id_usuario+1;}
					
					$usuario = pg_query("insert into usuario values ('$id_usuario', '$this->id', '$this->cedula', 
					'$this->primer_nombre', '$this->segundo_nombre', '$this->primer_apellido', '$this->segundo_apellido',
					 '$this->sexo', '$this->correo', '$this->ocupacion', '$this->tipo', '$this->programa', '$this->discapacidad', 
					 '$this->fecha_nac', '$this->referencia', '$this->direccion1', '$this->direccion2', '$this->telefono1', 
					 '$this->telefono2', '$this->fecha', '$this->codigo_carnet')");
						
						$this->cedula='';
						$this->primer_nombre='';
						$this->segundo_nombre='';
						$this->primer_apellido='';
						$this->segundo_apellido='';
						$this->sexo='0';
						$this->correo='';
						$this->ocupacion='';
						$this->tipo='0';
						$this->programa='';
						$this->discapacidad='';
						$this->fecha_nac='';
						$this->referencia='';
						$this->direccion1='';
						$this->direccion2='';
						$this->telefono1='';
						$this->telefono2='';
						
						$this->mensaje=1;
				}//fin de consulta cedula
				else 
				{
					$this->mensaje=2;
				}
			}//fin de campos blancos
			else 
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function actualizar_usuario($id, $ci)
		{
			$this->id=$id;
			$this->ci=$ci;
			
			if( ($this->cedula !='') && ($this->primer_nombre !='') && ($this->segundo_nombre !='') && ($this->primer_apellido !='') && ($this->segundo_apellido !='') && ($this->sexo !='') && ($this->correo !='') && ($this->ocupacion !='') && ($this->tipo !='') && ($this->fecha_nac !='') && ($this->referencia !='') && ($this->direccion1 !='') && ($this->direccion2 !='') && ($this->telefono1 !='') && ($this->telefono2 !=''))
			{
				$cedula = pg_num_rows (pg_query("select cedula from usuario where cedula='$this->cedula' and cedula <> '$this->ci'"));
				if($cedula==0)
				{
					$actualizar = pg_query("update usuario set cedula='$this->cedula', primer_nombre='$this->primer_nombre', segundo_nombre='$this->segundo_nombre', primer_apellido='$this->primer_apellido', segundo_apellido='$this->segundo_apellido', sexo='$this->sexo', correo='$this->correo', ocupacion='$this->ocupacion', tipo='$this->tipo', programa='$this->programa', discapacidad='$this->discapacidad', fecha_nac='$this->fecha_nac', referencia='$this->referencia', direccion1='$this->direccion1', direccion2='$this->direccion2', telefono1='$this->telefono1', telefono2='$this->telefono2' where id_usuario='$this->id'");
					
						echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
						print"<script>	$(location).attr('href','http:./usuarios.php');</script>";
				}//fin de consulta cedula
				else 
				{
					$this->mensaje=1;
				}
			}//fin de campos blancos
			else 
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function listado_usuario()
		{	
			
			$this->consulta= pg_query("select * from usuario ORDER BY id_usuario asc;");
		}//fin de function
		
		public function datos_usuario ($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./usuarios.php');</script>";
			}
			else
			{
				$query=pg_query("select *from usuario where id_usuario='$id'");
				$this->reg=pg_fetch_object($query);
					$this->cedula=$this->reg->cedula;
					$this->primer_nombre=$this->reg->primer_nombre;
					$this->segundo_nombre=$this->reg->segundo_nombre;
					$this->primer_apellido=$this->reg->primer_apellido;
					$this->segundo_apellido=$this->reg->segundo_apellido;
					$this->sexo=$this->reg->sexo;
					$this->correo=$this->reg->correo;
					$this->ocupacion=$this->reg->ocupacion;
					$this->tipo=$this->reg->tipo;
					$this->programa=$this->reg->programa;
					$this->discapacidad=$this->reg->discapacidad;
					$this->fecha_nac=$this->reg->fecha_nac;
					$this->referencia=$this->reg->referencia;
					$this->direccion1=$this->reg->direccion1;
					$this->direccion2=$this->reg->direccion2;
					$this->telefono1=$this->reg->telefono1;
					$this->telefono2=$this->reg->telefono2;
					$this->fecha=$this->reg->fecha;
					$this->carnet=$this->reg->codigo_carnet;
			}
			
		}//fin function	
		
		public function usuarios_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM usuario");
			$this->recientes = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function reporte_usuarios_fecha_rango($desde,$hasta,$tipo)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->tipo=$tipo;
			
			if($this->tipo!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->tipo=="Todos")
				{ 
					$this->consulta_u = pg_query("SELECT *FROM usuario WHERE fecha BETWEEN '$this->desde' AND '$this->hasta'");
					$this->valido = pg_num_rows ($this->consulta_u);
				}
				else
				{
					$this->consulta_u = pg_query("SELECT *FROM usuario WHERE fecha BETWEEN '$this->desde' AND '$this->hasta' AND tipo='$this->tipo'");
					$this->valido = pg_num_rows ($this->consulta_u);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		
		public function auditoria_usuario()
		{	
			
			$this->auditoria= pg_query("select  * from bibliotecario, usuario where 
			                            bibliotecario.id_bibliotecario=usuario.id_bibliotecario");
		}//fin de function
		
		public function usuarios_sancionados()
		{	
			$this->usuario_sancionados=pg_query("select * from usuario, prestamo, sancion where usuario.id_usuario=prestamo.id_usuario and prestamo.id_prestamo=sancion.id_prestamo");
		}//fin de funcion
		
		public function reactivacion_usuarios ()
	{
		$hoy = date("Y-m-d");
		
		$suspendidos = pg_query("select * from usuario, prestamo, sancion where 		              
				          usuario.id_usuario=prestamo.id_usuario and prestamo.id_prestamo=sancion.id_prestamo 
				          and sancion.estatus='Suspendido' and fecha_fin='$hoy'");
						  
		$usuarios_sancionados = pg_num_rows ($suspendidos);
		if ($usuarios_sancionados>0)
		{
			while($reg=pg_fetch_array($suspendidos))
    		{
				$id = $reg['id_sancion']; //Guardar id sancion
				$usuario = $reg['id_usuario']; //Guardar sus cedulas
				$fecha_reactivacion = $reg['fecha_fin'];
				$fecha_hoy = date("d-m-Y");
				  
  				$fecha_hoy = explode ("-", $fecha_hoy); 
  				$diaprimera   = $fecha_hoy[0];  
  				$mesprimera = $fecha_hoy[1];  
 				$anyoprimera  = $fecha_hoy[2];


				$fecha_reactivacion = explode ("-", $fecha_reactivacion); 
  				$diaSegunda   = $fecha_reactivacion[0];  
  				$mesSegunda = $fecha_reactivacion[1];  
 				$anyoSegunda  = $fecha_reactivacion[2];
				
  				$fecha_hoy = gregoriantojd($mesprimera, $diaprimera, $anyoprimera); 
  				$fecha_reactivacion = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);     
				
				$validar_fechas = $fecha_reactivacion - $fecha_hoy;
				
				if($validar_fechas <= 0)
				{
					$sql_activar = "update sancion set estatus='ACTIVO' where id_sancion='$id'";
					$consulta_activar  = pg_query($sql_activar);
			
				}
			}//fin de while
		}//fin de validar sancionados
	}//fin function

	}//fin de class
	?>