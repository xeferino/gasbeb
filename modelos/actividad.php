<?php
	class Actividad
	{
			public $nombre;
			public $descripcion;
			public $fecha_inicio;
			public $fecha_fin;
			public $hora_inicio;
			public $hora_fin;
			public $estatus;
			public $mensaje;
		
		public function inicializar_datos ($nombre, $descripcion, $fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $estatus)
			{
				$this->nombre=$nombre;
				$this->descripcion=$descripcion;
				$this->fecha_inicio=$fecha_inicio;
				$this->fecha_fin=$fecha_fin;
				$this->hora_inicio=$hora_inicio;
				$this->hora_fin=$hora_fin;
				$this->estatus=$estatus;
				$this->mensaje=$mensaje;
			}//fin de function
			
		public function registrar_actividad($id)
		{
			$this->id=$id;
			if( ($this->nombre !='') && ($this->descripcion !='') && ($this->fecha_inicio !='') && ($this->fecha_fin !='') && ($this->hora_inicio !='') && ($this->hora_fin !='') && ($this->estatus !='0'))
			{
				/*if ($this->fecha_inicio>$this->fecha_fin)
				{
					/*$valida_actividad= pg_num_rows (pg_query("select id_actividad from actividad where estatus='Iniciada' and id_actividad='$id'"));
					
					if ($valida_actividad==0)
					{*/
						$fecha_reg= date('d-m-Y-h:i:s');
						$actividad = pg_query("insert into actividad values (default,'$this->id','$this->nombre','$this->descripcion','$this->fecha_inicio','$this->fecha_fin','$this->hora_inicio','$this->hora_fin','$this->estatus','$fecha_reg')");
						$this->mensaje=1;
						$this->nombre='';
						$this->descripcion='';
						$this->fecha_inicio='';
						$this->fecha_fin='';
						$this->hora_inicio='';
						$this->hora_fin='';
						$this->estatus='0';
					/*}
					else
					{
						$this->mensaje=3;	
					}
				}//fin fechas
				else
				{
					$this->mensaje=3;
				}*/
			}//fin de campos blancos
			else 
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function actualizar_actividad($id)
		{
			$this->id=$id;
			if(($this->nombre !='') && ($this->descripcion !='') && ($this->fecha_inicio !='') && ($this->fecha_fin !='') && ($this->hora_inicio !='') && ($this->hora_fin !='') && ($this->estatus !='0'))
			{
				$fecha_act= date('d-m-Y-h:i:s');
				$actualizar = pg_query("update actividad set nombre_a='$this->nombre', descripcion='$this->descripcion', fecha_inicio='$this->fecha_inicio', fecha_fin='$this->fecha_fin', hora_inicio='$this->hora_inicio', hora_fin='$this->hora_fin', estatus='$this->estatus', fecha_act='$fecha_act' where id_actividad='$this->id'");
				$this->mensaje=1;
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./actividades.php');</script>";

			}//fin de campos blancos
			else 
			{
				$this->mensaje=2;
			}
		}//fin de function
		
		public function listado_actividad()
		{	
			
			$this->consulta= pg_query("select * from actividad ORDER BY id_actividad desc");
		}//fin de funcion
		
		public function datos_actividad($id)
		{
			$this->id=$id;
			if($this->id==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./actividades.php');</script>";
			}
			else
			{
				$query=pg_query("select *from actividad where id_actividad='$id'");
				$this->reg=pg_fetch_object($query);
					$this->nombre=$this->reg->nombre_a;
					$this->descripcion=$this->reg->descripcion;
					$this->fecha_inicio=$this->reg->fecha_inicio;
					$this->fecha_fin=$this->reg->fecha_fin;
					$this->hora_inicio=$this->reg->hora_inicio;
					$this->hora_fin=$this->reg->hora_fin;
					$this->estatus=$this->reg->estatus;
					
			}
		}//fin de function
		
		public function actividad_finalizar($id)
		{	
			$this->id=$id;
			$this->finalizada = pg_num_rows (pg_query("select id_actividad from actividad where estatus='Iniciada' and id_actividad='$id'"));
			
			if($this->finalizada==1)
			{
				$actualizar = pg_query("update actividad set estatus='Finalizada' where id_actividad='$id'");
			}
			
		}//fin de function
		
		public function actividad_suspender($id)
		{	
			$this->id=$id;
			$this->suspendida = pg_num_rows (pg_query("select id_actividad from actividad where estatus='Iniciada' and id_actividad='$id'"));
			
			if($this->suspendida==1)
			{
				$actualizar = pg_query("update actividad set estatus='Suspendida' where id_actividad='$id'");
			}
			
		}//fin de function
		
		public function actividades_recientes()
		{	
			$fecha=date('Y-m-d');
			$consulta= pg_query("SELECT count(*) FROM actividad");
			$this->recientes = pg_fetch_array($consulta);
		}//fin de funcion
		
		public function actividades_calendario()
		{	
			$actividades= pg_query("SELECT id_actividad, nombre_a, fecha_inicio, fecha_fin, hora_inicio, hora_fin FROM actividad");
			$this->calendario = pg_fetch_assoc($actividades);
		}//fin de funcion
		
		public function reporte_actividades_fecha_rango($desde,$hasta, $estatus)
		{	
			$this->desde=$desde;
			$this->hasta=$hasta;
			$this->estatus=$estatus;
			
			if($this->estatus!='0' && $this->desde!='' && $this->hasta!='')
			{
				if($this->estatus=="Todas")
				{ 
					$this->consulta_a = pg_query("SELECT *FROM actividad WHERE fecha_inicio BETWEEN '$this->desde' AND '$this->hasta'");
					$this->valido = pg_num_rows ($this->consulta_a);
				}
				else
				{
					$this->consulta_a = pg_query("SELECT *FROM actividad WHERE fecha_inicio BETWEEN '$this->desde' AND '$this->hasta' AND estatus='$this->estatus'");
					$this->valido = pg_num_rows ($this->consulta_a);
				}
			}
			else
			{
				$this->mensaje=1;
			}
		}//fin de funcion
		public function auditoria_actividad()
		{	
			
			$this->auditoria= pg_query("select  * from bibliotecario, actividad where 
			                            bibliotecario.id_bibliotecario=actividad.id_bibliotecario");
		}//fin de function
	}//fin de class
	?>