<?php
	class Bibliotecario
	{
			public $cedula;
			public $nombre;
			public $apellido;
			public $direccion;
			public $telefono;
			public $sexo;
			public $perfil;
			public $estatus;
			public $usuario;
			public $clave;
			public $mensaje;
		
		public function inicializar_datos ( $nombre, $apellido, $cedula, $perfil, $direccion, $telefono, $sexo, $usuario, $clave, $estatus)
			{
				$this->nombre=$nombre;
				$this->apellido=$apellido;
				$this->cedula=$cedula;
				$this->perfil=$perfil;
				$this->direccion=$direccion;
				$this->telefono=$telefono;
				$this->sexo=$sexo;
				$this->usuario=$usuario;
				$this->clave=$clave;
				$this->estatus=$estatus;
				$this->mensaje=$mensaje;
			}//fin de function
			
		public function iniciar_sesion ()
		{	
			if(( $this->usuario !='' ) && ( $this->clave !='' ))
			{
				$clave = md5($this->clave);
				$consulta= pg_query("select *from bibliotecario where cuenta='$this->usuario' and clave='$clave'");
				$row = pg_num_rows ($consulta);
				if($row>0)
				{	
					$reg=pg_fetch_object($consulta);
	
					if($reg->estatus == "Activo")
					{	
						$_SESSION['perfil']=$reg->tipo;
						$_SESSION['nombre']=$reg->nombre;
						$_SESSION['apellido']=$reg->apellido;
						$_SESSION['conectado'] = $reg->id_bibliotecario;
						header("Location: ./vistas/");
					}
					else 
					{
						$this->mensaje=1;
					}
				}
				else 
				{
					$this->mensaje=2;
				}
			}
			else 
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function cerrar_sesion()
		{
			session_destroy();
			header("Location: ./../");
		}//fin de function
	
		public function registrar_bibliotecario()
		{
			if( ($this->nombre !='') && ($this->apellido !='') && ($this->cedula !='') && ($this->perfil !='0') && ($this->direccion !='') && ($this->telefono !='') && ($this->sexo !='0') && ($this->usuario !='') && ($this->clave !='') && ($this->estatus !='0'))
			{
				$cedula = pg_num_rows (pg_query("select *from bibliotecario where cedula='$this->cedula'"));
				if($cedula==0)
				{
					$usuario = pg_num_rows (pg_query("select *from bibliotecario where cuenta='$this->usuario'"));
					if($usuario==0)
					{
					$clave = md5($this->clave);
					$fecha = date ('d-m-y');		
					$bibliotecario = pg_query("insert into bibliotecario values (default,'$this->nombre','$this->apellido','$this->cedula','$this->perfil','$this->direccion','$this->telefono','$this->sexo','$this->usuario','$clave','$this->estatus','$fecha')");
					
					$this->nombre='';
					$this->apellido='';
					$this->cedula='';
					$this->perfil='0';
					$this->direccion='';
					$this->telefono='';
					$this->sexo='0';
					$this->usuario='';
					$this->clave='';
					$this->estatus='0';
					
					$this->mensaje=1;
					
					}//fin consulta cuenta de usuario
					else 
					{
						$this->mensaje=2;
					}
				}//fin de consulta cedula
				else 
				{
					$this->mensaje=3;
				}
			}//fin de campos blancos
			else 
			{
				$this->mensaje=4;
			}
		}//fin de function
		
		public function actualizar_bibliotecario($id, $ci, $user)
		{
			$this->id=$id;
			$this->ci=$ci;
			$this->user=$user;
			if( ($this->nombre !='') && ($this->apellido !='') && ($this->cedula !='') && ($this->perfil !='0') && ($this->direccion !='') && ($this->telefono !='') && ($this->sexo !='0') && ($this->usuario !='') && ($this->clave !='') && ($this->estatus !='0'))
			{
				$cedula = pg_num_rows (pg_query("select cedula from bibliotecario where cedula='$this->cedula' and cedula <> '$this->ci'"));
				if($cedula==0)
				{
					$usuario = pg_num_rows (pg_query("select cuenta from bibliotecario where cuenta='$this->usuario' and cuenta <> '$this->user'"));
					if($usuario==0)
					{
						$clave = md5($this->clave);
						$actualizar = pg_query("update bibliotecario set nombre='$this->nombre', apellido='$this->apellido', cedula='$this->cedula', tipo='$this->perfil', direccion='$this->direccion', telefono='$this->telefono', sexo='$this->sexo', cuenta='$this->usuario', clave='$clave', estatus='$this->estatus' where cuenta='$this->user' and id_bibliotecario='$this->id'");
						echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
						print"<script>	$(location).attr('href','http:./bibliotecarios.php');</script>";
					}//fin consulta cuenta de usuario
					else 
					{
						$this->mensaje=2;
					}
				}//fin de consulta cedula
				else 
				{
					$this->mensaje=3;
				}
			}//fin de campos blancos
			else 
			{
				$this->mensaje=4;
			}
		}//fin de function
		
		public function listado_bibliotecario()
		{	
			//$this->verificacion= pg_num_rows (pg_query("select * from bibliotecario"));
			$this->consulta= pg_query("select * from bibliotecario ORDER BY id_bibliotecario asc;");
		}//fin de function
		
		public function desactivar_bibliotecario($id_bibliotecario)
		{	
			$this->id_bibliotecario=$id_bibliotecario;
			$this->consulta = pg_num_rows (pg_query("select id_bibliotecario from bibliotecario where id_bibliotecario='$id_bibliotecario' and estatus='Activo'"));
			if($this->consulta==1)
			{
				$desactivar = pg_query("update bibliotecario set estatus='Inactivo' where id_bibliotecario='$id_bibliotecario'");
			}
			else
			{
				$activar = pg_query("update bibliotecario set estatus='Activo' where id_bibliotecario='$id_bibliotecario'");
			}
		}//fin de function
		
		public function datos_bibliotecario ($id_bibliotecario)
		{
			$this->id_bibliotecario=$id_bibliotecario;
			if($this->id_bibliotecario==''){
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:./bibliotecarios.php');</script>";
			}
			else
			{
				$query=pg_query("select *from bibliotecario where id_bibliotecario='$id_bibliotecario'");
				$this->reg=pg_fetch_object($query);
					$this->nombre=$this->reg->nombre;
					$this->apellido=$this->reg->apellido;
					$this->cedula=$this->reg->cedula;
					$this->perfil=$this->reg->tipo;
					$this->direccion=$this->reg->direccion;
					$this->telefono=$this->reg->telefono;
					$this->sexo=$this->reg->sexo;
					$this->usuario=$this->reg->cuenta;
					$this->clave=$this->reg->clave;
					$this->estatus=$this->reg->estatus;
			}
			
		}//fin function	
		
		public function datos_acceso($id_bibliotecario)
		{
			$this->id_bibliotecario=$id_bibliotecario;
			if($this->id_bibliotecario==''){
			
				echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
				print"<script>	$(location).attr('href','http:././');</script>";
			}
			else
			{
				$query=pg_query("select cuenta, id_bibliotecario from bibliotecario where id_bibliotecario='$id_bibliotecario'");
				$this->reg=pg_fetch_object($query);
					$this->usuario=$this->reg->cuenta;
			}
		}//fin de function
		
		public function actualizar_datos_acceso($usuario, $clave_a, $clave_n, $user, $id_bibliotecario)
		{
			$this->usuario=$usuario;
			$this->clave_a=$clave_a;
			$this->clave_n=$clave_n;
			$this->user=$user;
			$this->id_bibliotecario=$id_bibliotecario;
			
			if($this->id_bibliotecario=='')
			{
				print"<script>	$(location).attr('href','http:././');</script>";
			}
			else
			{	
				if (($this->usuario!='') && ($this->clave_a!='') && ($this->clave_n!=''))
				{
					$cuenta = pg_num_rows (pg_query("select *from bibliotecario where cuenta='$this->usuario' and cuenta <> '$this->user'"));
					if($cuenta==0)
						{
							$clave_a = md5($this->clave_a);
							$claves = pg_num_rows (pg_query("select clave from bibliotecario where clave='$clave_a'"));
							if($claves==1)
							{
								$clave_n = md5($this->clave_n);
								$actualizar= pg_query("update bibliotecario set cuenta='$this->usuario', clave='$clave_n' where id_bibliotecario='$this->id_bibliotecario'");
								
									$this->mensaje=1;
									echo'<div align="center" style="margin-top:4px;"><img src="../public/img/cargar.gif" width="32" height="32"> Redireccionando...</div>';
									print"<script>	$(location).attr('href','http:./bibliotecarios.php');</script>";

							}
							else
							{	
									$this->mensaje=2;
							}
						}
						else
						{	
							$this->mensaje=3;
						}
					}
					else
					{
						$this->mensaje=4;
					}
			}//fin de else verificacion
		}//fin de function

	}//fin de class
	?>