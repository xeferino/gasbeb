<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GASBEB|Error-404</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="public/bootstrap/css/bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">
<style>.page{background-color:#ecf0f5;}</style>
</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
  <!-- Left side column. contains the logo and sidebar -->

    <!-- Main content -->
    <div class="page">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        404 Error en P&aacute;gina
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Uy! P&aacute;gina no Encontrada.</h3>

          <p>
            Ponganse en contacto con el admistrador del sistema, si el error persiste!. <a href="./"> Entrar al Sistema </a>
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content -->
<!-- ./wrapper -->
<!-- jQuery 2.2.0 -->
<script src="public/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="public/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>