<?php
if(isset($_POST['hasta']) && isset($_POST['desde']) && isset($_POST['tipo']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");
	include("./../public/plugins/fpdf/fpdf.php");

	class PDF extends FPDF 
	{ 
	
		function Header() 
		{ 		
			
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
			
			//Logo
			$this->Image('./../public/img/bibliotecanacional.jpg',120,10,40,15);
			//Arial bold 15
			
			$this->SetFont('Arial','B',14); 
			//Movernos a la derecha 
			$this->Cell(30); 
			//T�tulo 
			$this->Ln(2); 
			
			$this->SetFont('Arial','B',10); 
			$this->Cell(36);
			
			$this->Text(105,30,'REP�BLICA BOLIVARIANA DE VENEZUELA');
	
			$this->Text(112,36.5,'BIBLIOTECA "EDUARDO BLANCO"');
	
			$this->Text(98,43,'MUNICIPIO RIBERO - CARIACO � ESTADO � SUCRE');
			
			$this->Text(220,52,$fecha_hoy);

			$this->SetFont('Arial','',6); 
		
			$this->SetFont('Arial','',10); 
			//Salto de l�nea 
			$this->Ln(10); 
			$this->SetY(60);
		} 
	
		//Pie de p�gina 
		function Footer() 
		{ 
		   //$fecha=date("d/m/Y");
		  //Posicion
			$this->SetY(-35.8); 
			//Arial italic 8 
			$this->SetFont('Arial','I',10); 
			//N�mero de p�gina 
			$this->Cell(0,30,'P�gina '.$this->PageNo().'/{nb}',0,0,'C'); 
			$this->Text(148,259.5,''); 
			$this->Text(180,259.5,'');
		} 
	
		function __construct() 
		{        
			//Llama al constructor de su clase Padre. 
			//Modificar aka segun la forma del papel del reporte 
			parent::__construct('L','mm','Letter'); 
		}
	}// fin de class
			
    //Creaci�n del objeto de la clase heredada 
		$pdf=new PDF(); 
		$pdf->SetTopMargin(10.8); 
		$pdf->SetLeftMargin(10.5);     
		$pdf->AliasNbPages(); 
		$pdf->SetFont('Arial','B',10);
		
				$desde =  $_POST['desde'];
				$hasta =  $_POST['hasta'];
				$tipo =  $_POST['tipo'];
				
				if($tipo=="Todos")
				{ 
					$consulta_e = pg_query("SELECT libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, 
												  libro.edicion, libro.pag_vol, libro.anio, libro.observaciones, libro.estatus,                                                  libro.sala, libro.cant_inactivos, libro.fecha, area.codigo, area.area, 
												  editorial.nombre, editorial.ciudad,
												  editorial.pais, autor.autor1, autor.autor2, autor.autor3
												  FROM libro, area, editorial, autor
												  WHERE libro.id_editorial=editorial.id_editorial 
												  and libro.id_area=area.id_area 
												  and fecha BETWEEN '$desde' AND '$hasta'
												  and libro.id_libro=autor.id_libro order by libro.id_libro desc");
												  
					$cant_cat= pg_query("SELECT sum(ejemplares) as cantidad_categoria FROM libro");
					$total = pg_fetch_array($cant_cat);
				}
				else
				{
					$consulta_e = pg_query("SELECT libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, 
												  libro.edicion, libro.pag_vol, libro.anio, libro.observaciones, libro.estatus,                                                  libro.sala, libro.cant_inactivos, libro.fecha, area.codigo, area.area, 
												  editorial.nombre, editorial.ciudad,
												  editorial.pais, autor.autor1, autor.autor2, autor.autor3
												  FROM libro, area, editorial, autor
												  WHERE libro.id_editorial=editorial.id_editorial 
												  and libro.id_area=area.id_area 
												  and libro.id_area='$tipo'
												  and fecha BETWEEN '$desde' AND '$hasta'
												  and libro.id_libro=autor.id_libro order by libro.id_libro desc");
												  
					$cant_cat= pg_query("SELECT sum(ejemplares) as cantidad_categoria FROM libro where id_area='$tipo'");
					$total = pg_fetch_array($cant_cat);
					
					
						
						$categorias= pg_query("select *from area where id_area='$tipo'");
						$area = pg_fetch_array($categorias);
						$nombre = $area['area'];
				}
				
		if($tipo == "Todos"){$reporte = "Listado de Ejemplares Existentes";}
		if($tipo != "Todos"){$reporte = "Listado de Ejemplares Existentes dde la Categoria: ".$nombre;}	
 
        $j=1; 
    	$pdf->AddPage();
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',10);			
		$pdf->Cell(240,6,utf8_decode($reporte).", Para el Rango de Fecha Seleccionado: DESDE (".$desde.") HASTA (".$hasta.")",0,'','C',1);
		$pdf->Ln(6);	
		$pdf->SetFillColor(236,236,236);
		$pdf->Ln(6);
		$pdf->SetFont('Arial','B',11);	
		$pdf->Cell(15,6,'ID',0,'','C',1);
		$pdf->Cell(55,6,'Titulo',0,'','J',1);
		$pdf->Cell(25,6,'ISBN',0,'','C',1);
		$pdf->Cell(30,6,'Cota',0,'','C',1);
		$pdf->Cell(30,6,'Categoria',0,'','C',1);
		$pdf->Cell(40,6,'Editorial',0,'','C',1);
		$pdf->Cell(15,6,'Act.',0,'','C',1);
		$pdf->Cell(15,6,'Inac.',0,'','C',1);
		$pdf->Cell(10,6,'Cant.',0,'','C',1);
		$pdf->Cell(30,6,'Sala',0,'','C',1);	
		
         while($ejemplar=pg_fetch_array($consulta_e))//mostrar todos
    	{
			$i++;
			//$fecha = date('Y-m-d');
			$cantidad=$ejemplar['ejemplares']-$ejemplar['cant_inactivos'];
			
			if($i%2 == 1)
				{
					$pdf->SetFillColor(255,255,255);
					$pdf->Ln(8);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(15,6,$i,0,'','C',1);
					$pdf->Cell(55,6,utf8_decode($ejemplar['titulo']),0,'','J',1);
					$pdf->Cell(25,6,utf8_decode($ejemplar['isbn']),0,'','J',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['cota']),0,'','J',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['area']),0,'','J',1);
					$pdf->Cell(40,6,utf8_decode($ejemplar['nombre']),0,'','C',1);
					$pdf->Cell(15,6,utf8_decode($cantidad),0,'','C',1);
					$pdf->Cell(15,6,utf8_decode($ejemplar['cant_inactivos']),0,'','C',1);
					$pdf->Cell(10,6,utf8_decode($ejemplar['ejemplares']),0,'','C',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['sala']),0,'','C',1);
				}
				else
				{
					$pdf->SetFillColor(236,236,236);
					$pdf->Ln(8);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(15,6,$i,0,'','C',1);
					$pdf->Cell(55,6,utf8_decode($ejemplar['titulo']),0,'','J',1);
					$pdf->Cell(25,6,utf8_decode($ejemplar['isbn']),0,'','J',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['cota']),0,'','J',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['area']),0,'','J',1);
					$pdf->Cell(40,6,utf8_decode($ejemplar['nombre']),0,'','C',1);
					$pdf->Cell(15,6,utf8_decode($cantidad),0,'','C',1);
					$pdf->Cell(15,6,utf8_decode($ejemplar['cant_inactivos']),0,'','C',1);
					$pdf->Cell(10,6,utf8_decode($ejemplar['ejemplares']),0,'','C',1);
					$pdf->Cell(30,6,utf8_decode($ejemplar['sala']),0,'','C',1);					
				}
						
		  $pdf->SetAutoPageBreak(true,40);
          $j=$j+1; 
        }
		
		if($tipo == "Todos"){
			$pdf->Ln(10);			
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(195,6,"Cantidad de Ejemplares: ".$total['cantidad_categoria'],0,'','J',1);
			
			$pdf->Ln(10);			
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(265,6,"Leyenda: (Act.: Cantidad Activa), (Inac. Cantidad Inactiva), (Cant. Cantidad Ejemplares).",0,'','J',1);
		}
		
		if($tipo != "Todos"){
			$pdf->Ln(10);			
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(195,6,"Cantidad de Ejemplares: ".$total['cantidad_categoria'].", Para la Categoria: ".$nombre,0,'','J',1);
			
			$pdf->Ln(10);			
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(265,6,"Leyenda: (Act.: Cantidad Activa), (Inac. Cantidad Inactiva), (Cant. Cantidad Ejemplares).",0,'','J',1);
		}
		
				
			
		 $pdf->cell(0,30,'',0,1);
         $pdf->Text(115,$pdf->GetY(),'________________________'); 
         $pdf->cell(0,5,'',0,1);
         $pdf->Text(130,$pdf->GetY(),'Bibliotecario');			 
		 /*------------------------------------------------------------------------------------------------*/
		
		 
		 $pdf->Output();
}
else
{
	include_once("error_404.php");
	//header("Location:./");
}
 
?>
