<?php
if(isset($_POST['hasta']) && isset($_POST['desde']) && isset($_POST['tipo']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");
	include("./../public/plugins/fpdf/fpdf.php");

	class PDF extends FPDF 
	{ 
	
		function Header() 
		{ 		
			
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
			
			//Logo
			$this->Image('./../public/img/bibliotecanacional.jpg',85,10,40,15);
			//Arial bold 15
			
			$this->SetFont('Arial','B',14); 
			//Movernos a la derecha 
			$this->Cell(30); 
			//T�tulo 
			$this->Ln(2); 
			
			$this->SetFont('Arial','B',10); 
			$this->Cell(36);
			
			$this->Text(70,30,'REP�BLICA BOLIVARIANA DE VENEZUELA');
	
			$this->Text(77,36.5,'BIBLIOTECA "EDUARDO BLANCO"');
	
			$this->Text(63,43,'MUNICIPIO RIBERO - CARIACO � ESTADO � SUCRE');
			
			$this->Text(132,52,$fecha_hoy);

			$this->SetFont('Arial','',6); 
		
			$this->SetFont('Arial','',10); 
			//Salto de l�nea 
			$this->Ln(10); 
			$this->SetY(60);
		} 
	
		//Pie de p�gina 
		function Footer() 
		{ 
		   //$fecha=date("d/m/Y");
		  //Posicion
			$this->SetY(-35.8); 
			//Arial italic 8 
			$this->SetFont('Arial','I',10); 
			//N�mero de p�gina 
			$this->Cell(0,30,'P�gina '.$this->PageNo().'/{nb}',0,0,'C'); 
			$this->Text(148,259.5,''); 
			$this->Text(180,259.5,'');
		} 
	
		function __construct() 
		{        
			//Llama al constructor de su clase Padre. 
			//Modificar aka segun la forma del papel del reporte 
			parent::__construct('P','mm','Letter'); 
		}
	}// fin de class
			
    //Creaci�n del objeto de la clase heredada 
		$pdf=new PDF(); 
		$pdf->SetTopMargin(10.8); 
		$pdf->SetLeftMargin(10.5);     
		$pdf->AliasNbPages(); 
		$pdf->SetFont('Arial','B',10);
		
				$desde =  $_POST['desde'];
				$hasta =  $_POST['hasta'];
				$tipo =  $_POST['tipo'];
				
				if($tipo=="Todos")
				{ 
					$consulta_u = pg_query("SELECT *FROM usuario WHERE fecha BETWEEN '$desde' AND '$hasta'");
					$count_usuarios= pg_query("SELECT count(*) FROM usuario");

				}
				else
				{
					$consulta_u = pg_query("SELECT *FROM usuario WHERE fecha BETWEEN '$desde' AND '$hasta' AND tipo='$tipo'");
					$count_usuarios= pg_query("SELECT count(*) FROM usuario where tipo='$tipo'");

				}
				
					$usuarios = pg_fetch_array($count_usuarios);
				
				
		if($tipo == "Todos"){$reporte="Listado de Usuarios Existentes"." ".$usuarios['0'];}
		if($tipo != "Todos"){$reporte="Listado de Usuarios: "."".$tipo." ".$usuarios['0'];}	
 
        $j=1; 
    	$pdf->AddPage();
		$pdf->SetFillColor(236,236,236);
		$pdf->SetFont('Arial','',10);			
		$pdf->Cell(195,6,utf8_decode($reporte).", Para el Rango de Fecha Seleccionado: DESDE (".$desde.") HASTA (".$hasta.")",0,'','C',1);
		$pdf->Ln(6);	
		
         while($usuario=pg_fetch_array($consulta_u))//mostrar todos
    	{
			$i++;
			$fecha = date('Y-m-d');
			$edad_usuario=$fecha-$usuario['fecha_nac'];
						  		  			  
				if($i%2 == 1)
				{										
					$pdf->SetFillColor(255,255,255);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(10,6,'ID',0,'','C',1);
					$pdf->Cell(40,6,'Cedula',0,'','C',1);
					$pdf->Cell(50,6,'Nombres y Apellidos',0,'','C',1);
					$pdf->Cell(45,6,'Correo',0,'','C',1);
					$pdf->Cell(30,6,'Ocupacion',0,'','C',1);
					$pdf->Cell(30,6,'sexo',0,'','C',1);	
					$pdf->Ln(6);
					$pdf->SetFillColor(255,255,255);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(10,6,$i,0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(40,6,$usuario['cedula'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(50,6,$usuario['primer_nombre']." ". $usuario['segundo_nombre']." ".$usuario['primer_apellido']." ".							                    $usuario['segundo_apellido'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(45,6,$usuario['correo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['ocupacion'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['sexo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(30,6,'Tipo',0,'','C',1);
					$pdf->Cell(40,6,'Programa',0,'','C',1);
					$pdf->Cell(50,6,'Discapacidad',0,'','C',1);
					$pdf->Cell(45,6,'Fecha Nac.',0,'','C',1);
					$pdf->Cell(30,6,'Edad',0,'','C',1);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['tipo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(40,6,$usuario['programa'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(50,6,$usuario['discapacidad'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(45,6,$usuario['fecha_nac'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$edad_usuario,0,'','C',1);
					$pdf->SetFont('Arial','',10);
				
					$pdf->SetFillColor(255,255,255);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(80,6,'Punto de Referencia',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Direccion 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Direccion 2',0,'','C',1);
						
					$pdf->Ln(6);
					$pdf->SetFillColor(255,255,255);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(80,6,$usuario['referencia'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['direccion1'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['direccion2'],0,'','C',1);
					
					$pdf->SetFillColor(255,255,255);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(80,6,'Telefono 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Telefono 2 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Fecha de Alta',0,'','C',1);
					
					$pdf->Ln(6);
					$pdf->SetFillColor(255,255,255);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(80,6,$usuario['telefono1'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['telefono2'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['fecha'],0,'','C',1);
					

				}
			
				else
				{
					$pdf->SetFillColor(236,236,236);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(10,6,'ID',0,'','C',1);
					$pdf->Cell(40,6,'Cedula',0,'','C',1);
					$pdf->Cell(50,6,'Nombres y Apellidos',0,'','C',1);
					$pdf->Cell(45,6,'Correo',0,'','C',1);
					$pdf->Cell(30,6,'Ocupacion',0,'','C',1);
					$pdf->Cell(30,6,'sexo',0,'','C',1);	
					$pdf->Ln(6);
					$pdf->SetFillColor(236,236,236);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(10,6,$i,0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(40,6,$usuario['cedula'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(50,6,$usuario['primer_nombre']." ". $usuario['segundo_nombre']." ".$usuario['primer_apellido']." ".							                    $usuario['segundo_apellido'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(45,6,$usuario['correo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['ocupacion'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['sexo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(30,6,'Tipo',0,'','C',1);
					$pdf->Cell(40,6,'Programa',0,'','C',1);
					$pdf->Cell(50,6,'Discapacidad',0,'','C',1);
					$pdf->Cell(45,6,'Fecha Nac.',0,'','C',1);
					$pdf->Cell(30,6,'Edad',0,'','C',1);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$usuario['tipo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(40,6,$usuario['programa'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(50,6,$usuario['discapacidad'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(45,6,$usuario['fecha_nac'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$edad_usuario,0,'','C',1);
					$pdf->SetFont('Arial','',10);
				
					$pdf->SetFillColor(236,236,236);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(80,6,'Punto de Referencia',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Direccion 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Direccion 2',0,'','C',1);
						
					$pdf->Ln(6);
					$pdf->SetFillColor(236,236,236);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(80,6,$usuario['referencia'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['direccion1'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['direccion2'],0,'','C',1);
					
					$pdf->SetFillColor(236,236,236);
					$pdf->Ln(6);
					$pdf->SetFont('Arial','B',11);	
					$pdf->Cell(80,6,'Telefono 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Telefono 2 1',0,'','C',1);
					$pdf->SetFont('Arial','B',11);
					$pdf->Cell(60,6,'Fecha de Alta',0,'','C',1);
					
					$pdf->Ln(6);
					$pdf->SetFillColor(236,236,236);
					$pdf->SetFont('Arial','',11);	
					$pdf->Cell(80,6,$usuario['telefono1'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['telefono2'],0,'','C',1);
					$pdf->SetFont('Arial','',11);
					$pdf->Cell(60,6,$usuario['fecha'],0,'','C',1);
				}
						
		  $pdf->SetAutoPageBreak(true,40);
          $j=$j+1; 
			
		
        }
						
		 $pdf->cell(0,30,'',0,1);
         $pdf->Text(85,$pdf->GetY(),'________________________'); 
         $pdf->cell(0,5,'',0,1);
         $pdf->Text(100,$pdf->GetY(),'Bibliotecario');			 
		 /*------------------------------------------------------------------------------------------------*/
		
		 
		 $pdf->Output();
}
else
{
	include_once("error_404.php");
	//header("Location:./");
}
 
?>
