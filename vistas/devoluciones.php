<?php include_once("./../controladores/instancias_prestamo.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>

<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=3; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=3; include ('menu_administrador.php');
		}?>				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Devoluciones</li>
					<li class="active">Devoluci&oacute;n</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-save"></i> Devoluciones 			<a href="ejemplares_prestados.php" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="glyphicon glyphicon-ok-circle"></i> Ejemplares Prestados</a>

<a href="./" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Cancelar</a>
            <small class="pull-right">Fecha: <?php echo date('m-d-Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      
      <!-- /.row -->
<?php $validar = pg_num_rows($prestamo->ejemplares_prestados);
if ($validar>0){?>
      <!-- Table row -->
      <div class="row">
		<br>
        <div class="col-xs-12 table-responsive">
           <table class="table table-striped" id="dataTables-example">
            <thead>
            <tr>
			  <th>ID</th>
			  <th>ISBN</th>
              <th>Titulo</th>
			  <th>Lector</th>
			  <th>Tipo</th>
              <th>Expedido</th>
              <th>Vence</th>
			  <th>Entrega</th>
			  <th>Retraso</th>
			  <th>Estatus</th>
			  <th>Acc&iacute;on</th>
            </tr>
            </thead>
            <tbody>
	<?php $i=0;?>			
	<?php while($result=pg_fetch_array($prestamo->ejemplares_prestados)){?>
	<?php $id_libro = $result['id_libro'];?>
	<?php $id_prestamo = $result['id_prestamo'];?>
	<?php $id_usuario = $result['id_usuario'];?>
	<?php $id_prestamo_libro = $result['id_prestamo_libro'];?>
	<?php $i++;?>
	<?php $expedido = explode ('-',$result['fecha_prestamo']);?>
	<?php $fecha_prestamo = $expedido [2]."-".$expedido[1]."-".$expedido[0]; ?>
	<?php $vence = explode ('-',$result['fecha_entrega']);?>
	<?php $fecha_entrega = $vence [2]."-".$vence[1]."-".$vence[0]; ?>
	<?php $devolucion = explode ('-',$result['fecha_devolucion']);?>
	<?php $fecha_devolucion = $devolucion [2]."-".$devolucion[1]."-".$devolucion[0]; ?>
	<?php $fecha = date ('d-m-Y');?>
	<?php $dias_retraso = $fecha-$fecha_entrega;
		  
		  $fecha_e=date("d-m-Y",strtotime($result['fecha_entrega']));
		  $fecha_d=date("d-m-Y");
		  
		  //aqui se compara la fechas para el calculo del retraso del prestamo y las sanciones
		  $retraso = calcular_retraso($fecha_d, $fecha_e);
	?>
            <tr>
			  <td><?php echo $i;?></td>
			  <td><?php echo $result['isbn'];?></td>
              <td><?php echo $result['titulo'];?></td>
			  <td><?php echo $result['primer_nombre']." ".$result['primer_apellido'];?></td>
			  <td><?php echo $result['tipo'];?></td>
              <td><?php echo $fecha_prestamo;?></td>
              <td><?php echo $fecha_entrega;?></td>
			  <td align="center"><?php if($result['fecha_devolucion']==NULL){echo '<span class="label label-info">----</span>';}else {echo $fecha_devolucion; }?></td>
			  <td><?php if(($result['prestamo_estatus']=='Retornado')){?><span class="label label-info"><?php echo "---";?></span><?php }else {?><span  class="label label-warning"><?php /*if ($dias_retraso<0){echo $dias_retraso." "."dias de Prestamo";} else {echo $dias_retraso." "."dias";}*/if($retraso>=0){ echo "D&iacute;as de Retraso: "; }else{$restantes = $retraso*-1; echo "D&iacute;as Restantes: ";}
			  if($retraso>=0){echo "$retraso";}else{echo "$restantes";}?></span><?php } ?></td>

              <td><?php if(($result['prestamo_estatus']=='Pendiente')){?><span class="label label-danger"><?php echo $result['prestamo_estatus'];?></span><?php }else {?><span  class="label label-warning"><?php echo $result['prestamo_estatus'];?></span><?php } ?></td>
			  <td align="center">
			  <?php if(($result['prestamo_estatus']=='Pendiente')){?><a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="retornar" data-toggle="modal" data-target="#retornar<?php echo $i;?>"  href=""><i class="glyphicon glyphicon-save"></i> Retornar</a><?php }else {?><i class="glyphicon glyphicon-ok"></i><?php } ?>
			  
			  </td>
            </tr>
			<div class="modal fade" id="retornar<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Devolver el Libro
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="../controladores/instancias_prestamo.php<?php echo '?id_prestamo='.$id_prestamo_libro.'&retraso='.$retraso.'&id='.$id_usuario; ?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>					
	<?php }?>
            </tbody>
          </table>
        </div>
       			
      </div>
      <!-- /.row -->
	  <?php }else{$mensaje=1;}?>


      <!-- this row will not appear when printing 
      <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-success pull-right" style="margin-right: 25px;" title="Agregar por Lotes">
            <i class="glyphicon glyphicon-save"></i> <strong>Devolver</strong></button>
        </div>
      </div>-->
	  <?php include('modal_prestamo.php'); ?>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<script>
    $(document).ready(function() {
	
		<?php if($mensaje==1){?>
        $('#devolucion').modal({ show:true, backdrop:'static'});
		<?php }?>
		
    });
</script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[7]}]
        });
    });
</script>
<?php include('llamado_footer.php');?>