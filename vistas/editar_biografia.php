<?php include_once("./../controladores/instancias_biografia.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php");?>
	<?php include('header.php'); ?>
	
	<?php if(isset($_GET['id']))
				{
					$biografia->datos_biografia($_GET['id']);
				}
				
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'update') {	
	
			$titulo=$_POST['titulo'];
			$autor=$_POST['autor'];
			$contenido=$_POST['contenido'];
			
			/*$imagen=$_FILES['imagen']['tmp_name']; 
			$nombre=$_FILES['imagen']['name'];*/
			
			$imagen= $_FILES['imagen']['name'];
			$ruta = $_FILES['imagen']['tmp_name']; 
			
			$id = $_POST['id'];
			
			//echo $titulo;
					
			$biografia->inicializar_datos ($titulo, $autor, $contenido, $imagen);
			$biografia->actualizar_biografia($id, $ruta);
	}
	?>
	
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=7; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=7; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Biografia</li>
					<li class="active">Actualizar Biografia</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						
					<div class="row">
					<div class="col-md-8">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-modal-window"></i> Actualizar Biografia</h3> <a href="biografias.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Biografias</a>
								</div>
						<form class="form-horizontal" action="" method="post"m enctype="multipart/form-data">
							<input type="hidden" name="actividad" value="registro de cuanta de usuario" class="url">
						<input type="hidden" name="id" value="<?php echo $biografia->reg->id_biografia;?>">

							<input type="hidden" name="submit" value="update" />
              <div class="box-body">
				<?php if(!$biografia->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Actualize Los Datos de la Biografia.
								</div>
								<?php }?>
			
								<?php if($biografia->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Biografia Actualizada Satisfactoriamente.
								</div>
								<?php }?>
								<?php if($biografia->mensaje==2){?>
								<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
							<?php }?>
							
							<?php if($mensaje){?>
								<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Formato de Imagen no Soportado
						</div>
							<?php }?>	
               <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">T&iacute;tulo:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="titulo"  value="<?php echo $biografia->titulo;?>" onKeyPress="return soloLetras(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Autor:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="autor"  value="<?php echo $biografia->autor;?>" onKeyPress="return soloLetras(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Contenido:</label>

                  <div class="col-sm-10">
				   	<textarea name="contenido"  value="" class="form-control" cols="" rows="" onKeyPress="return soloLetras(event)"><?php echo $biografia->contenido;?></textarea>
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Imagen:</label>

                  <div class="col-sm-10">
             		 <input name="imagen" type="file" value="" class="contact" required/> 
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div align="right">
					<a href="biografias.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Registrar Inmueble" value="Actualizar Biografia" class="btn btn-primary"/>
					</div>
              </div>
              <!-- /.box-body -->
                
              <!-- /.box-footer -->
            </form>
								
							</div><!-- /.col -->
						</div>
							<div class="col-md-4">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Imagen de Biografia.	</div>
							<center>
							<img src="../<?php echo $biografia->imagen;?>" width="100" height="100" class="img-circle">
							</center>
								</div>
						
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>