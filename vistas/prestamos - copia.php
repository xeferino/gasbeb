<?php include_once("./../controladores/instancias_usuario.php"); ?>
<?php include_once("./../controladores/instancias_prestamo.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); 

/*$pag=$_SERVER['PHP_SELF'];
		$num_total_registros = pg_num_rows($prestamo->busqueda);
		//Si hay registros
		if ($num_total_registros > 0) 
		{
			//Limito la busqueda
			$TAMANO_PAGINA = 10;
			$pagina = false;
			
			//examino la pagina a mostrar y el inicio del registro a mostrar
			if (isset($_GET["pagina"]))
			$pagina = $_GET["pagina"];
			if (!$pagina)
			{
				$inicio = 0;
				$pagina = 1;
			}
			else 
			{
				$inicio = ($pagina - 1) * $TAMANO_PAGINA;
			}
			//calculo el total de paginas
			$total_paginas = ceil($num_total_registros / $TAMANO_PAGINA);
		}*/
?>

<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=2; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=2; include ('menu_administrador.php');
		}?>
				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Prestamos</li>
					<li class="active">Prestamo</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-open"></i> Prestamos
			<a href="ejemplares_prestados.php" class="mb-xs mt-xs mr-xs btn btn-primary"><i class="glyphicon glyphicon-ok-circle"></i> Ejemplares Prestados</a>
			<a href="./" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Cancelar</a>
            <small class="pull-right">Fecha: </small>
			
          </h2>
        </div>
        <!-- /.col -->
		<div class="col-xs-2">
        </div>
		<div class="col-xs-8">
         <form action="" method="post">
		 	<input type="hidden" name="submit" value="buscar">
		 <label	a>Busqueda por Titulo o ISBN</label>
		 <div class="input-group input-group-sm">
                <input type="text" name="palabra" class="form-control" value="<?php echo $palabra;?>" required>
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
              </div></form>
        </div>
			  <div class="col-xs-2">
        </div>
      </div>
		<?php if(($prestamo->valida_busqueda>0) and (isset($_POST['submit']) && $_POST['submit'] == 'buscar')){?>
		<!--hjhhjj-->     
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col"  style="margin-top:4px;">
          <strong>Usuario:</strong>
			  <address>
			  <form method="post" name="form"  action="">
			  	<input type="hidden" name="submit" value="prestamo">
			  <div class="form-group">
				  <select class="form-control select2" name="id_usuario">
				  	<option value="0">--Selecccione--</option>
				  <?php while($reg=pg_fetch_object($usuario->consulta)){?>
					<?php $i++;?>
					<option value="<?php echo $reg->id_usuario;?>"><?php echo $reg->primer_nombre." ".$reg->segundo_nombre." ".$reg->primer_apellido." ".$reg->segundo_apellido;?></option>
				  <?php }?>
				   </select>
                </div>
				  
			  </address>
		</div>
		
		<div class="col-sm-4 invoice-col"  style="margin-top:4px;">
			  <strong>Tipo Pr&eacute;tamo:</strong>
			  <address>
			  <div class="form-group">
				  <select class="form-control" id="tipo_prestamo" name="tipo_prestamo" onChange="cambiar();">
				  	<?php if ((isset($_POST['submit']) && $_POST['submit'] == 'prestamo')&&( $tipo_prestamo=="Interno")){?>
					<option value="<?php echo $tipo_prestamo;?>"><?php echo $tipo_prestamo;?></option>
					<option  value="Circulante">Circulante</option>
					<?php }?>
					 <?php if ((isset($_POST['submit']) && $_POST['submit'] == 'prestamo')&&( $tipo_prestamo=="Circulante")){?>
					<option value="<?php echo $tipo_prestamo;?>"><?php echo $tipo_prestamo;?></option>
					<option  value="Interno">Interno</option>
					<?php }?>
					
					 <?php if ((isset($_POST['submit']) && $_POST['submit'] == 'prestamo')&&( $tipo_prestamo=="0")){?>
					<option value="0">--Selecccione--</option>
					<option  value="Interno">Interno</option>
					<option  value="Circulante">Circulante</option>
					<?php }?>
					
					<?php if(!(isset($_POST['submit']) && $_POST['submit'] == 'prestamo')){?>
					<option value="0">--Selecccione--</option>
					
                    <option  value="Interno">Interno</option>
					<option  value="Circulante">Circulante</option>
					<?php }?>
				   </select>
                </div>
			  </address>
		</div>
        <!-- /.col -->
        
        <!-- /.col -->
         <div class="col-sm-4 invoice-col"  style="margin-top:4px;">
             <strong>Fecha Entrega:</strong>
			  <address>
			  <div class="form-group">
			   <input name="fecha_prestamo" type="hidden" value="<?php echo date("d-m-Y") ;?>">
				<input type="text" name="fecha_entrega" id="datepicker"  value="<?php echo date('d-m-Y');?>"  class="form-control">

                </div>
				  
			  </address>
		</div>
	</div>
	
	<div class="row invoice-info">
        <div class="col-sm-2 invoice-col"  style="margin-top:0px;">
         
		</div>
		
		<div class="col-sm-8 invoice-col"  style="margin-top:-30px; margin-bottom:-10px; ">	
		<div class="alert alert-info alert-dismissible" style="margin-top:23px; padding: 5px 5px 5px 5px;">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding-right:20px;">&times;</button>
               		<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n: Seleccione un Libro de la Lista.</strong> 
              </div>
		
		
		</div>
        <!-- /.col -->
        
        <!-- /.col -->
         <div class="col-sm-2 invoice-col"  style="margin-top:4px;">
          
	</div>
	  
      <!-- /.row -->

      <!-- Table row -->
      <!-- Table row -->
      <div class="row" id="cons_isbn" style="display:block">
		<br>
        <div class="col-xs-12 table-responsive">
		
          <table class="table table-striped">
            <thead>
            <tr>
			  <th>ID</th>
			  <th>ISBN</th>
              <th>Titulo</th>
			  <th>&Aacute;rea</th>
              <th>Autor</th>
              <th>Editorial</th>
			  <th>Disponibles</th>
			  <th>Agregar</th>
            </tr>
            </thead>
            <tbody>
			<?php /*$libros=pg_query("SELECT * FROM libro 
									  WHERE ((titulo like '%$prestamo->palabra%') or (isbn like '%$prestamo->palabra%')) ORDER BY 
									  libro.id_libro DESC limit ".$TAMANO_PAGINA." offset ".$inicio." ");*/?>
	<?php $i=0;?>			
	<?php while($result=pg_fetch_array($prestamo->busqueda)){?>
	<?php $i++;?>
	<?php $id = $result['id_libro'];?>
		<input name="id_libro" type="hidden" value="<?php echo $id;?>">
	<?php $descontar = pg_query("SELECT count(*) from prestamo, prestamo_libro, libro 
	                             where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
								 and prestamo_libro.prestamo_estatus!='Retornado' and prestamo_libro.id_libro='$id'");
		
						$row=pg_fetch_array($descontar);
						$cant_prestado=$row['0'];
						$cantidad=$result['ejemplares']-$result['cant_inactivos'];
						$disponibles= $cantidad-$cant_prestado;
						
						$ejemplares = pg_query("SELECT libro.id_libro, libro.isbn, libro.titulo, libro.cota, libro.ejemplares, 
												libro.edicion, libro.pag_vol, libro.anio, libro.observaciones, libro.estatus, 
												libro.sala, libro.cant_inactivos, area.codigo, area.area, editorial.nombre, 
												editorial.ciudad, editorial.pais, autor.autor1, autor.autor2, autor.autor3
												FROM libro, area, editorial, autor
												WHERE libro.id_editorial=editorial.id_editorial and libro.id_area=area.id_area and
												libro.id_libro=autor.id_libro order by libro.id_libro desc");
						$libro=pg_fetch_array($ejemplares);

	?>
            <tr>
			
			  <td><?php echo $i; echo $prestamo->maximo;?></td>
			  <td><?php echo $result['isbn'];?></td>
              <td><?php echo $result['titulo'];?></td>
			  <td><?php echo $libro['area'];?></td>
              <td><?php echo $libro['autor1'];?></td>
              <td><?php echo $libro['nombre'];?></td>
			  <td align="center" id="disponibles"><?php if ($disponibles==1){?><span class="label label-danger"><?php echo $disponibles;}else{?> <span class="label label-success"><?php echo $disponibles;}?></span></td>
			  <td align="center" class="disponibles"><input id="selector" class="cl" name="select[]" type="checkbox" value="<?php echo $id;?>" >
			  <input type="hidden" class="selector" name="submit" value="<?php echo $disponibles;?>">
			  </td>
            </tr>
	<?php }?>
            </tbody>
          </table>
        </div>
        <!-- /.col 
		<div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary pull-right btn_sig" id="btn_next" title="Siguiente" style="margin-right: 30px;" disabled="disabled">
           <strong>Siguiente</strong> <i class="glyphicon glyphicon-triangle-right"></i></button>
        </div>
      </div>-->
      </div>
	  <?php 
		echo'<div class="text-center">
  				<ul class="pagination">';

			  if ($total_paginas > 1) {
				if ($pagina != 1)
				  echo '
				
					<li><a href="'.@$pag.'?pagina='.($pagina-1).'" style="text-decoration:none;">&laquo;</a></li>';
				for ($i=1;$i<=$total_paginas;$i++) {
				  if ($pagina == $i)
					//si muestro el ?ndice de la p?gina actual, no coloco enlace
					echo "<li><a href=''><u>".$pagina."</u></a></li>";
				  else
					//si el ?ndice no corresponde con la p?gina mostrada actualmente,
					//coloco el enlace para ir a esa p?gina
					echo '<li><a href="'.@$pag.'?pagina='.$i.'" style="text-decoration:none;">'.$i.'</a></li>';
				}
				if ($pagina != $total_paginas)
				  echo '<li><a href="'.@$pag.'?pagina='.($pagina+1).'" style="text-decoration:none;">&raquo;</a></li>';
			  }
  echo '</ul></div>';?>
      <!-- /.row -->
	<div class="row no-print" align="center">
        <div class="col-xs-12">
			  <button type="submit" class="btn btn-success pull-left BtnPrestamo" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-open"></i> Procesar Pr&eacute;stamo</button>
		 </form>
		 <!--hjhj-->
		 <?php }else{$mensaje=5;}?>
        </div>
      </div>
	  <div class="modal fade" id="valor1_cir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Ejemplar Disponibles solo para prestamos Internos.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
							<div class="modal fade" id="valor0_cir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> No hay Ejemplares disponibles.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>	
							
							<div class="modal fade" id="retornar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Maximo 3 Libros por Prestamos
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>	
      <!-- this row will not appear when printing -->
    </section>
	
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
		<?php include('modal_prestamo.php'); ?>
<script>
    $(document).ready(function() {
	
		<?php if($prestamo->mensaje==1){?>
        $('#prestamo_registro').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if($prestamo->mensaje==2){?>
        $('#error_prestamo').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if($prestamo->mensaje==3){?>
        $('#usuario_prestamo').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if($prestamo->mensaje==4){?>
        $('#usuario_visitante').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if($mensaje==4){?>
        $('#fecha').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if((isset($_POST['submit']) && $_POST['submit'] == 'buscar')and($mensaje==5)){?>
        $('#busqueda').modal({ show:true, backdrop:'static'});
		<?php }?>
		
		<?php if($prestamo->mensaje==7){?>
        $('#maximo').modal({ show:true, backdrop:'static'});
		<?php }?>
    });
</script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
    });
</script>
	
<script>		
$(".cl").change(function(){
    var max= 3;
    if( $(".cl:checked").length == max ){
	
        $(".cl").attr('disabled', 'disabled');
		$('#retornar').modal({ show:true, backdrop:'static'});
        $(".cl:checked").removeAttr('disabled');
		
    }else{

         $(".cl").removeAttr('disabled');
    }
})
</script>

<script>
	function cambiar(){
		if(form.tipo_prestamo.value=="Circulante"){
			form.fecha_entrega.disabled=false;
			//form.lanzador.disabled=false;
		}else{
		   form.fecha_entrega.disabled=true;
		   //form.lanzador.disabled=true;
		}
	}
</script>

<script>
$('select#tipo_prestamo').on('change',function(){

    var valor = $(this).val();
	
	if (valor=='Circulante'){
		$(".disponibles").click(function(){
	
		var valor = $(".selector",this).val();
		
			if(valor=="1"){
			$('#valor1_cir').modal({ show:true, backdrop:'static'});
			$(".BtnPrestamo").attr('disabled', 'disabled');
			}
			
			else if (valor=="0"){
				$('#valor0_cir').modal({ show:true, backdrop:'static'});
				$(".BtnPrestamo").attr('disabled', 'disabled');
			}
			else{
				$(".BtnPrestamo").removeAttr('disabled');
			}		
		});//
    }
	
	if (valor=='Interno'){
		$(".disponibles").click(function(){
	
		var valor = $(".selector",this).val();
		
			if(valor=="0"){
			$('#valor0_cir').modal({ show:true, backdrop:'static'});
			$(".BtnPrestamo").attr('disabled', 'disabled');
			}
			else{
				$(".BtnPrestamo").removeAttr('disabled');
			}		
		});//
    }
});//fin de select
</script>
<?php include('llamado_footer.php');?>
		<script>
			$(".select2").select2();
			$(".select22").select2();
		</script>