<?php include_once("./../controladores/instancias_ejemplar.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=9; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=9; include ('menu_administrador.php');
		}?>				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Reportes</li>
					<li class="active">Informe de Ejemplares</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-book"></i> Informe de Ejemplares <!--<a href="#" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Usuarios Sancionados</a> --><a href="reportes.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a> 
			</h2> 
        </div>
        <!-- /.col -->
      </div>
	  
	  <div class="row">
        <div class="col-xs-2">
           
        </div>
		<div class="col-xs-8">
           <?php if($ejemplar->mensaje==1){?>
								<address>
		  <div class="alert alert-warning alert-dismissible" style="margin-top:5px; padding: 8px 8px 8px 8px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding-right:20px;">&times;</button>
               		<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos en Blancos, Por Favor Verifique.
              </div>
			
          </address>
			<?php }?>
			
			<?php if(($ejemplar->valido==0) && ($_POST)){?>
								<address>
		  <div class="alert alert-danger alert-dismissible" style="margin-top:5px; padding: 8px 8px 8px 8px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding-right:20px;">&times;</button>
               		<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> No hay Resultados, en la Busqueda.
              </div>
			
          </address>
			<?php }?>
        </div>
		<div class="col-xs-2">
           
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
	  
      <div class="row invoice-info">
          <div class="col-sm-4 invoice-col">
				<form method="post" action="">
					<input type="hidden" name="submit" value="fecha_rango" />
				<label>Categorias:</label>
					<select class="form-control" name="tipo" id="Estatus">
					
						<?php $categorias= pg_query("select *from area where id_area='$tipo'");?>
						<?php $area = pg_fetch_array($categorias);?>
						<?php $nombre = $area['area'];?>
						
						<?php if($_POST){?>
						<option value="<?php echo $tipo;?>" selected="selected"><?php if ($tipo=='Todos'){echo $tipo;} else {echo $nombre;}?></option>
						<option value="Todos">Todos</option>
						<?php while($reg=pg_fetch_object($ejemplar->categorias)){?>
					<?php $i++;?>
						<option value="<?php echo $reg->id_area;?>"><?php echo $reg->area;?></option>
				  <?php }
				  		}
				  ?>
				  
				  <?php if(!$_POST){?>
						<option value="Todos" selected="selected">Todos</option>
						<?php  while($reg=pg_fetch_object($ejemplar->categorias)){?>
					<?php $i++;?>
						<option value="<?php echo $reg->id_area;?>"><?php echo $reg->area;?></option>
				  <?php }
				  		}
				  ?>
					</select>
		  </div>
		  
		  <div class="col-sm-3 invoice-col">
		  		<label>Desde:</label>
			<div class="input-group input-group-icon">
					  <input type="text" class="form-control"  id="campofecha1" value="<?php if ($_POST){ echo $desde;} else{?><?php echo date('Y-m-d');}?>" name="desde" title="Desde">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
				</div>
		   <div class="col-sm-3 invoice-col">
		   		<label>Hasta:</label>
				<div class="input-group input-group-icon">
						<input type="text" class="form-control"  id="campofecha" value="<?php if ($_POST){ echo $hasta;} else{?><?php echo date('Y-m-d');}?>" name="hasta" title="Hasta">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
		  </div>
		  
		  <div class="col-sm-2 invoice-col">
		   		<label style="color:#FFFFFF">mmm</label>
					<button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-search"></i> Consultar
			  </button>
		  </div>
        <!-- /.col -->
		</form>
      </div>
      <!-- /.row -->
		<br>
      <!-- Table row -->
      <div class="row">
	  <?php if ($ejemplar->valido>0){?>
        <div class="col-xs-12 table-responsive">
		<table class="table table-striped" id="dataTables-example">
            <thead>
            <tr>
				<th>ID</th>
				<th>ISBN</th>
				<th>Titulo</th>
				<th>Area</th>
				<th>Autor</th>
				<th>Cant. Activa</th>
				<th>Cant. Inactiva</th>
				<th>Ejemplares</th>
            </tr>
            </thead>
            <tbody>
			<?php $i=0;?>			
			<?php while($reg=pg_fetch_object($ejemplar->consulta_e)){?>
			<?php $i++;?>
			<?php $cantidad=$reg->ejemplares-$reg->cant_inactivos;?>

					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $reg->isbn;?></td>
						<td><?php echo $reg->titulo;?></td>
						<td><?php echo $reg->area;?></td>
						<td><?php echo $reg->autor1;?></td>
						<td><span class="label label-success"><?php echo  $cantidad;?></span></td>
						<td><?php if ($reg->cant_inactivos==0){echo "---";}else {echo '<span class="label label-danger">'.$reg->cant_inactivos;}?></span></td>
						<td><span class="label label-warning"><?php echo $reg->ejemplares;?></span></td>
					</tr>
<?php }?>
            </tbody>
          </table>
        </div>
		
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
		<form action="reportes_ejemplares_pdf.php" method="post" target="_blank">
			<input type="hidden" name="tipo" value="<?php echo $tipo;?>">
			<input type="hidden" name="desde" value="<?php echo $desde;?>">
			<input type="hidden" name="hasta" value="<?php echo $hasta;?>">
			  <button type="submit" class="btn btn-primary pull-left" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-file"></i> Generar PDF
			  </button>
		 </form>
        </div>
      </div>
	  <?php }?>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
               responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[5]}]
        });
    });
    </script>
		
<?php include('llamado_footer.php');?>