<?php include_once("./../controladores/instancias_usuario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<?php 
		if(isset($_GET['id']))
				{
					$usuario->datos_usuario($_GET['id']);
				}
?>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=4; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=4; include ('menu_administrador.php');
		}?>				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Usuario</li>
					<li class="active">Ficha Usuario</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-th-large"></i> Ficha de Usuario <a href="usuarios.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a>
            <small class="pull-right">Fecha: <?php echo date('m-d-Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
	  <div align="center">
		  <img src="../public/img/bibliotecanacional.jpg">
		 <h3>PLANILLA DE INCRIPCION DEL PRESTAMO CIRCULANTE</h3>
	 </div>
	 <br>
        <div class="col-sm-4 invoice-col">
          Nombres y Apellidos
          <address>
            <strong><?php echo $usuario->primer_nombre." ".$usuario->segundo_nombre." ".$usuario->primer_apellido." ".$usuario->segundo_apellido; ?></strong>
		</address>
		  Fecha Nacimiento
          <address>
            <strong><?php echo $usuario->fecha_nac; ?></strong>
          </address>
		  Edad
          <address>
            <strong><?php $hoy=date('Y-m-d'); $edad= $hoy-$usuario->fecha_nac; echo $edad; ?></strong>
          </address>
		   Discapacidad
          <address>
            <strong><?php echo $usuario->discapacidad; ?></strong><br>
          </address>
		   Direccion de Habitacion 1
          <address>
            <strong><?php echo $usuario->direccion1; ?></strong><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Codigo Carnet
          <address>
            <strong>Usu000<?php echo $usuario->carnet; ?></strong>
		</address>
		  Cedula de Identidad
          <address>
            <strong><?php echo $usuario->cedula; ?></strong><br>
          </address>
		  Programa o Misiones
          <address>
            <strong><?php echo $usuario->programa; ?></strong><br>
          </address>
		    Direccion de Habitacion 2
          <address>
            <strong><?php echo $usuario->direccion2; ?></strong><br>
          </address>
		  Punto de Refencia
          <address>
            <strong><?php echo $usuario->referencia; ?></strong><br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Sexo
          <address>
            <strong><?php echo $usuario->sexo; ?></strong>
		</address>
		 Correo
          <address>
            <strong><?php echo $usuario->correo; ?></strong><br>
          </address>
		  Ocupacion
          <address>
            <strong><?php echo $usuario->ocupacion; ?></strong><br>
          </address>
		   Telefono Movil
          <address>
            <strong><?php echo $usuario->telefono1; ?></strong><br>
          </address>
		  Telefono Habitacion
          <address>
            <strong><?php echo $usuario->telefono2; ?></strong><br>
          </address>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <!--<table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Serial #</th>
              <th>Description</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Call of Duty</td>
              <td>455-981-221</td>
              <td>El snort testosterone trophy driving gloves handsome</td>
              <td>$64.50</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Need for Speed IV</td>
              <td>247-925-726</td>
              <td>Wes Anderson umami biodiesel</td>
              <td>$50.00</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Monsters DVD</td>
              <td>735-845-642</td>
              <td>Terry Richardson helvetica tousled street art master</td>
              <td>$10.70</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Grown Ups Blue Ray</td>
              <td>422-568-642</td>
              <td>Tousled lomo letterpress</td>
              <td>$25.99</td>
            </tr>
            </tbody>
          </table>-->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
		<form action="ficha_usuario_pdf.php" method="post" target="_blank">
			<input type="hidden" name="id" value="<?php echo $usuario->reg->id_usuario;?>">
			  <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-file"></i> Generar PDF
			  </button>
		 </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php include('llamado_footer.php');?>