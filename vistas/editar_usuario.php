<?php include_once("./../controladores/instancias_usuario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<?php 
		if(isset($_GET['id']))
				{
					$usuario->datos_usuario($_GET['id']);
				}
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'editar') {	
	
		$cedula=$_POST['cedula'];
		$primer_nombre=$_POST['primer_nombre'];
		$segundo_nombre=$_POST['segundo_nombre'];
		$primer_apellido=$_POST['primer_apellido'];
		$segundo_apellido=$_POST['segundo_apellido'];
		$sexo=$_POST['sexo'];
		$correo=$_POST['correo'];
		$ocupacion=$_POST['ocupacion'];
		$tipo=$_POST['perfil'];
		$programa=$_POST['programa'];
		$discapacidad=$_POST['discapacidad'];
		$fecha_nac=$_POST['fecha_nac'];
		$referencia=$_POST['referencia'];
		$direccion1=$_POST['direccion1'];
		$direccion2=$_POST['direccion2'];
		$telefono1=$_POST['telefono1'];
		$telefono2=$_POST['telefono2'];
		
		$id =  $_POST['id'];
		$ci =  $_POST['ci'];

		$usuario->inicializar_datos ($cedula, $primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $sexo,$correo, $ocupacion, $tipo, $programa, $discapacidad, $fecha_nac, $referencia, $direccion1, $direccion2, $telefono1, $telefono2, $fecha, $codigo_carnet);
		$usuario->actualizar_usuario($id, $ci);
	}
?>
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=4; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=4; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Usuario</li>
					<li class="active">Actualizar Usuario</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-user"></i> Actualizar Usuario</h3> <a href="usuarios.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Usuarios</a>
									 <a href="registrar_usuario.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Regitrar Usuario</a>
<a href="sanciones.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Usuarios Sancionados</a>
								</div>
						<form method="post" id="formulario_usuario" role="form">
						<input type="hidden" name="id" value="<?php echo $usuario->reg->id_usuario;?>">
						<input type="hidden" name="ci" value="<?php echo $usuario->cedula;?>">
						<input type="hidden" name="submit" value="editar" />
              <div class="box-body">
			  <?php if(!$usuario->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Ingrese Los Datos del Nuevo Registro.
								</div>
								<?php }?>
								
						<?php if($usuario->mensaje==1){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, C&eacute;dula De Identidad no Disponible.
						</div>
						<?php }?>
						
						<?php if($usuario->mensaje==2){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
						<?php }?>
		<div class="col-sm-12">
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">C&eacute;dula de Identidad:</label>
                  <input type="text" name="cedula" value="<?php echo $usuario->cedula;?>" class="form-control" onKeyPress="return solonumeros(event)">
                </div>
				<div class="form-group">
                  <label for="form-control">Primer Apellido:</label>
                  <input type="text" name="primer_apellido"  value="<?php echo $usuario->primer_apellido;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Primer Nombre:</label>
                  <input type="text" name="primer_nombre"  value="<?php echo $usuario->primer_nombre;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
				<div class="form-group">
                  <label for="form-control">Segundo Apellido:</label>
                  <input type="text" name="segundo_apellido"  value="<?php echo $usuario->segundo_apellido;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Segundo Nombre:</label>
                  <input type="text" name="segundo_nombre"  value="<?php echo $usuario->segundo_nombre;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
				<div class="form-group">
                  <label for="form-control">Sexo:</label>
				 <select class="form-control" name="sexo" id="sexo">
					<?php if ($usuario->sexo=="Masculino"){?>
					<option value="<?php echo $usuario->sexo;?>"><?php echo $usuario->sexo;?></option>
					<option  value="Femenino">Femenino</option>
					<?php }?>
					 	 <?php if ($usuario->sexo=="Femenino"){?>
					<option value="<?php echo $usuario->sexo;?>"><?php echo $usuario->sexo;?></option>
					<option  value="Masculino">Masculino</option>
					<?php }?>
                  </select>                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Correo:</label>
                  <input type="email" name="correo"  value="<?php echo $usuario->correo;?>"  class="form-control">
                </div>
				<div class="form-group">
                  <label for="form-control">Fecha Nacimiento:</label>
                  <input type="text" name="fecha_nac"  id="datepicker" value="<?php echo $usuario->fecha_nac;?>"  class="form-control">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Perfil:</label>
				  <select class="form-control" name="perfil" id="sexo">
					<?php if ($usuario->tipo=="Circulante"){?>
					<option value="<?php echo $usuario->tipo;?>"><?php echo $usuario->tipo;?></option>
					<option  value="Visitante">Visitante</option>
					<?php }?>
					 	 <?php if ($usuario->tipo=="Visitante"){?>
					<option value="<?php echo $usuario->tipo;?>"><?php echo $usuario->tipo;?></option>
					<option  value="Circulante">Circulante</option>
					<?php }?>
                  </select>
                </div>
				<div class="form-group">
                  <label for="form-control">Discapacidad:</label>
                  <input type="text" name="discapacidad"  value="<?php echo $usuario->discapacidad;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Ocupaci&oacute;n:</label>
                  <input type="text" name="ocupacion"  value="<?php echo $usuario->ocupacion;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
				<div class="form-group">
                  <label for="form-control">Programa o Misiones:</label>
                  <input type="text" name="programa"  value="<?php echo $usuario->programa;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Direcci&oacute;n de Habitaci&oacute;n 1:</label>
<textarea name="direccion1"  value="" class="form-control" cols="" rows="" onKeyPress="return soloLetras(event)" ><?php echo $usuario->direccion1;?></textarea>                </div>
				<div class="form-group">
                 <label for="form-control">Telefono Movil:</label>
                  <input type="text" name="telefono1"  value="<?php echo $usuario->telefono1;?>"  class="form-control" onKeyPress="return solonumeros(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Direcci&oacute;n de Habitaci&oacute;n 2:</label>
				  	<textarea name="direccion2"  value="" class="form-control" cols="" rows="" onKeyPress="return soloLetras(event)"><?php echo $usuario->direccion2;?></textarea>
                </div>
				
			</div>
			<div class="col-sm-4">
				<div class="form-group">
                   <label for="form-control">Punto de Referencia:</label>
<textarea name="referencia"  value="" class="form-control" cols="" rows="" onKeyPress="return soloLetras(event)"><?php echo $usuario->referencia;?></textarea>                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Telefono Habitaci&oacute;n:</label>
                  <input type="text" name="telefono2"  value="<?php echo $usuario->telefono2;?>"  class="form-control" onKeyPress="return solonumeros(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div align="right">
				<br>
					<a href="usuarios.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Registrar Actualizar" value="Actualizar Usuario" class="btn btn-primary" />
                </div>
			</div>
			
			</div>
              </div><!-- /.box-body -->
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>