<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<?php 
	$actividad = new Actividad();  $actividad->actividades_recientes(); 
	$usuario = new Usuario();  $usuario->usuarios_recientes(); 
	$ejemplar = new Ejemplar();  $ejemplar->ejemplares_recientes();
	$ejemplar->categorias_recientes(); 
	$ejemplar->editoriales_recientes(); 
	$ejemplar->autores_recientes();
	
	$prestamo = new Prestamo();
	$prestamo->prestamos_recientes();
	$prestamo->devoluciones_recientes();
	 
?>

<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  	<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=1; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=1; include ('menu_administrador.php');
		}?>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Biblioteca
        <small><h2>&quot;Eduardo Blanco&quot;</h2></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active"></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <div class="row">
	  <div class="col-md-6">
			<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-user"></i> Usuarios</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registrados <span class="pull-right badge bg-blue"><?php echo $usuario->recientes['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-open"></i> Pr&eacute;stamos</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Pr&eacute;stamos <span class="pull-right badge bg-blue"><?php echo $prestamo->prestamo['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-save"></i> Devoluciones</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Devoluciones <span class="pull-right badge bg-blue"><?php echo $prestamo->devoluciones['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-calendar"></i> Actividades</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registradas <span class="pull-right badge bg-blue"><?php echo $actividad->recientes['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		</div>
		<div class="col-md-6">
			<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-book"></i> Ejemplares</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registrados <span class="pull-right badge bg-blue"><?php echo $ejemplar->libros['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-tags"></i> Editoriales</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registrados <span class="pull-right badge bg-blue"><?php echo $ejemplar->editoriales['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-bookmark"></i> Categorias</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registradas <span class="pull-right badge bg-blue"><?php echo $ejemplar->categorias['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		  <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-header with-border">
              <h3 class="box-title"><i class="glyphicon glyphicon-user"></i> Autores</h3>

              <div class="box-tools pull-right">
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a style="background:#f6f6f6;">Registrados <span class="pull-right badge bg-blue"><?php echo $ejemplar->autores['0'];?></span></a></li>
              </ul>
            </div>
          </div>
		  
		</div>
	 </div>
	 <!--<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><strong>Evoluci&oacute;n de los Prestamos</strong></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart" align="center">
			    <canvas id="barChart" style="height:230px"><img src="grafica_prestamo.php"></canvas>
                <div><img src="grafica_prestamo.php"></div>
              </div>
            </div>
            <!-- /.box-body 
          </div>-->
      <!-- /.row 
    </section>
    <!-- /.content -->
  </div>
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
<?php include('llamado_footer.php');?>
