<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>	
	<?php
		$inmueble = new Inmueble();
		$inmueble->auditoria_inmueble();
		$actividad = new Actividad();
		$actividad->auditoria_actividad();
		$usuario = new Usuario();
		$usuario->auditoria_usuario();
		$ejemplar = new Ejemplar();
		$ejemplar->auditoria_ejemplar();
		$biografia = new Biografia();
		$biografia->auditoria_biografia();
		$prestamo = new Prestamo();
		$prestamo->auditoria_prestamo();
		
	?>
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=10; include ('menu_administrador.php');
		}?>				</aside>
							</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Configuraci&oacute;n</li>
					<li class="active">Auditoria</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"> Listado de Movimientos (Bibliotecarios)</h3>
<a href="restore_backup.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-hdd"></i> Data Base</a>
<a href="bibliotecarios.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a>
								</div>
								<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Prestamos</a></li>
              <li><a href="#tab_2" data-toggle="tab">Devoluciones</a></li>
              <li><a href="#tab_3" data-toggle="tab">Usuarios</a></li>
              <li><a href="#tab_4" data-toggle="tab">Ejemplares</a></li>
			  <li><a href="#tab_5" data-toggle="tab">Biografias</a></li>
              <li><a href="#tab_6" data-toggle="tab">Actividades</a></li>
              <li><a href="#tab_7" data-toggle="tab">Inmuebles</a></li>
            </ul>
			
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
			  <h3 style=" margin-top:2px;">Prestamos - Movimientos</h3>
                <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-1">
									<thead>
										<tr>
											<th>ID</th>
											<th>Prestamo</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($prestamo->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->tipo;?></td>
											<td><?php echo $reg->fecha_prestamo;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Bibliotecario" data-toggle="modal" data-target="#prestamo_detalles<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>								
											</td>
										</tr>
										<div class="modal fade" id="prestamo_detalles<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
				
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
			  	<h3 style=" margin-top:2px;">Devoluciones - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-2">
									<thead>
										<tr>
											<th>ID</th>
											<th>Tipo Prestamo</th>
											<th>Estatus Prestamo</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php 		
									$prestamo = new Prestamo();
									$prestamo->auditoria_devoluciones();
									$i=0;?>			
								<?php while($reg=pg_fetch_object($prestamo->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->tipo;?></td>
											<td><?php echo $reg->prestamo_estatus;?></td>
											<td><?php echo $reg->fecha_prestamo;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Bibliotecario" data-toggle="modal" data-target="#prestamo_devoluciones<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>								
											</td>
										</tr>
										<div class="modal fade" id="prestamo_devoluciones<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
			  	<h3 style=" margin-top:2px;">Usuarios - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-3">
									<thead>
										<tr>
											<th>ID</th>
											<th>Usuario</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($usuario->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->primer_nombre." ".$reg->primer_apellido;?></td>
											<td><?php echo $reg->fecha;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Bibliotecario" data-toggle="modal" data-target="#view<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>							
											</td>
										</tr>
										<div class="modal fade" id="view<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
			  
			   <div class="tab-pane" id="tab_4">
			   	<h3 style=" margin-top:2px;">Ejemplares - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-4">
									<thead>
										<tr>
											<th>ID</th>
											<th>ISBN</th>
											<th>Titulo</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($ejemplar->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->isbn;?></td>
											<td><?php echo $reg->titulo;?></td>
											<td><?php echo $reg->fecha;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Bibliotecario" data-toggle="modal" data-target="#ver<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>						
											</td>
										</tr>
										<div class="modal fade" id="ver<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
			  
			   <div class="tab-pane" id="tab_5">
			   	<h3 style=" margin-top:2px;">Biografias - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-5">
									<thead>
										<tr>
											<th>ID</th>
											<th>Titulo</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($biografia->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->titulo;?></td>
											<td><?php echo $reg->fecha;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Bibliotecario" data-toggle="modal" data-target="#detalles_biografia<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>							
											</td>
										</tr>
										<div class="modal fade" id="detalles_biografia<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
			  
			   <div class="tab-pane" id="tab_6">
			   	<h3 style=" margin-top:2px;">Actividades - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-6">
									<thead>
										<tr>
											<th>ID</th>
											<th>Actividad</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($actividad->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->nombre_a;?></td>
											<td><?php echo $reg->fecha_reg;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Bibliotecario" data-toggle="modal" data-target="#detalles<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>						
											</td>
										</tr>
										<div class="modal fade" id="detalles<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
			  
			   <div class="tab-pane" id="tab_7">
			   	<h3 style=" margin-top:2px;">Inmuebles - Movimientos</h3>
                 <b><table class="table table-striped table-bordered table-hover" id="dataTables-example-7">
									<thead>
										<tr>
											<th>ID</th>
											<th>Inmueble</th>
											<th>Fecha</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($inmueble->auditoria)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->nombre_i;?></td>
											<td><?php echo $reg->fecha_reg;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Bibliotecario" data-toggle="modal" data-target="#detalles_imuebles<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>							
											</td>
										</tr>
										<div class="modal fade" id="detalles_imuebles<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
				
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Bibliotecario(a):</strong> <?php echo $reg->nombre." ".$reg->apellido;?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
				
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
										<?php }//fin de while?>
									
									</tbody>
								</table></b>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
						
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example-1').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-2').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-3').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-4').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-5').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-6').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
	
	$(document).ready(function() {
        $('#dataTables-example-7').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
    });
    </script>
		
		<?php include('llamado_footer.php');?>