<?php
if(isset($_POST['id']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");

	include("./../public/plugins/fpdf/fpdf.php");
	$id=$_POST['id'];
				$cons_biografia=pg_query("select *from biografias where titulo like '%$id%'");
				$biografia=pg_fetch_object($cons_biografia);
	
		//clase para hacer el Dcocumento
			class PDF extends FPDF {

		//Cabecera del archivo PDF
		function Header() {
		
		$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
			// Logo
			$this->Image('./../public/img/bibliotecanacional.jpg',85,10,40,15);
			$this->SetFont('Arial','B',14);
			$this->Cell(150); //Derecha
			$this->Ln(30); //Salto de línea
			$this->Cell(120); //Derecha
			$this->SetFont('Arial','B',10);
			$this->Cell(0,10,$fecha_hoy,0,1);
			$this->Ln(15); //Salto de línea
		}

		function Footer() {
			$this->SetY(-10);
			$this->SetFont('Arial','I',7);
			// Número de página
			$this->Cell(0,10,'Página '.$this->PageNo().' / {nb}',0,0,'C');
		}
	}

	//Objeto
	$pdf = new PDF();
	$pdf->AliasNbPages();
	//Página
	$pdf->AddPage();
	//Configurando la fuente de la página
	$pdf->SetFont('Times','',12);
	$pdf->Cell(120); //Derecha
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,10,'opopop',0,1);
			$pdf->Cell(60); //Derecha
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,10,$biografia->titulo,0,1);
			//$pdf->Image('./../public/biografias/biografia-1.JPEG',85,10,40,15);
			$pdf->Ln(15); //Salto de línea
	$texto = utf8_decode($biografia->contenido);

	$pdf->MultiCell(176,8,$texto);

	//$url = "http://silvercorp.wordpress.com";

	$pdf->Ln(15); //Salto de línea
	$pdf->SetFont('Arial','B',10);
	//$pdf->MultiCell(176,8,'Nuestra página Web es '.$url);	
	
		
	$pdf->Output();
		}
		else
		{
			include_once("error_404.php");
			//header("Location:./");
		}
?>