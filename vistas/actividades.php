<?php include_once("./../controladores/instancias_actividad.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=6; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=6; include ('menu_administrador.php');
		}?>					</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Actividades</li>
					<li class="active">Listado Actividades</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listado de Actividades</h3> <a href="registrar_actividad.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Actividad</a> <a href="calendario_actividades.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-calendar"></i> Calendario de Actividades</a>
								</div>
								<?php $validar = pg_num_rows($actividad->consulta);?>
								<?php if ($validar>0){?>
								<div class="box-body">
								<table class="table table-striped" id="dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre de Actividad</th>
											<th>Descripci&oacute;n</th>
											<th>Fecha: (Inicio - Fin)</th>
											<th>Hora: (Inicio - Fin)</th>
											<th>Estatus</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($actividad->consulta)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->nombre_a;?></td>
											<td><?php echo $reg->descripcion;?></td>
											<td><?php echo $reg->fecha_inicio." - ".$reg->fecha_fin;?></td>
											<td><?php echo $reg->hora_inicio." - ".$reg->hora_fin;?></td>
											<td><?php if(($reg->estatus=='Finalizada')){?><span class="label label-success"><?php echo $reg->estatus;?></span><?php }else if(($reg->estatus=='Suspendida')){?><span class="label label-danger"><?php echo $reg->estatus;?></span><?php } else {?> <span class="label label-warning"><?php echo $reg->estatus;?></span><?php }?> </td>
											<td class="center">
											<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_actividad;?>"><i class="glyphicon glyphicon-pencil"></i></button>	
											<div class="modal fade" id="actualizar<?php echo $reg->id_actividad;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<b>Alerta</b>
											</div>
											<div class="modal-body">
											<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar los Datos de la Actividad.
											</div>
											<div class="modal-footer">
											<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
											<a class="btn btn-primary btn-primary pull-rigth" href="editar_actividad.php?id=<?php echo $reg->id_actividad;?>">Confirmar</a>
											</div>  
											</div>  
											</div>  
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
											
											<?php if(($reg->estatus=='Finalizada') || ($reg->estatus=='Suspendida')){?>
											<button type="button" disabled="disabled" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Finalizar Actividad" data-toggle="modal" data-target="#finalizar<?php echo $reg->id_actividad;?>"><i class="glyphicon glyphicon-ok"></i></button>
											<button type="button" disabled="disabled" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Suspender Actividad" data-toggle="modal" data-target="#suspender<?php echo $reg->id_actividad;?>"><i class="glyphicon glyphicon-ban-circle"></i></button>	
											<?php }
												  else
												  {
											?>
											<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Finalizar Actividad" data-toggle="modal" data-target="#finalizar<?php echo $reg->id_actividad;?>"><i class="glyphicon glyphicon-ok"></i></button>
											<button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Suspender Actividad" data-toggle="modal" data-target="#suspender<?php echo $reg->id_actividad;?>"><i class="glyphicon glyphicon-ban-circle"></i></button>
											<?php } ?>
											<div class="modal fade" id="suspender<?php echo $reg->id_actividad;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<b>Alerta</b>
											</div>
											<div class="modal-body">
											<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Suspender la Actividad.
											</div>
											<div class="modal-footer">
											<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
											<a class="btn btn-primary btn-primary pull-rigth" href="actividad_estatus.php?suspender=<?php echo $reg->id_actividad;?>">Confirmar</a>
											</div>  
											</div>  
											</div>  
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
											
											<div class="modal fade" id="finalizar<?php echo $reg->id_actividad;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<b>Alerta</b>
											</div>
											<div class="modal-body">
											<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Finalizar la Actividad.
											</div>
											<div class="modal-footer">
											<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
											<a class="btn btn-primary btn-primary pull-rigth" href="actividad_estatus.php?finalizar=<?php echo $reg->id_actividad;?>">Confirmar</a>
											</div>  
											</div>  
											</div>  
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
											
											</td>
										</tr>
										<?php }//fin de while?>
									
									</tbody>
								</table>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					<?php }else {$mensaje=1;}?>
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<?php //include('footer.php');?>
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			 <div class="modal fade" id="actividad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Actualmente no Hay Actividades Registradas.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div> 
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
               responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
    });
    </script>
	<script>
    $(document).ready(function() {
	
		<?php if($mensaje==1){?>
        $('#actividad').modal({ show:true, backdrop:'static'});
		<?php }?>
    });
</script>
		
		<?php include('llamado_footer.php');?>