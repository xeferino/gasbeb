<!-- ChartJS 1.0.1 -->
	<script src="../public/plugins/chartjs/Chart.min.js"></script>
	
<!-- Bootstrap 3.3.6 -->
<script src="../public/bootstrap/js/bootstrap.min.js"></script>

<!-- select2 -->
<script src="../public/plugins/select2/select2.full.min.js"></script>

<!-- timepicker -->
<script src="../public/plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- datepicker-res 
<script src="../public/plugins/datepicker-res/bootstrap-datepicker.js"></script>-->

<!-- datepicker-res lang
<script src="../public/plugins/datepicker-res/locales/bootstrap-datepicker.es.js"></script>-->

<!-- datatables -->
<script src="../public/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="../public/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="../public/dist/js/app.min.js"></script>

<div class="modal fade" id="salir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<b><span class="glyphicon glyphicon-exclamation-sign"></span> Alerta: <?php echo $_SESSION['perfil'];?></b>
											</div>
											<div class="modal-body">
											<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?>, Desea Salir del Sistema.
											</div>
											<div class="modal-footer">
											<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
											<a class="btn btn-primary btn-primary pull-rigth" href="./../controladores/salir.php">Aceptar</a>
											</div>  
											</div>  
											</div>  
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
</body>
</html>