<?php include_once("./../controladores/instancias_inmueble.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=8; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=8; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Inmueble</li>
					<li class="active">Nuevo Inmueble</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-compressed"></i> Nuevo Inmueble</h3> <a href="inmuebles.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Inmuebles</a>
								</div>
						<form class="form-horizontal" action="" method="post" id="formulario_usuario" role="formulario_usuario">
							<input type="hidden" name="actividad" value="registro de cuanta de usuario" class="url">
							<input type="hidden" name="submit" value="new" />
              <div class="box-body">
				<?php if(!$inmueble->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Ingrese Los Datos del Nuevo Registro.
								</div>
								<?php }?>
			
								<?php if($inmueble->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Inmueble Registrado Satisfactoriamente.
								</div>
								<?php }?>
								<?php if($inmueble->mensaje==2){?>
								<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
							<?php }?>
							
							<?php if($inmueble->mensaje==3){?>
								<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Inmueble Inactivo: <?php echo $cantidad_inactivo;?> es Mayor o Igual a Cantidad Inmueble: <?php echo $cantidad_activo;?>
						</div>
							<?php }?>	
               <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Nombre:</label>

                  <div class="col-sm-10">
				  <div class="input-group input-group-icon">
                    <input type="text" class="form-control"  value="<?php echo $inmueble->nombre;?>" name="nombre" onKeyPress="return soloLetras(event)">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-pencil"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Fecha:</label>

                  <div class="col-sm-10">
                    <div class="input-group input-group-icon">
					<input type="text" class="form-control" id="datepicker" name="fecha" value="<?php echo date('Y-m-d');?>">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</span>
					  </div>
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Cantidad:</label>

                  <div class="col-sm-10">
				  <div class="input-group input-group-icon">
                    <input type="text" class="form-control" name="cantidad_activo"  value="<?php echo $inmueble->cantidad_activo;?>" onKeyPress="return solonumeros(event)">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-pencil"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Cant. Inactivo:</label>

                  <div class="col-sm-10">
				  <div class="input-group input-group-icon">
                    <input type="text" class="form-control" name="cantidad_inactivo"  value="<?php echo $inmueble->cantidad_inactivo;?>" onKeyPress="return solonumeros(event)">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-pencil"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
                  </div>
                </div>
				<div align="right">
					 <a href="inmuebles.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Registrar Inmueble" value="Registrar Inmueble" class="btn btn-primary" />
					</div>
              </div>
              <!-- /.box-body -->
                
              <!-- /.box-footer -->
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>
		<script>
		/*$('#datepicker').datepicker({
      		autoclose: true,
	  		language: 'es'
    	});*/
	</script>