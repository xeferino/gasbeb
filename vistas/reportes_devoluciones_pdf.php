<?php
if(isset($_POST['hasta']) && isset($_POST['desde']) && isset($_POST['estatus']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");
	include("./../public/plugins/fpdf/fpdf.php");

	class PDF extends FPDF 
	{ 
	
		function Header() 
		{ 		
			
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
			
			//Logo
			$this->Image('./../public/img/bibliotecanacional.jpg',115,10,40,15);
			//Arial bold 15
			
			$this->SetFont('Arial','B',14); 
			//Movernos a la derecha 
			$this->Cell(30); 
			//T�tulo 
			$this->Ln(2); 
			
			$this->SetFont('Arial','B',10); 
			$this->Cell(36);
			
			$this->Text(100,30,'REP�BLICA BOLIVARIANA DE VENEZUELA');
	
			$this->Text(107,36.5,'BIBLIOTECA "EDUARDO BLANCO"');
	
			$this->Text(93,43,'MUNICIPIO RIBERO - CARIACO � ESTADO � SUCRE');
			
			$this->Text(162,52,$fecha_hoy);

			$this->SetFont('Arial','',6); 
		
			$this->SetFont('Arial','',10); 
			//Salto de l�nea 
			$this->Ln(10); 
			$this->SetY(60);
		} 
	
		//Pie de p�gina 
		function Footer() 
		{ 
		   //$fecha=date("d/m/Y");
		  //Posicion
			$this->SetY(-35.8); 
			//Arial italic 8 
			$this->SetFont('Arial','I',10); 
			//N�mero de p�gina 
			$this->Cell(0,30,'P�gina '.$this->PageNo().'/{nb}',0,0,'C'); 
			$this->Text(148,259.5,''); 
			$this->Text(180,259.5,'');
		} 
	
		function __construct() 
		{        
			//Llama al constructor de su clase Padre. 
			//Modificar aka segun la forma del papel del reporte 
			parent::__construct('L','mm','Letter'); 
		}
	}// fin de class
			
    //Creaci�n del objeto de la clase heredada 
		$pdf=new PDF(); 
		$pdf->SetTopMargin(10.8); 
		$pdf->SetLeftMargin(10.5);     
		$pdf->AliasNbPages(); 
		$pdf->SetFont('Arial','B',10);
		
				$desde =  $_POST['desde'];
				$hasta =  $_POST['hasta'];
				$estatus =  $_POST['estatus'];
				
				//echo "hola"."".$estatus;
				
				if($estatus=="Todos")
				{ 
					$cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$desde' AND '$hasta' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$valido = pg_num_rows ($cons);
				}
				
				if($estatus=="Entregados")
				{
					$cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                   
												  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$desde' AND '$hasta' AND 
												  prestamo_libro.prestamo_estatus='Retornado' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$valido = pg_num_rows ($cons);
				}
				
				if($estatus=="Pendientes")
				{
					$cons = pg_query("SELECT prestamo.id_prestamo, prestamo.tipo, prestamo.fecha_prestamo,                                                  prestamo.fecha_entrega, usuario.primer_nombre,                                                  usuario.primer_apellido, prestamo_libro.id_prestamo_libro,                                                  prestamo_libro.id_libro, prestamo_libro.fecha_devolucion,                                                  prestamo_libro.prestamo_estatus, libro.titulo, libro.isbn
                                                  FROM libro, prestamo, usuario, prestamo_libro
                                                  WHERE prestamo.id_usuario=usuario.id_usuario and                                                  libro.id_libro=prestamo_libro.id_libro and                                                  prestamo.id_prestamo=prestamo_libro.id_prestamo and prestamo.fecha_prestamo 
												  BETWEEN '$desde' AND '$hasta' AND 
												  prestamo_libro.prestamo_estatus='Pendiente' ORDER BY 
												  prestamo_libro.id_prestamo_libro DESC");
					$valido = pg_num_rows ($cons);
				}
				
				
		if($estatus == "Todos"){$reporte="Listado de Todos los Prestamos Existentes";}
		if($estatus != "Todos"){$reporte="Listado de Todos los Prestamos: ($estatus)";}	

        $j=1; 
    	$pdf->AddPage();
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',10);			
		$pdf->Cell(260,6,utf8_decode($reporte).", Para el Rango de Fecha Seleccionado: DESDE (".$desde.") HASTA (".$hasta.")",0,'','C',1);
		$pdf->Ln(6);	
		$pdf->SetFillColor(236,236,236);
		$pdf->Ln(6);
		$pdf->SetFont('Arial','B',11);	
		$pdf->Cell(15,6,'ID',0,'','C',1);
		$pdf->Cell(55,6,'Usuario',0,'','C',1);
		$pdf->Cell(55,6,'Tipo',0,'','C',1);
		$pdf->Cell(35,6,'Expedido',0,'','C',1);
		$pdf->Cell(35,6,'Vence',0,'','C',1);
		$pdf->Cell(35,6,'Entrega',0,'','C',1);
		$pdf->Cell(35,6,'Estatus',0,'','C',1);
        
         while($prestamos=pg_fetch_array($cons))//mostrar todos
    	{
			$i++;						  		  			  
				if($i%2 == 1)
				{										
					$pdf->Ln(6);	
					$pdf->SetFillColor(255,255,255);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(15,6,$i,0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(55,6,$prestamos['primer_nombre']." ".$prestamos['primer_apellido'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(55,6,$prestamos['tipo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['fecha_prestamo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['fecha_entrega'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					if ($prestamos['fecha_devolucion']==NULL){$pdf->Cell(35,6,'----',0,'','C',1);}
					else{$pdf->Cell(35,6,$prestamos['fecha_devolucion'],0,'','C',1);}					
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['prestamo_estatus'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					
				}
			
				else
				{
					$pdf->Ln(6);	
					$pdf->SetFillColor(236,236,236);
					$pdf->SetFont('Arial','B',10);
					$pdf->Cell(15,6,$i,0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(55,6,$prestamos['primer_nombre']." ".$prestamos['primer_apellido'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(55,6,$prestamos['tipo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['fecha_prestamo'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['fecha_entrega'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					if ($prestamos['fecha_devolucion']==NULL){$pdf->Cell(35,6,'----',0,'','C',1);}
					else{$pdf->Cell(35,6,$prestamos['fecha_devolucion'],0,'','C',1);}
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(35,6,$prestamos['prestamo_estatus'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
				}
						
		  $pdf->SetAutoPageBreak(true,40);
          $j=$j+1; 
			
		
        }
						
		 $pdf->cell(0,30,'',0,1);
         $pdf->Text(115,$pdf->GetY(),'________________________'); 
         $pdf->cell(0,5,'',0,1);
         $pdf->Text(130,$pdf->GetY(),'Bibliotecario');			 
		 /*------------------------------------------------------------------------------------------------*/
		
		 
		 $pdf->Output();
}
else
{
	include_once("error_404.php");
	//header("Location:./");
}
 
?>
