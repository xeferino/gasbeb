<?php include_once("./../controladores/instancias_actividad.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); 
	
	$query= pg_query("SELECT id_actividad, nombre_a, fecha_inicio, fecha_fin, hora_inicio, hora_fin FROM actividad");
	$actividades = pg_fetch_assoc($query);
	
	?>	

<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">
    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=6; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=6; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Actividades</li>
					<li class="active">Calendario de Actividades</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
								<h3 class="box-title"><i class="glyphicon glyphicon-calendar"></i> Calendario de Actividades</h3> <a href="actividades.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Actividades</a>
<a href="registrar_actividad.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Actividad</a>
								</div>
								<div class="box-body">
									<div id='calendar'></div>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
	
	<script>

	$(document).ready(function() {

		$('#calendar').fullCalendar({
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '<?php echo date('Y/m/d');?>',
			editable: true,
			events: [
					
					<?php $i=0;?>			
					<?php while($reg=pg_fetch_object($actividad->consulta)){?>
					<?php $i++;?>
					
				{
					title:'<?php echo $reg->nombre_a;?>',
					start:'<?php echo $reg->fecha_inicio;?>',
					end:'<?php echo $reg->fecha_fin;?>'
				},
			<?php }//fin de while?>
			]
		});
		
	});

</script>
		<?php /*foreach($actividades as $actividad): 
			
				$start = explode(" ", $actividad['fecha_inicio']);
				$end = explode(" ", $actividad['fecha_fin']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $actividad['fecha_inicio'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $actividad['fecha_fin'];
				}
			?>
				{
					id: '<?php echo $actividad['id_actividad']; ?>',
					title: '<?php echo $actividad['nombre']; ?>',
					start: '<?php echo $start;?>',
					end: '<?php echo $end; ?>'
					
				},
			<?php endforeach; */?>
		<?php include('llamado_footer.php');?>