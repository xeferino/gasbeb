<?php
if(isset($_POST['hasta']) && isset($_POST['desde']) && isset($_POST['estatus']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");
	include("./../public/plugins/fpdf/fpdf.php");

	class PDF extends FPDF 
	{ 
	
		function Header() 
		{ 		
			
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
			
			//Logo
			$this->Image('./../public/img/bibliotecanacional.jpg',115,10,40,15);
			//Arial bold 15
			
			$this->SetFont('Arial','B',14); 
			//Movernos a la derecha 
			$this->Cell(30); 
			//T�tulo 
			$this->Ln(2); 
			
			$this->SetFont('Arial','B',10); 
			$this->Cell(36);
			
			$this->Text(100,30,'REP�BLICA BOLIVARIANA DE VENEZUELA');
	
			$this->Text(107,36.5,'BIBLIOTECA "EDUARDO BLANCO"');
	
			$this->Text(93,43,'MUNICIPIO RIBERO - CARIACO � ESTADO � SUCRE');
			
			$this->Text(162,52,$fecha_hoy);

			$this->SetFont('Arial','',6); 
		
			$this->SetFont('Arial','',10); 
			//Salto de l�nea 
			$this->Ln(10); 
			$this->SetY(60);
		} 
	
		//Pie de p�gina 
		function Footer() 
		{ 
		   //$fecha=date("d/m/Y");
		  //Posicion
			$this->SetY(-35.8); 
			//Arial italic 8 
			$this->SetFont('Arial','I',10); 
			//N�mero de p�gina 
			$this->Cell(0,30,'P�gina '.$this->PageNo().'/{nb}',0,0,'C'); 
			$this->Text(148,259.5,''); 
			$this->Text(180,259.5,'');
		} 
	
		function __construct() 
		{        
			//Llama al constructor de su clase Padre. 
			//Modificar aka segun la forma del papel del reporte 
			parent::__construct('L','mm','Letter'); 
		}
	}// fin de class
			
    //Creaci�n del objeto de la clase heredada 
		$pdf=new PDF(); 
		$pdf->SetTopMargin(10.8); 
		$pdf->SetLeftMargin(10.5);     
		$pdf->AliasNbPages(); 
		$pdf->SetFont('Arial','B',10);
		
				$desde =  $_POST['desde'];
				$hasta =  $_POST['hasta'];
				$estatus =  $_POST['estatus'];
				
				if($estatus=="Todas")
				{ 
					$consulta_a = pg_query("SELECT *FROM actividad WHERE fecha_inicio BETWEEN '$desde' AND '$hasta'");
				}
				else
				{
					$consulta_a = pg_query("SELECT *FROM actividad WHERE fecha_inicio BETWEEN '$desde' AND '$hasta' AND estatus='$estatus'");
				}
				
				
		if($estatus == "Todas"){$reporte="Listado de Todas las Actividades Existentes";}
		if($estatus != "Todas"){$reporte="Listado de Todas las Actividades: $estatus";}	

        $j=1; 
    	$pdf->AddPage();
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','',12);			
		$pdf->Cell(258,6,utf8_decode($reporte).", Para el Rango de Fecha Seleccionado: DESDE (".$desde.") HASTA (".$hasta.")",0,'','C',1);
		$pdf->Ln(6);	
		$pdf->SetFillColor(212,212,212);
		$pdf->Ln(6);
		$pdf->Cell(8,6,'ID',0,'','C',1);
		$pdf->Cell(100,6,'Nombre',0,'','C',1);
		$pdf->Cell(60,6,'Fecha: (Inicio - Fin)',0,'','C',1);
		$pdf->Cell(60,6,'Hora: (Inicio - Fin)',0,'','C',1);
		$pdf->Cell(30,6,'Estatus',0,'','C',1);

		
        
         while($actividades=pg_fetch_array($consulta_a))//mostrar todos
    	{
			$i++;				  		  			  
				if($i%2 == 1)
				{
					$pdf->Ln(6);	
					$pdf->SetFillColor(255,255,255);
					$pdf->Cell(8,6,$i,0,'','J',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(100,6,$actividades['nombre_a'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(60,6,$actividades['fecha_inicio']." - ".$actividades['fecha_fin'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(60,6,$actividades['hora_inicio']." - ".$actividades['hora_fin'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Cell(30,6,$actividades['estatus'],0,'','C',1);
					$pdf->SetFont('Arial','',10);
					$pdf->Ln(6);
					$pdf->Ln(3);
					$pdf->Cell(258,6,utf8_decode('Descripcion:')." ".$actividades['descripcion'],0,'','J',1);
					$pdf->Ln(3);
				}
			
				else
				{
				$pdf->Ln(6);	
				$pdf->SetFillColor(236,236,236);
				$pdf->Cell(8,6,$i,0,'','J',1);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(100,6,$actividades['nombre_a'],0,'','C',1);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(60,6,$actividades['fecha_inicio']." - ".$actividades['fecha_fin'],0,'','C',1);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(60,6,$actividades['hora_inicio']." - ".$actividades['hora_fin'],0,'','C',1);
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(30,6,$actividades['estatus'],0,'','C',1);
				$pdf->SetFont('Arial','',10);
				$pdf->Ln(6);
				$pdf->Ln(3);
				$pdf->Cell(258,6,utf8_decode('Descripcion:')." ".$actividades['descripcion'],0,'','J',1);
				$pdf->Ln(3);
				}
						
		  $pdf->SetAutoPageBreak(true,40);
          $j=$j+1; 
			
		
        }
						
		 $pdf->cell(0,30,'',0,1);
         $pdf->Text(115,$pdf->GetY(),'________________________'); 
         $pdf->cell(0,5,'',0,1);
         $pdf->Text(130,$pdf->GetY(),'Bibliotecario');			 
		 /*------------------------------------------------------------------------------------------------*/
		
		 
		 $pdf->Output();
}
else
{
	include_once("error_404.php");
	//header("Location:./");
}
 
?>
