<?php
if(isset($_POST['id']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");

	include("./../public/plugins/fpdf/fpdf.php");
	
		//clase para hacer el Dcocumento
		class PDF extends FPDF 
		{  
			function Header() 
			{ 		
				//Logo
				$this->Image('./../public/img/bibliotecanacional.jpg',85,10,40,15);
				//Arial bold 15
				$this->Ln(25);
				$this->SetFillColor(230,230,200);
				$this->SetFont('Arial','',10);
				$this->SetY(35); 
			} 
		
			//Pie de página 
			function Footer() 
			{ 
			   //$fecha=date("d/m/Y");
			  //Posicion
				$this->SetY(-35.8); 
				//Arial italic 8 
				$this->SetFont('Arial','I',7); 
				//Número de página 
				$this->Text(148,259.5,''); 
				$this->Text(180,259.5,'');
			} 
		
			function __construct() 
			{        
				//Llama al constructor de su clase Padre. 
				//Modificar aka segun la forma del papel del reporte 
				parent::__construct('P','mm','Letter'); 
			}
			
		}//fin de class
		
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
		
			
				$id=$_POST['id'];
				$cons_biografia=pg_query("select *from biografias where titulo like '%$id%'");
				$biografia=pg_fetch_object($cons_biografia);
		
			$pdf=new PDF();      
			$pdf->AliasNbPages(); 
			$pdf->SetFont('Arial','B',10);
			
			$j=1; 
			
			$pdf->AddPage();     
			/*$pdf->SetFillColor(229,229,229);
			$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(104,5,utf8_decode('Ficha de Usuario Circulante'),'','','L','');
			$pdf->Cell(73.5,5,utf8_decode($fecha_hoy),'','','R','');*/
			
			//DATOS DE LA DENUNCIA
			$pdf->Ln(10);
			/*$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(177.5,5,'Datos del Lector',1,'','L',1);*/
	
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 90); 
			$pdf->Cell(50,10,'Tipo de Lector: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,10,utf8_decode($biografia->titulo),'','','L','');
			
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 65); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Apellidos: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->primer_apellido." ".$usuario->segundo_apellido),'','','L','');
			
						
			
			$j=$j+1;
			$pdf->Output();
		}
		else
		{
			include_once("error_404.php");
			//header("Location:./");
		}
?>