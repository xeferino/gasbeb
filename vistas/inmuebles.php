<?php include_once("./../controladores/instancias_inmueble.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=8; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=8; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Inmuebles</li>
					<li class="active">Listado Inmuebles</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listado de Inmuebles</h3> <a href="registrar_inmueble.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Inmueble</a>
								</div>
								<?php $validar = pg_num_rows($inmueble->consulta);?>
								<?php if ($validar>0){?>
								<div class="box-body">
								<table class="table table-striped" id="dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre de Inmueble</th>
											<th>Fecha</th>
											<th>Cantidad-Activo</th>
											<th>Cantidad-Inactivo</th>
											<th>Total-Inmuebles</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($inmueble->consulta)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->nombre_i;?></td>
											<td><?php echo $reg->fecha;?></td>
											<td><?php $descontar=$reg->cantidad_activo-$reg->cantidad_inactivo;?> <span class="label label-success"><?php echo  $descontar;?></span></td>
											<td><?php if ($reg->cantidad_inactivo=='0'){echo "---";}else {echo '<span class="label label-danger">'.$reg->cantidad_inactivo;}?></span></td>
											<td><span class="label label-warning"><?php echo $reg->cantidad_activo;?></span> </td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_inmueble;?>" href="">
											<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<div class="modal fade" id="actualizar<?php echo $reg->id_inmueble;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
											<div class="modal-dialog">
											<div class="modal-content">
											<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<b>Alerta</b>
											</div>
											<div class="modal-body">
											<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar los Datos del Inmueble.
											</div>
											<div class="modal-footer">
											<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
											<a class="btn btn-primary btn-primary pull-rigth" href="editar_inmueble.php?id=<?php echo $reg->id_inmueble;?>">Confirmar</a>
											</div>  
											</div>  
											</div>  
											<!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
											</div>
											
											</td>
										</tr>
										<?php }//fin de while?>
									
									</tbody>
								</table>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					<?php }else {$mensaje=1;}?>
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<?php //include('footer.php');?>
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			<div class="modal fade" id="inmuebles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Actualmente no Hay Inmuebles Registrados.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div> 
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
    });
    </script>
	
	<script>
    $(document).ready(function() {
	
		<?php if($mensaje==1){?>
        $('#inmuebles').modal({ show:true, backdrop:'static'});
		<?php }?>
    });
</script>
		
		
		<?php include('llamado_footer.php');?>