<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GASBEB</title>
   <link rel="shortcut icon" type="image/x-icon" href="../public/img/minilogo.ico">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../public/dist/css/AdminLTE.min.css">
	
	<!-- AdminLTE Skins. Choose a skin from the css/skins-->
	<link rel="stylesheet" href="../public/dist/css/skins/_all-skins.min.css">  
	
	<!-- datepicker-->
	<link rel="stylesheet" href="../public/plugins/datepicker/jquery-ui.css">
	
	<!-- datepicker-res
	<link rel="stylesheet" href="../public/plugins/datepicker-res/datepicker3.css">-->
	
	
	<!-- fullcalendar-->
	<link rel='stylesheet' href='../public/plugins/fullcalendar/lib/cupertino/jquery-ui.min.css' />
	<link href='../public/plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
	<link href='../public/plugins/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
	
	<!-- select2-->
	<link rel="stylesheet" href="../public/plugins/select2/select2.min.css">
	
	 <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../public/plugins/timepicker/bootstrap-timepicker.min.css">
  
  <!-- datatables-->
	<link href="../public/plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
	<link href="../public/plugins/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
	
	<!-- jQuery-->
	<script src="../public/plugins/jQuery/jQuery-2.2.0.min.js"></script>
	
	<!-- fullcalendar-->
	<script src='../public/plugins/fullcalendar/lib/moment.min.js'></script>
	<script src='../public/plugins/fullcalendar/lib/jquery.min.js'></script>
	<script src='../public/plugins/fullcalendar/lib/jquery-ui.custom.min.js'></script>
	<script src='../public/plugins/fullcalendar/fullcalendar.min.js'></script>
	<script src='../public/plugins/fullcalendar/lang/es.js'></script>
	
	<!-- datepicker-->
	<script src="../public/plugins/datepicker/jquery-ui.js"></script>
	<script src="../public/plugins/datepicker/datepicker-es.js"></script>
	
	<!-- funciones-->
	<script src="../public/funciones.js/valida_num_letras.js"></script>
     
    <script>
	$(function () {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#datepicker").datepicker({
	firstDay: 0,
	changeMonth: true,
	changeYear : true,
	dateFormat: "dd-mm-yy",
	yearRange: "1900:2099"
	});
	});
	</script>
	<script>
	$(function () {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#campofecha").datepicker({
	firstDay: 0,
	changeMonth: true,
	changeYear : true,
	dateFormat: "dd-mm-yy",
	yearRange: "1900:2099"
	});
	});
	</script>
	<script>
	$(function () {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	$("#campofecha1").datepicker({
	firstDay: 0,
	changeMonth: true,
	changeYear : true,
	dateFormat: "dd-mm-yy",
	yearRange: "1900:2099"
	});
	});
	</script>
	
	<script language="JavaScript">
		function hora(){
		var fecha = new Date()
		var hora = fecha.getHours()
		var minuto = fecha.getMinutes()
		var segundo = fecha.getSeconds()
		if (hora < 10) {hora = "0" + hora}
		if (minuto < 10) {minuto = "0" + minuto}
		if (segundo < 10) {segundo = "0" + segundo}
		var horita = hora + ":" + minuto + ":" + segundo
		document.getElementById('hora').firstChild.nodeValue = horita
		tiempo = setTimeout('hora()',1000)
		}
		function inicio(){
		document.write('<strong id="hora">')
		document.write ('000000</strong>')
		hora()
		}
	</script>
<style>

	body {
		margin: 0;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		width: 90%;
		margin: 5px auto;
	}
	.fc-day-number{
		font-size: 12px;
	}

</style>
</head>
<?php  
		date_default_timezone_set('America/Caracas');
		$mes= date('m'); $dia= date('d');$ano= date('Y');
		if($mes == 1){ $mes = "Enero";}	if($mes == 2){ $mes = "Febrero";} if($mes == 3){ $mes = "Marzo";}
		if($mes == 4){ $mes = "Abril";}	if($mes == 5){ $mes = "Mayo";} if($mes == 6){ $mes = "Junio";}
		if($mes == 7){ $mes = "Julio";}	if($mes == 8){ $mes = "Agosto";} if($mes == 9){ $mes = "Septiembre";}
		if($mes == 10){ $mes = "Octubre";} if($mes == 11){ $mes = "Noviembre";} if($mes == 12){ $mes = "Diciembre";}
		
?>
