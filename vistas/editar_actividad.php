<?php include_once("./../controladores/instancias_actividad.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<?php 
		if(isset($_GET['id']))
				{
					$actividad->datos_actividad($_GET['id']);
				}
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'editar') {	
	
		$nombre =  $_POST['nombre'];
		$descripcion =  $_POST['descripcion'];
		$fecha_inicio =  $_POST['fecha_inicio'];
		$fecha_fin =  $_POST['fecha_fin'];
		$hora_inicio =  $_POST['hora_inicio'];
		$hora_fin =  $_POST['hora_fin'];
		$estatus =  $_POST['estatus'];
		
		$id=$_POST['id'];
	
		$actividad->inicializar_datos ($nombre, $descripcion, $fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $estatus);
		$actividad->actualizar_actividad($id);
	}
?>
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=6; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=6; include ('menu_administrador.php');
		}?>	
				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Actividad</li>
					<li class="active">Actualizar Actividad</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-calendar"></i> Actualizar Actividad</h3> <a href="actividades.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Actividades</a>
									<a href="registrar_actividad.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Actividad</a> <a href="calendario_actividades.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-calendar"></i> Calendario de Actividades</a>
								</div>
						<form class="form-horizontal" action="" method="post" id="formulario_usuario" role="formulario_usuario">
							<input type="hidden" name="id" value="<?php echo $actividad->reg->id_actividad;?>">
							<input type="hidden" name="submit" value="editar" />
              <div class="box-body">
				<?php if(!$actividad->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Actualize Los Datos de la Actividad.
								</div>
								<?php }?>
			
								<?php if($actividad->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Actividad Actualizada Satisfactoriamente.
								</div>
								<?php }?>
								<?php if($actividad->mensaje==2){?>
								<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
							<?php }?>		
               <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Nombre:</label>

                  <div class="col-sm-10">
				      <input type="text" class="form-control"  value="<?php echo $actividad->nombre;?>" name="nombre" onKeyPress="return soloLetras(event)">
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Descripci&oacute;n:</label>

                  <div class="col-sm-10">
				   	<textarea name="descripcion"  value="" class="form-control" cols="" rows="" onKeyPress="return soloLetras(event)"><?php echo $actividad->descripcion;?></textarea>
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="text" class="col-sm-2 control-label">Inicio:</label>
                  <div class="col-sm-5">
					  <div class="input-group input-group-icon">
						<input type="text" class="form-control" id="campofecha" value="<?php echo $actividad->fecha_inicio;?>" name="fecha_inicio" title="Fecha Inicio" id="" placeholder="">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
					</div>
					 <div class="col-sm-5">
					 <div class="bootstrap-timepicker">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" name="hora_inicio" value="<?php echo $actividad->hora_inicio;?>" title="Hora Inicio" id="timepicker-inicio" placeholder="">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-time"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
					  </div>
					  </div>
				</div>
				<div class="form-group has-feedback">
                  <label for="text" class="col-sm-2 control-label">Fin:</label>
                  <div class="col-sm-5">
					  <div class="input-group input-group-icon">
						<input type="text" class="form-control" id="campofecha1" name="fecha_fin" value="<?php echo $actividad->fecha_fin;?>" title="Fecha Fin" id="" placeholder="">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
					</div>
					 <div class="col-sm-5">
					 <div class="bootstrap-timepicker">
					<div class="input-group input-group-icon">
						<input type="text" class="form-control" name="hora_fin" value="<?php echo $actividad->hora_fin;?>" title="Hora Fin" id="timepicker-fin" placeholder="">
							<span class="input-group-addon">
								<span class="icon icon-lg">
									<i class="glyphicon glyphicon-time"></i>
								</span>
							</span>
							<span class="help-block"></span>
					  </div>
					  </div>
					  </div>
				</div>
				
				<div class="form-group">
                  <label class="col-sm-2 control-label">Estatus</label>

                  <div class="col-sm-10">
                    <!--<input type="text" class="form-control" name="estatus" id="estatus" placeholder="Estatus de Usuario">-->
                    <select class="form-control" name="estatus" id="Estatus">
					<?php if($actividad->estatus=="Finalizada"){?>
					
					<option  value="<?php echo $actividad->estatus;?>" selected="selected"><?php echo $actividad->estatus;?></option>
					<option  value="Iniciada">Iniciada</option>
					<option  value="Suspendida">Suspendida</option>
					<?php } 
						  	if($actividad->estatus=="Suspendida")

						  {
					?>
					<option  value="<?php echo $actividad->estatus;?>" selected="selected"><?php echo $actividad->estatus;?></option>
					<option  value="Iniciada">Iniciada</option>
					<option  value="Finalizada">Finalizada</option>
					<?php }?>
					
					<?php 
						  	if($actividad->estatus=="Iniciada")

						  {
					?>
					<option  value="<?php echo $actividad->estatus;?>" selected="selected"><?php echo $actividad->estatus;?></option>
					<option  value="Suspendida">Suspendida</option>
					<option  value="Finalizada">Finalizada</option>
					<?php }?>
                  </select>
                  </div>
                </div>
				<div align="right">
				 	<a href="actividades.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Actualizar Actividad" value="Actualizar Actividad" class="btn btn-primary" />
					</div>
              </div>
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>
		<script>
			//Timepicker
			$("#timepicker-inicio").timepicker({
			  showInputs: false
			});
			$("#timepicker-fin").timepicker({
			  showInputs: false
			});
	</script>