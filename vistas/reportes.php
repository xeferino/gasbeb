<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=9; include ('menu_administrador.php');
		}?>				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Reportes</li>
					<li class="active">Informe de Movimientos</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-th-large"></i> Gestionar Reportes <a href="./" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Cancelar</a>
            <small class="pull-right">Fecha: <?php echo date('d-m-Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
	  <div class="row">
	  <a href="reporte_usuarios.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Usuarios <span class="pull-right"><i class="glyphicon glyphicon-user"></i></span>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->
        <a href="reporte_prestamos.php" style="color:#000000;">
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Prestamos <span class="pull-right"><i class="glyphicon glyphicon-open"></i></span>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <a href="reporte_devoluciones.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Devoluciones <span class="pull-right"><i class="glyphicon glyphicon-save"></i></span>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->
		<a href="reporte_ejemplares.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Ejemplares <span class="pull-right"><i class="glyphicon glyphicon-book"></i></span>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		 </a>
        <!-- /.col -->
      </div>
	  <div class="row">
	  <a href="reporte_actividades.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             Actividades <span class="pull-right"><i class="glyphicon glyphicon-calendar"></i></span>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->
		<a href="reporte_inmuebles.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Inmuebles <span class="pull-right"><i class="glyphicon glyphicon-compressed"></i></span>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
<a href="reporte_biografias.php" style="color:#000000;">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><img src="../public/img/pdf.gif" width="20" height="20"></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Biografias <span class="pull-right"><i class="glyphicon glyphicon-modal-window"></i></span>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
		</a>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Informe</h3> <span class="pull-right"><a href="reporte_usuario.php"><img src="../public/img/pdf.gif" width="20" height="20"></a></span>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Configuraci&oacute;n <span class="pull-right"><i class="glyphicon glyphicon-cog"></i></span>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php include('llamado_footer.php');?>