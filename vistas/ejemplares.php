<?php include_once("./../controladores/instancias_ejemplar.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=5; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=5; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Ejemplares</li>
					<li class="active">Listado de Libros</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listado de Libros</h3> <a href="registrar_ejemplar.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Ejemplar</a> <a href="areas.php"class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Categorias</a> <a href="editoriales.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Editoriales</a>

								</div>
								<?php $validar = pg_num_rows($ejemplar->ejemplares);?>
								<?php if ($validar>0){?>
								<div class="box-body">
								<table class="table table-striped" id="dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>ISBN</th>
											<th>Titulo</th>
											<th>Area</th>
											<th>Autor</th>
											<th>Cant. Activa</th>
											<th>Cant. Inactiva</th>
											<th>Ejemplares</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($ejemplar->ejemplares)){?>
								<?php $i++;?>
								<?php $cantidad=$reg->ejemplares-$reg->cant_inactivos;?>
									<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->isbn;?></td>
											<td><?php echo $reg->titulo;?></td>
											<td><?php echo $reg->area;?></td>
											<td><?php echo $reg->autor1;?></td>
											<td><span class="label label-success"><?php echo  $cantidad;?></span></td>
											<td><?php if ($reg->cant_inactivos==0){echo "---";}else {echo '<span class="label label-danger">'.$reg->cant_inactivos;}?></span></td>
											<td><span class="label label-warning"><?php echo $reg->ejemplares;?></span></td>

											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_libro;?>" href="">
											<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-info modal-basic" title="Detalles" data-toggle="modal" data-target="#detalles<?php echo $reg->id_libro;?>" href="">
											<i class="glyphicon glyphicon-book"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-info modal-basic" title="Inventario" data-toggle="modal" data-target="#inventario<?php echo $reg->id_libro;?>" href="">
											<i class="glyphicon glyphicon-book"></i>
											</a>
											<div class="modal fade" id="actualizar<?php echo $reg->id_libro;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar los Datos del Ejemplar.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="editar_ejemplar.php?id=<?php echo $reg->id_libro;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
							<div class="modal fade" id="inventario<?php echo $reg->id_libro;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar la Cantidad del Ejemplar: (<?php echo $reg->titulo;?>).
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="editar_ejemplar_cantidad.php?id=<?php echo $reg->id_libro;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
										<div class="modal fade" id="detalles<?php echo $reg->id_libro;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b><span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Ficha del Ejemplar</b>
			</div>
			<div class="modal-body">
			
				<section class="invoice">
					<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
									<i class="glyphicon glyphicon-book"></i> Ejemplar (<?php echo $reg->id_libro;?>)
									<small class="pull-right"><b>Cota:</b> <?php echo $reg->cota;?></small>
								</h2>
							</div> 
						      <div class="row invoice-info">
							  	<address>
									<div align="justify">
				<table width="500" border="1" align="center">
				  <tr>
					<td width="229">&nbsp;<b>Titulo:</b><br>&nbsp;<?php echo $reg->titulo;?></td>
					<td width="255">&nbsp;<b>Autor (es):</b><br>&nbsp;<?php echo $reg->autor1.", ".$reg->autor2.", ".$reg->autor3;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>ISBN:</b><br>&nbsp;<?php echo $reg->isbn;?></td>
					<td width="255">&nbsp;<b>Area:</b><br>&nbsp;<?php echo $reg->area.", ".$reg->codigo;?>.</td>
				  </tr>
				  <tr>
					<td width="229">&nbsp;<b>Editorial:</b><br>&nbsp;<?php echo $reg->nombre.", ".$reg->ciudad.", ".$reg->pais;?>.</td>
					<td width="255">&nbsp;<b>Edicion:</b><br>&nbsp;<?php echo $reg->edicion.", A&ntilde;o: ".$reg->anio;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>Num. Paginas:</b><br>&nbsp;<?php echo $reg->pag_vol;?>.</td>
					<td width="255">&nbsp;<b>Ejemplares:</b><br>&nbsp;<?php echo $reg->ejemplares;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>Estatus:</b><br>&nbsp;<?php echo $reg->estatus;?>.</td>
					<td width="255">&nbsp;<b>Ubicacion:</b><br>&nbsp;Sala <?php echo $reg->sala;?>.</td>
				  </tr>
				  <tr>
					<td colspan="2">&nbsp;<b>Observaciones:</b><br>&nbsp;<?php echo $reg->observaciones;?>.</td>
					</tr>
				</table>
									</div>
								</address>
							  </div>
						<!-- /.col -->
					</div>
				</section>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-primary btn-primary pull-rigth" data-dismiss="modal"></span> Aceptar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
											</td>
										</tr>
										<?php }//fin de while?>
									
									</tbody>
								</table>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					<?php }else {$mensaje=1;}?>
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<?php //include('footer.php');?>
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			<div class="modal fade" id="ejemplares" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Actualmente no Hay Ejemplares Registrados.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div> 
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[7]}]
        });
    });
    </script>
	
	<script>
    $(document).ready(function() {
	
		<?php if($mensaje==1){?>
        $('#editoriales').modal({ show:true, backdrop:'static'});
		<?php }?>
    });
</script>
		
		<?php include('llamado_footer.php');?>