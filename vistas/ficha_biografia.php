<?php include_once("./../controladores/instancias_biografia.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<?php 
		if(isset($_GET['id']))
				{
					$biografia->datos_biografia($_GET['id']);
				}
?>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=7; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=7; include ('menu_administrador.php');
		}?>				</aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Biograf&iacute;as</li>
					<li class="active">Ver Biograf&iacute;a</li>
					</ol>
					</section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-th-large"></i> Biograf&iacute;a <a href="biografias.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a>
            <small class="pull-right">Fecha: <?php echo date('m-d-Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
	  <div class="col-sm-12">
		<div class="left col-md-12">
		<div align="center">
			<img src="../<?php echo $biografia->imagen;?>" alt="" class="img-circle img-responsive" width="200" height="200">
		  <h2><?php echo $biografia->titulo;?></h2>
		  <p><strong>Autor: </strong> <?php echo $biografia->autor;?> </p>
		  <p align="justify"><strong>Sinopsis: </strong> <?php echo $biografia->contenido;?> </p>
		</div>
      </div>
	  </div>
	  <div class="row no-print">
        <div class="col-xs-12">
          <form action="ficha_biografia_pdf.php" method="post" target="_blank">
			<input type="hidden" name="id" value="<?php echo $biografia->titulo;?>">
			  <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-file"></i> Generar PDF
			  </button>
		 </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<?php include('llamado_footer.php');?>