<?php include_once("./../controladores/instancias_ejemplar.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=5; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=5; include ('menu_administrador.php');
		}?>			</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Ejemplares</li>
					<li class="active">Nuevo Ejemplar</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
            <div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-book"></i> Nuevo Ejemplar</h3> <a href="ejemplares.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Ejemplares</a>
								</div>
            <!-- /.box-header -->
            <!-- form start -->
           <form method="post" id="formulario_usuario" role="form">
			<input type="hidden" name="actividad" value="registro de cuanta de usuario" class="url">
			<input type="hidden" name="submit" value="new" />
              <div class="box-body">
			  <?php if(!$ejemplar->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Ingrese Los Datos del Nuevo Registro.
								</div>
								<?php }?>
			
								<?php if($ejemplar->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Ejemplar Registrado Satisfactoriamente.
								</div>
								<?php }?>
								
						<?php if($ejemplar->mensaje==2){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, ISBN no Disponible.
						</div>
						<?php }?>
						
						<?php if($ejemplar->mensaje==3){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, Cota no Disponible.
						</div>
						<?php }?>
						
						<?php if($ejemplar->mensaje==4){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
						<?php }?>
		<div class="col-sm-12">
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">T&iacute;tulo:</label>
                  <input type="text" name="titulo" value="<?php echo $ejemplar->titulo;?>" class="form-control">
                </div>
				<div class="form-group">
                  <label for="form-control">Edici&oacute;n:</label>
                  <input type="text" name="edicion"  value="<?php echo $ejemplar->edicion;?>"  class="form-control">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">ISBN:</label>
                  <input type="text" name="isbn"  value="<?php echo $ejemplar->isbn;?>"  class="form-control">
                </div>
				<div class="form-group">
                  <label for="form-control">A&ntilde;o:</label>
                  <input type="text" name="anio"  value="<?php echo $ejemplar->anio;?>"  class="form-control" onKeyPress="return solonumeros(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Cota:</label>
                  <input type="text" name="cota"  value="<?php echo $ejemplar->cota;?>"  class="form-control">
                </div>
				<div class="form-group">
                  <label for="form-control">N&uacute;mero de P&aacute;g.</label>
                  <input type="text" name="paginas"  value="<?php echo $ejemplar->paginas;?>"  class="form-control" onKeyPress="return solonumeros(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Editorial</label>
				<select class="form-control select22" name="editoria" id="">
				  	<option value="0">--Selecccione--</option>
				  <?php while($reg=pg_fetch_object($ejemplar->editoriales)){?>
					<?php $i++;?>
					<option value="<?php echo $reg->id_editorial;?>"><?php echo $reg->nombre;?></option>
				  <?php }?>
				   </select>                </div>
				<div class="form-group">
                  <label for="form-control">Autor 1:</label>
                  <input type="text" name="autor1"  value="<?php echo $ejemplar->autor1;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Area:</label>
				  <select class="form-control select2" name="areas">
				  	<option value="0">--Selecccione--</option>
				  <?php while($reg=pg_fetch_object($ejemplar->categorias)){?>
					<?php $i++;?>
					<option value="<?php  echo $reg->id_area;?>"><?php echo $reg->area;?></option>
				  <?php }?>
				   </select>
                </div>
				<div class="form-group">
                  <label for="form-control">Autor 2:</label>
                  <input type="text" name="autor2"  value="<?php echo $ejemplar->autor2;?>"  class="form-control" onKeyPress="return solonumeros(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Ejemplares:</label>
				    <input type="text" name="cant_ejemplar"  value="<?php echo $ejemplar->cant_ejemplar;?>"  class="form-control"onKeyPress="return solonumeros(event)">
                </div>
				<div class="form-group">
                  <label for="form-control">Autor 3:</label>
                  <input type="text" name="autor3"  value="<?php echo $ejemplar->autor3;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Sala:</label>
				  <select class="form-control" name="sala" id="">
					<?php if (($_POST)&&( $ejemplar->sala=="Infantil")){?>
					<option value="<?php echo $ejemplar->sala;?>"><?php echo $ejemplar->sala;?></option>
					<option  value="General">General</option>
					<?php }?>
					 	 <?php if (($_POST)&&( $ejemplar->sala=="General")){?>
					<option value="<?php echo $ejemplar->sala;?>"><?php echo $ejemplar->sala;?></option>
					<option  value="Infantil">Infantil</option>
					<?php }?>
					
					<?php if (($_POST)&&( $ejemplar->sala=="0")){?>
					<option value="0">--Selecccione--</option>
                    <option value="Infantil">Infantil</option>
                    <option  value="General">General</option>
					<?php }?>
					
					<?php if(!$_POST){?>
					<option value="0">--Selecccione--</option>
					
                    <option value="Infantil">Infantil</option>
                    <option  value="General">General</option>
					<?php }?>
                  </select>
                </div>
				
			</div>
			<div class="col-sm-4">
				<div class="form-group">
                  <label for="form-control">Estatus:</label>
					<select class="form-control" name="estatus" id="Estatus">
					<?php if (($_POST)&&( $ejemplar->estatus=="Activo")){?>
					<option value="<?php echo $ejemplar->estatus;?>"><?php echo $ejemplar->estatus;?></option>
					<?php }?>
					
					<?php if (($_POST)&&( $ejemplar->estatus=="0")){?>
					<option value="0">--Selecccione--</option>
                    <option  value="Activo">Activo</option>
					<?php }?>
					<?php if(!$_POST){?>
					<option value="0">--Selecccione--</option>
                    <option  value="Activo">Activo</option>
					<?php }?>
                  </select>                </div>
			</div>
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Observaciones:</label>
                  <input type="text" name="observacion"  value="<?php echo $ejemplar->observacion;?>"  class="form-control" onKeyPress="return soloLetras(event)">
                </div>
			</div>
			<div align="right">
					<a href="ejemplares.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Registrar Ejemplar" value="Registrar Ejemplar" class="btn btn-primary" />
					</div>
			
			</div>
              </div><!-- /.box-body -->
            </form>
          </div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>
		<script>
			$(".select2").select2();
			$(".select22").select2();
		</script>