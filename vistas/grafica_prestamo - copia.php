<?php
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");
	include("./../public/plugins/fpdf/fpdf.php");
	include_once ("./../public/plugins/jpgraph/jpgraph.php");
	require_once ('./../public/plugins/jpgraph/jpgraph_bar.php');
	
	$interno = 0;
	$circulante = 0;	 
	
	$sql="select *from prestamo";
	$consulta = pg_query ($sql);
	
		while($reg=pg_fetch_object($consulta))//mostrar todos
		{	
			if($reg->tipo == "Circulante"){$interno++;}
			if($reg->tipo == "Interno"){$circulante++;}
			$total++;
		}//cierra while 
	
	
	$data=array($interno,$circulante);
	
	// Create the graph. These two calls are always required
	$graph = new Graph(600,200,"auto");	
	$graph->SetScale("textlin");
	
	// Add a drop shadow
	$graph->SetShadow();
	
	// Adjust the margin a bit to make more room for titles
	$graph->img->SetMargin(40,30,20,40);
	
	// Create a bar pot
	$bplot = new BarPlot($data);
	$graph->Add($bplot);
	
	// Setup the titles
	$graph->title->Set('');
	$graph->xaxis->title->Set("Prestamos");
	$graph->yaxis->title->Set("Total");
	
	$graph->title->SetFont(FF_FONT1,FS_BOLD);
	$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
	$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
	
	// Display the graph
	$graph -> img -> SetImgFormat('jpeg');
	$graph->Stroke("./../public/img/grafica_prestamos.jpg");
	$graph->Stroke();
?>
