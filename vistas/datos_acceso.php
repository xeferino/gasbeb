<?php include_once("./../controladores/instancias_bibliotecario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>
	<?php 
		if(isset($_GET['id']))
		$bibliotecario->datos_acceso($_GET['id']);
		
		if (isset($_POST['submit']) && $_POST['submit'] == 'editar') {	
		
				$usuario =  $_POST['usuario'];
				$clave_a =  $_POST['clave_a'];
				$clave_n =  $_POST['clave_n'];
							
				$id =  $_POST['id'];
				$user =  $_POST['user'];
				
			$bibliotecario->actualizar_datos_acceso($usuario, $clave_a, $clave_n, $user, $id);
		}
	?>
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=10; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Configuraci&oacute;n</li>
					<li class="active">Datos de Acceso</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-edit"></i> Modificar Datos de Acceso</h3>
								</div>
						<form class="form-horizontal" action="" method="post" id="formulario_usuario" role="formulario_usuario">
							<input type="hidden" name="id" value="<?php echo $bibliotecario->reg->id_bibliotecario;?>">
							<input type="hidden" name="user" value="<?php echo $bibliotecario->usuario;?>">							<input type="hidden" name="submit" value="editar" />
              <div class="box-body">
				<?php if(!$bibliotecario->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Actualize sus Datos de Acceso.
								</div>
								<?php }?>
			
								<?php if($bibliotecario->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Datos de Acceso Actualizados.
								</div>
								<?php }?>
								<?php if($bibliotecario->mensaje==2){?>
								<div class="alert alert-danger">
							<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> La Clave actual no Coincide con la Registrada en la Base de Datos.
						</div>
						<?php }?>
						
						<?php if($bibliotecario->mensaje==3){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, Cuenta de Usuario no Disponible.
						</div>
						<?php }?>
						
						<?php if($bibliotecario->mensaje==4){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
						<?php }?>
				 <div class="form-group has-feedback">
                  <label for="text" class="col-sm-2 control-label">Usuario:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="usuario"  value="<?php echo $bibliotecario->usuario;?>" placeholder="Cuenta de Usuario" >
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                </div>
                <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Clave Actual:</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="clave_a"  value="<?php echo $clave_a;?>" placeholder="Clave Actual">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Clave Nueva:</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="clave_n"  value="<?php echo $clave_n; ?>" placeholder="Clave Nueva">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                </div>
				<div align="right">
					 <a href="./" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Modificar" value="Modificar" class="btn btn-info" />
					</div>
              </div>
              <!-- /.box-body -->
                
              <!-- /.box-footer -->
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
						
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>