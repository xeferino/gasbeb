<?php include_once("./../controladores/instancias_ejemplar.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>
	<?php 
		if(isset($_GET['id']))
				{
					$ejemplar->datos_ejemplar ($_GET['id']);
				}
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'editar') {	
		
		$ejemplares = $_POST['ejemplares'];
		$ejemplares_inactivos = $_POST['ejemplares_inactivos'];
	
		$id_libro = $_POST['id_libro'];		
		
		$ejemplar->actualizar_ejemplar_cantidad($id_libro, $ejemplares, $ejemplares_inactivos);
	}
?>
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=5; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=5; include ('menu_administrador.php');
		}?>			</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Ejemplares</li>
					<li class="active">Actuaizar Ejemplar Activos</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
            <div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-book"></i> Actualizar Ejemplar Cantidad</h3> <a href="ejemplares.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Ejemplares</a>
								</div>
            <!-- /.box-header -->
            <!-- form start -->
           <form method="post" id="formulario_usuario" role="form">
			<input type="hidden" name="actividad" value="registro de cuanta de usuario" class="url">
			<input type="hidden" name="submit" value="editar" />
			<input type="hidden" name="id_libro" value="<?php echo $ejemplar->id_libro;?>">
			<input type="hidden" name="isbn_n" value="<?php echo $ejemplar->isbn;?>">
			<input type="hidden" name="cota_n" value="<?php echo $ejemplar->cota;?>">



              <div class="box-body">
			  <?php if(!$ejemplar->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Actualize los Datos del Ejemplar.
								</div>
								<?php }?>
			
								<?php if($ejemplar->mensaje==1){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, Libro no Existe.
						</div>
						<?php }?>
						
						<?php if($ejemplar->mensaje==2){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
						<?php }?>
		<div class="col-sm-12">
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Cantidad de Ejemplares:</label>
                  <input type="text" name="ejemplares" value="<?php echo $ejemplar->cant_ejemplar;?>" class="form-control">
                </div>
			</div>
			
			<div class="col-sm-4">
                <div class="form-group">
                  <label for="form-control">Cantidad de Ejemplares Inactivos:</label>
                  <input type="text" name="ejemplares_inactivos" value="<?php echo $ejemplar->cant_inactivos;?>" class="form-control">
                </div>
			</div>
			
			<div class="col-sm-4">
                <div class="form-group">
				<br style="margin-top:5px;">
<a href="ejemplares.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Actualizar Ejemplar" value="Actualizar Ejemplar" class="btn btn-primary" />                </div>
			</div>
			
			</div>
              </div><!-- /.box-body -->
            </form>
          </div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
			
		<script>
			$(".select2").select2();
		</script>
		
		<?php include('llamado_footer.php');?>