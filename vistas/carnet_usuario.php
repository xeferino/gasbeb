<?php include_once("./../controladores/instancias_usuario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php");?>
	<?php include('header.php'); ?>
	
	<?php if(isset($_GET['id']))
				{
					$usuario->datos_usuario($_GET['id']);
				}
				
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'update') {	
	
			$titulo=$_POST['titulo'];
			$autor=$_POST['autor'];
			$contenido=$_POST['contenido'];
			
			/*$imagen=$_FILES['imagen']['tmp_name']; 
			$nombre=$_FILES['imagen']['name'];*/
			
			$imagen= $_FILES['imagen']['name'];
			$ruta = $_FILES['imagen']['tmp_name']; 
			
			$id = $_POST['id'];
			
			//echo $titulo;
					
			$biografia->inicializar_datos ($titulo, $autor, $contenido, $imagen);
			$biografia->actualizar_biografia($id, $ruta);
	}
	?>
	
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=4; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=4; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Usuario</li>
					<li class="active">Carnet de Usuario</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						
					<div class="row">
							<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="glyphicon glyphicon-th-large"></i> Carnet de Usuario <a href="usuarios.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a>
            <small class="pull-right">Fecha: <?php echo date('m-d-Y');?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row">
	  <div align="center">
		  <img src="../public/img/bibliotecanacional.jpg">
		 <h3>CARNET DE 000<?php echo $usuario->carnet;?></h3>
		 	 <div class="col-md-3">
	
			</div>
		 <div class="col-md-6">
	<table width="500" border="0"  class="table table-striped">
		  <tr>
			<td>Apellidos: <?php echo $usuario->primer_apellido." ".$usuario->segundo_apellido;?></td>
			<td>Fecha Exp. <?php echo $usuario->fecha;?></td>
			</tr>
		  <tr><?php $vence = explode ('-',$usuario->fecha);?>
		  <?php $Y = $vence['0'];?>
		  <?php $m = $vence['1'];?>
		  <?php $d = $vence['2']; $Y1=$Y+1; ?>
			<td>Nombres: <?php echo $usuario->primer_nombre." ".$usuario->segundo_nombre;?></td>
			<td>Fecha Venc. <?php echo $Y1."-".$m."-".$d;?></td>
			</tr>
		  <tr>
			<td>Cedula de Identidad: <?php echo $usuario->cedula;?></td>
			<td>&nbsp;</td>
			</tr>
	</table>
</div>
<div class="col-md-3">
	
			</div>
	 </div>
	 <br>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
		<form action="carnet_usuario_pdf.php" method="post" target="_blank">
			<input type="hidden" name="id" value="<?php echo $usuario->reg->id_usuario;?>">
			  <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
				<i class="glyphicon glyphicon-file"></i> Generar PDF
			  </button>
		 </form>
        </div>
      </div>
    </section><!-- /.row -->
						</div>
						
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>