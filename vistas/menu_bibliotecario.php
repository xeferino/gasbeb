
	<section class="sidebar">
     <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
		<li class="<?php if ($menu==1){ echo "treeview active";}?>">
          <a href="./">
            <i class="glyphicon glyphicon-home"></i>
            <span>Inicio</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		<li class="<?php if ($menu==4){ echo "treeview active";}?>">
          <a href="usuarios.php">
            <i class="glyphicon glyphicon-user"></i>
            <span>Usuarios</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		<li class="<?php if ($menu==5){ echo "treeview active";}?>">
          <a href="ejemplares.php">
            <i class="glyphicon glyphicon-book"></i>
            <span>Ejemplares</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
        <li class="<?php if ($menu==2){ echo "treeview active";}?>">
          <a href="prestamos.php">
            <i class="glyphicon glyphicon-open"></i>
            <span>Pr&eacute;stamos</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		<li class="<?php if ($menu==3){ echo "treeview active";}?>">
          <a href="devoluciones.php">
            <i class="glyphicon glyphicon-save"></i>
            <span>Devoluciones</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		
		 
        <li class="<?php if ($menu==6){ echo "treeview active";}?>">
          <a href="calendario_actividades.php">
            <i class="glyphicon glyphicon-calendar"></i> <span>Actividades</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		 <li class="<?php if ($menu==7){ echo "treeview active";}?>">
          <a href="biografias.php">
            <i class="glyphicon glyphicon-modal-window"></i> <span>Biograf&iacute;as</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		 <li class="<?php if ($menu==8){ echo "treeview active";}?>">
          <a href="inmuebles.php">
            <i class="glyphicon glyphicon-compressed"></i> <span>Inmuebles</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
        </li>
		
		<li class="treeview">
        <li><a data-toggle="modal" data-target="#salir" href="#"><i class="glyphicon glyphicon-log-out text-red"></i> <span>Salir</span></a></li>
        </li>
      </ul>
    </section>
	
	
	
	
	