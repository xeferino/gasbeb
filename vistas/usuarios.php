<?php include_once("./../controladores/instancias_usuario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=4; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=4; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Usuario</li>
					<li class="active">Listado de Usuario</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listado de Usuarios</h3> <a href="registrar_usuario.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Regitrar Usuario</a>
<a href="sanciones.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Usuarios Sancionados</a>			</div>
								<?php $validar = pg_num_rows($usuario->consulta);?>
								<?php if ($validar>0){?>			
								<div class="box-body">
								<table class="table table-striped" id="dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>CI</th>
											<th>Nombre y Apellido</th>
											<th>Perfil</th>
											<th>Email</th>
											<th>Ocupaci&oacute;n</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($usuario->consulta)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->cedula;?></td>
											<td><?php echo $reg->primer_nombre." ".$reg->segundo_nombre." ".$reg->primer_apellido." ".$reg->segundo_apellido;?></td>
											<td><?php echo $reg->tipo;?></td>
											<td><?php echo $reg->correo;?></td>
											<td><?php echo $reg->ocupacion;?></td>
											<td class="center">
											<?php if ($reg->tipo=="Circulante"){?>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Detalles" data-toggle="modal" data-target="#detalles<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Ficha" data-toggle="modal" data-target="#ficha<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-credit-card"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Carnet" data-toggle="modal" data-target="#carnet<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-credit-card"></i>
											</a>
											<?php } else{?>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Detalles" data-toggle="modal" data-target="#detalles<?php echo $reg->id_usuario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>
											<?php }?>
											<div class="modal fade" id="actualizar<?php echo $reg->id_usuario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar los Datos del Usuario.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="editar_usuario.php?id=<?php echo $reg->id_usuario;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
							<div class="modal fade" id="ficha<?php echo $reg->id_usuario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Visualizar la Ficha del Usuario.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="ficha_usuario.php?id=<?php echo $reg->id_usuario;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
							<div class="modal fade" id="carnet<?php echo $reg->id_usuario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Visualizar el  Carnet del Usuario.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="carnet_usuario.php?id=<?php echo $reg->id_usuario;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							
										<div class="modal fade" id="detalles<?php echo $reg->id_usuario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b><span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Datos del Usuario</b>
			</div>
			<div class="modal-body">
			
				<section class="invoice">
					<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
									<i class="glyphicon glyphicon-th-large"></i> Datos del Usuario (<?php echo $reg->id_usuario;?>)
									<small class="pull-right">Carnet: us00<?php echo $reg->codigo_carnet;?></small>
								</h2>
							</div>
						      <div class="row invoice-info">
							  	<address> 
									<div align="justify">
										<table width="500" border="1" align="center">
				  <tr>
					<td width="229">&nbsp;<b>C&eacute;dula:</b><br>&nbsp;<?php echo $reg->cedula;?></td>
					<td width="255">&nbsp;<b>Nombres y Apellidos:</b><br>&nbsp;<?php echo $reg->primer_nombre." ".$reg->segundo_nombre.", ".$reg->primer_apellido." ".$reg->segundo_apellido;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>Sexo:</b><br>&nbsp;<?php echo $reg->sexo;?></td>
					<td width="255">&nbsp;<b>Correo:</b><br>&nbsp;<?php echo $reg->correo;?>.</td>
				  </tr>
				  <tr>
					<td width="229">&nbsp;<b>Ocupaci&oacute;n:</b><br>&nbsp;<?php echo $reg->ocupacion;?>.</td>
					<td width="255">&nbsp;<b>Programas o Misisones:</b><br>&nbsp;<?php echo $reg->programa;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>Tipo:</b><br>&nbsp;<?php echo $reg->tipo;?>.</td>
					<td width="255">&nbsp;<b>Discapacidad:</b><br>&nbsp;<?php echo $reg->discapacidad;?>.</td>
				  </tr>
				  
				  <tr>
					<td width="229">&nbsp;<b>Fecha Nacimiento:</b><br>&nbsp;<?php echo $reg->fecha_nac;?></td>
					<td width="255">&nbsp;<b>Edad:</b><br>&nbsp;<?php $fecha = date('Y-m-d');$edad = $fecha-$reg->fecha_nac;					echo $edad;?>.</td>
				  </tr>
				  <tr>
				  <tr>
					<td width="229">&nbsp;<b>Telefono Movil:</b><br>&nbsp;<?php echo $reg->telefono1;?></td>
					<td width="255">&nbsp;<b>Telefono de Habitaci&oacute;n:</b><br>&nbsp<?php echo $reg->telefono2;?>.</td>
				  </tr>
				  <tr>
				  
				  </tr>
				  <tr>
				  
					<td colspan="2">&nbsp;<b>Punto de Refencia:</b><br>&nbsp;<?php echo $reg->referencia;?>.</td>
					</tr>
					
					<tr>
					<td colspan="2">&nbsp;<b>Direcci&oacute;n 1:</b><br>&nbsp;<?php echo $reg->direccion1;?>.</td>
					</tr>
					
					<tr>
					<td colspan="2">&nbsp;<b>Direcci&oacute;n 2:</b><br>&nbsp;<?php echo $reg->direccion2;?>.</td>
					</tr>
				</table>
										
									</div>
								</address>
							  </div>
						<!-- /.col -->
					</div>
				</section>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-primary btn-primary pull-rigth" data-dismiss="modal"></span> Aceptar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
											</td>
										</tr>
										<?php }//fin de while?>
									
									</tbody>
								</table>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					<?php }else {$mensaje=1;}?>
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<?php //include('footer.php');?>
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div>
			 <div class="modal fade" id="usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Actualmente no Hay Usuarios Registrados.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cerrar</button>
			</div> 
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div><!-- ./wrapper -->
	<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
    });
    </script>
	
	<script>
    $(document).ready(function() {
	
		<?php if($mensaje==1){?>
        $('#usuario').modal({ show:true, backdrop:'static'});
		<?php }?>
    });
</script>
		
		<?php include('llamado_footer.php');?>