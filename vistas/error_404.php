<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<style>.page{background-color:#ecf0f5;}</style>

<body class="hold-transition skin-blue fixed sidebar-mini">
  <!-- Left side column. contains the logo and sidebar -->

    <!-- Main content -->
    <div class="page">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        404 Error en P&aacute;gina
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Uy! P&aacute;gina no Encontrada.</h3>

          <p>
            Ponganse en contacto con el admistrador del sistema, si el error persiste!.
          </p>

          
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content -->
<!-- ./wrapper -->
<?php include('llamado_footer.php');?>