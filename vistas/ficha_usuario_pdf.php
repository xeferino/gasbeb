<?php
if(isset($_POST['id']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");

	include("./../public/plugins/fpdf/fpdf.php");
	
		//clase para hacer el Dcocumento
		class PDF extends FPDF 
		{  
			function Header() 
			{ 		
				//Logo
				$this->Image('./../public/img/bibliotecanacional.jpg',85,10,40,15);
				//Arial bold 15
				$this->Ln(25);
				$this->SetFillColor(230,230,200);
				$this->SetFont('Arial','',10);
				$this->SetY(35); 
			} 
		
			//Pie de página 
			function Footer() 
			{ 
			   //$fecha=date("d/m/Y");
			  //Posicion
				$this->SetY(-35.8); 
				//Arial italic 8 
				$this->SetFont('Arial','I',7); 
				//Número de página 
				$this->Text(148,259.5,''); 
				$this->Text(180,259.5,'');
			} 
		
			function __construct() 
			{        
				//Llama al constructor de su clase Padre. 
				//Modificar aka segun la forma del papel del reporte 
				parent::__construct('P','mm','Letter'); 
			}
			
		}//fin de class
		
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
		
			
				$id=$_POST['id'];
				$cons_usuario=pg_query("select *from usuario where id_usuario='$id'");
				$usuario=pg_fetch_object($cons_usuario);
				
		
			$pdf=new PDF();      
			$pdf->AliasNbPages(); 
			$pdf->SetFont('Arial','',10);
			
			$j=1; 
			
			$pdf->AddPage();     
			$pdf->SetFillColor(229,229,229);
			$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(104,5,utf8_decode('Ficha de Usuario Circulante'),'','','L','');
			$pdf->Cell(73.5,5,utf8_decode($fecha_hoy),'','','R','');
			
			//DATOS DE LA DENUNCIA
			$pdf->Ln(10);
			$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(177.5,5,'Datos del Lector                                                                                                                        
			
			         USU000'.$usuario->codigo_carnet,1,'','L',1);
			
	
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 90, '', 1, '', '');
			$pdf->SetXY(22, 55); 
			$pdf->Cell(50,10,'Tipo de Lector: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,10,utf8_decode($usuario->tipo),'','','L','');
			
			$pdf->SetXY(21, 40);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 0, '', 1, '', '');
			$pdf->SetXY(120, 55); 
			$pdf->Cell(20,10,'Cedula: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,10,utf8_decode($usuario->cedula),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,15,'Nombres:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,15,utf8_decode($usuario->primer_nombre." ".$usuario->segundo_nombre),'','','L','');
			$pdf->SetFont('Arial','',10);
				$edad=date('Y-m-d');
				$suma=$edad-$usuario->fecha_nac;
			$pdf->SetFont('Arial','',10);
			$pdf->SetX(120); //Movernos a la derecha  
			$pdf->Cell(20,15,'Edad:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,15,utf8_decode($suma),'','','L','');
			$pdf->SetFont('Arial','',10);
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 65); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Apellidos: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->primer_apellido." ".$usuario->segundo_apellido),'','','L','');
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 90); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Direccion 1: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->direccion1),'','','L','');
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(90, 90); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'telefono 2: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->telefono2),'','','L','');
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 100); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Direccion 2: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->direccion2),'','','L','');
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 110); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Punto de Referencia: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->referencia),'','','L','');
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(90, 110); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Programa: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->programa),'','','L','');
			
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(90, 100); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Telefono 1: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->telefono1),'','','L','');
			
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(120, 65); //espacio entre los datos para bajarlos  
			$pdf->Cell(30,20,'Dsicapacidad: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(100,20,utf8_decode($usuario->discapacidad),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,25,'Fecha de Nacimiento:','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,25,utf8_decode($usuario->fecha_nac),'','','L','');
			$pdf->SetFont('Arial','',10);
			
			$pdf->SetX(120); //Movernos a la derecha  
			$pdf->Cell(20,25,'Sexo:','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,25,utf8_decode($usuario->sexo),'','','L','');
			$pdf->SetFont('Arial','',10);
			
			
			$j=$j+1;
			$pdf->Output();
		}
		else
		{
			include_once("error_404.php");
			//header("Location:./");
		}
?>