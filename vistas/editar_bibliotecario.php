<?php include_once("./../controladores/instancias_bibliotecario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>
<?php 
		if(isset($_GET['id']))
				{
					$bibliotecario->datos_bibliotecario($_GET['id']);
				}
				
		if (isset($_POST['submit']) && $_POST['submit'] == 'editar') {	
	
			$nombre =  $_POST['nombre'];
			$apellido =  $_POST['apellido'];
			$cedula =  $_POST['cedula'];
			$perfil =  $_POST['perfil'];
			$direccion =  $_POST['direccion'];
			$telefono =  $_POST['telefono'];
			$sexo =  $_POST['sexo'];
			$usuario =  $_POST['usuario'];
			$clave =  $_POST['clave'];
			$estatus =  $_POST['estatus'];
			$id =  $_POST['id'];
			$user =  $_POST['user'];
			$ci =  $_POST['ci'];
			
		$bibliotecario->inicializar_datos ($nombre, $apellido, $cedula, $perfil, $direccion, $telefono, $sexo, $usuario, $clave, $estatus);
		$bibliotecario->actualizar_bibliotecario($id, $ci, $user);
	}
?>
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=10; include ('menu_administrador.php');
		}?>				</aside>
							</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Bibliotecario</li>
					<li class="active">Actualizar Bibliotecario</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-cog"></i> Actualizar Bibliotecario</h3> <a href="bibliotecarios.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-th-list"></i> Listar Bibliotecarios</a>
									<a href="registrar_bibliotecario.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Bibliotecario</a>
								</div>
						<form class="form-horizontal" action="<?php //echo "./../controladores/instancias_bibliotecario.php";?>" method="post" id="formulario_usuario" role="formulario_usuario">
							<input type="hidden" name="id" value="<?php echo $bibliotecario->reg->id_bibliotecario;?>">
							<input type="hidden" name="user" value="<?php echo $bibliotecario->usuario;?>">
							<input type="hidden" name="ci" value="<?php echo $bibliotecario->cedula;?>">
							<input type="hidden" name="submit" value="editar" />
              <div class="box-body">
				<?php if(!$bibliotecario->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Actualize Los Datos del Bibliotecario.
								</div>
								<?php }?>
			
								<?php if($bibliotecario->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Datos Actualizados Satisfactoriamente.
								</div>
								<?php }?>
								<?php if($bibliotecario->mensaje==2){?>
								<div class="alert alert-danger">
							<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> La Cuenta de Usuario Existe.
						</div>
						<?php }?>
						
						<?php if($bibliotecario->mensaje==3){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Disculpe, C&eacute;dula De Identidad no Disponible.
						</div>
						<?php }?>
						
						<?php if($bibliotecario->mensaje==4){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos en Blancos, Verifique.
						</div>
						<?php }?>
				<div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">C&eacute;dula:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="cedula"  value="<?php echo $bibliotecario->cedula;?>" id="Nombre" placeholder="C&eacute;dula de Identidad" onKeyPress="return solonumeros(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				
               <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Nombre:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control"  value="<?php echo $bibliotecario->nombre;?>" name="nombre" placeholder="Nombre de Bibliotecario" onKeyPress="return soloLetras(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Apellido:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="apellido"  value="<?php echo $bibliotecario->apellido;?>" placeholder="Apellido de Bibliotecario" onKeyPress="return soloLetras(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Direcci&oacute;n:</label>

                  <div class="col-sm-10">
				   	<textarea name="direccion"  value="" class="form-control" cols="" rows="" placeholder="Direcci&oacute;n de Bibliotecario" onKeyPress="return soloLetras(event)"><?php echo $bibliotecario->direccion;?></textarea>
                    <!--<input type="text" class="form-control" name="apellido" id="Apellido" placeholder="Apellido de Usuario">-->
					<span class="help-block"></span>
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				
				<div class="form-group has-feedback">
                  <label for="inputEmail3" class="col-sm-2 control-label">Telefono:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="telefono"  value="<?php echo $bibliotecario->telefono;?>"placeholder="Telefono de Bibliotecario" onKeyPress="return solonumeros(event)">
					<!--<span class="glyphicon glyphicon-pencil form-control-feedback"></span>-->
                  </div>
                </div>
				
				<div class="form-group">
                  <label class="col-sm-2 control-label">Sexo:</label>

                  <div class="col-sm-10">
				  <!--<input type="text" class="form-control" name="perfil" id="perfil" placeholder="Perfil de Usuario">-->
                    <select class="form-control" name="sexo" id="sexo">
					<?php if ($bibliotecario->sexo=="Masculino"){?>
					<option value="<?php echo $bibliotecario->sexo;?>"><?php echo $bibliotecario->sexo;?></option>
					<option  value="Femenino">Femenino</option>
					<?php }?>
					 	 <?php if ($bibliotecario->sexo=="Femenino"){?>
					<option value="<?php echo $bibliotecario->sexo;?>"><?php echo $bibliotecario->sexo;?></option>
					<option  value="Masculino">Masculino</option>
					<?php }?>
                  </select>
                  </div>
                </div>
				
				<div class="form-group">
                  <label class="col-sm-2 control-label">Perfil:</label>

                  <div class="col-sm-10">
				  <!--<input type="text" class="form-control" name="perfil" id="perfil" placeholder="Perfil de Usuario">-->
                    <select class="form-control" name="perfil" id="Perfil">
					<?php if ($bibliotecario->perfil=="Administrador"){?>
					<option value="<?php echo $bibliotecario->perfil;?>"><?php echo $bibliotecario->perfil;?></option>
					<option  value="Bibliotecario">Bibliotecario</option>
					<?php }?>
					 	 <?php if ($bibliotecario->perfil=="Bibliotecario"){?>
					<option value="<?php echo $bibliotecario->perfil;?>"><?php echo $bibliotecario->perfil;?></option>
					<option  value="Administrador">Administrador</option>
					<?php }?>
                  </select>
                  </div>
                </div>
				<div class="form-group">
                  <label class="col-sm-2 control-label">Estatus</label>

                  <div class="col-sm-10">
                    <!--<input type="text" class="form-control" name="estatus" id="estatus" placeholder="Estatus de Usuario">-->
                    <select class="form-control" name="estatus" id="Estatus">
					<?php if ($bibliotecario->estatus=="Activo"){?>
					<option value="<?php echo $bibliotecario->estatus;?>"><?php echo $bibliotecario->estatus;?></option>
					<option  value="Inactivo">Inactivo</option>
					<?php }?>
					 	 <?php if ($bibliotecario->estatus=="Inactivo"){?>
					<option value="<?php echo $bibliotecario->estatus;?>"><?php echo $bibliotecario->estatus;?></option>
					<option  value="Activo">Activo</option>
					<?php }?>
                  </select>
                  </div>
                </div>
				 <div class="form-group has-feedback">
                  <label for="text" class="col-sm-2 control-label">Usuario:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="usuario"  value="<?php echo $bibliotecario->usuario;?>" placeholder="Cuenta de Usuario">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                </div>
                <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Clave:</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="clave"  value="<?php echo $clave;?>" placeholder="Clave de Usuario">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                </div>
				<div align="right">
					 <a href="bibliotecarios.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Actualizar Bibliotecario" value="Actualizar Bibliotecario" class="btn btn-primary" />
					</div>
              </div>
              <!-- /.box-body -->
                
              <!-- /.box-footer -->
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>