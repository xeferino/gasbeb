<?php include_once("./../controladores/instancias_bibliotecario.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=10; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Configuraci&oacute;n</li>
					<li class="active">Bibliotecarios</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listar Bibliotecarios</h3> <a href="registrar_bibliotecario.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Registrar Bibliotecario</a> <a href="auditoria.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-eye-open"></i> Auditoria</a> <a href="restore_backup.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-hdd"></i> Data Base</a>
								</div>
								<?php //if(!$bibliotecario->verificacion==0){?>
								<div class="box-body">
								<table class="table table-striped" id="dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre y Apellido</th>
											<th>Perfil</th>
											<th>Estatus</th>
											<th>Usuario</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>	
								<?php $i=0;?>			
								<?php while($reg=pg_fetch_object($bibliotecario->consulta)){?>
								<?php $i++;?>				
										<tr>
											<td><?php echo $i;?></td>
											<td><?php echo $reg->nombre." ".$reg->apellido;?></td>
											<td><?php echo $reg->tipo;?></td>
											<td><?php if(($reg->estatus=='Activo')){?><span class="label label-success"><?php echo $reg->estatus;?></span><?php }else {?><span class="label label-danger"><?php echo $reg->estatus;?></span><?php } ?> </td>
											<td><?php echo $reg->cuenta;?></td>
											<td class="center">
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic actualizar" title="Editar" data-toggle="modal" data-target="#actualizar<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" title="Detalles" data-toggle="modal" data-target="#detalles<?php echo $reg->id_bibliotecario;?>" href="">
											<i class="glyphicon glyphicon-user"></i>
											</a>
											<div class="modal fade" id="actualizar<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Actualizar los Datos del Bibliotecario.
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="editar_bibliotecario.php?id=<?php echo $reg->id_bibliotecario;?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
											<?php if($reg->estatus=='Activo'){?>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" data-toggle="modal" data-target="#confirma<?php echo $reg->id_bibliotecario;?>" title="Desactivar Usuario" href="">
											<i class="glyphicon glyphicon-ok"></i>
											</a>
											<?php }?>
											
											<?php if($reg->estatus=='Inactivo'){?>
											<a class="mb-xs mt-xs mr-xs btn btn-sm btn-primary modal-basic" data-toggle="modal" data-target="#confirma<?php echo $reg->id_bibliotecario;?>" title="Activar Usuario" href="">
											<i class="glyphicon glyphicon-ban-circle"></i>
											</a>
											<?php }?>
											
											<div class="modal fade" id="confirma<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b>Alerta</b>
			</div>
			<div class="modal-body">
			<?php if($reg->estatus=='Activo'){?>
			<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desactivar Acceso Temporalmente.
			<?php }?>
			<?php if($reg->estatus=='Inactivo'){?>
				<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Desea Activar la Cuenta de Usuario.
			<?php }?>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-default btn-default pull-rigth" data-dismiss="modal"></span> Cancelar</button>
				<a class="btn btn-primary btn-primary pull-rigth" href="desactivar_bibliotecario.php?id=<?php echo $reg->id_bibliotecario; ?>">Confirmar</a>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
							<div class="modal fade" id="detalles<?php echo $reg->id_bibliotecario;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header titulo_panel" style="background:#f6f6f6; border-bottom: 1px solid #DADADA;">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<b><span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Datos del Bibliotecario</b>
			</div>
			<div class="modal-body">
				<section class="invoice">
					<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
									<i class="glyphicon glyphicon-th-large"></i> Datos de Perfil
									<small class="pull-right">Fecha: <?php echo date('m-d-Y');?></small>
								</h2>
							</div>
						      <div class="row invoice-info">
							  	<address> 
									<div align="justify">
										<table width="500" border="1" align="center">
				  <tr>
					<td width="229">&nbsp;<b>C&eacute;dula:</b><br>&nbsp;<?php echo $reg->cedula;?></td>
					<td width="255">&nbsp;<b>Nombre y Apellido:</b><br>&nbsp;<?php echo $reg->nombre." ".$reg->apellido;?>.</td>
				  </tr>
				  
				 <tr>
					<td width="229">&nbsp;<b>Perfil:</b><br>&nbsp;<?php echo $reg->tipo;?></td>
					<td width="255">&nbsp;<b>Telefono:</b><br>&nbsp;<?php echo $reg->telefono;?>.</td>
				  </tr>
				    
				 <tr>
					<td width="229">&nbsp;<b>Cueta de Usuario:</b><br>&nbsp;<?php echo $reg->cuenta;?></td>
					<td width="255">&nbsp;<b>Estatus:</b>&nbsp;<?php if($reg->estatus=="Activo"){?><span class="label label-success"><?php echo $reg->estatus;?></span><?php } else {?><span class="label label-danger"><?php echo $reg->estatus;?></span><?php }?></td>
				  </tr>
				</table>
										
									</div>
								</address>
							  </div>
						<!-- /.col -->
					</div>
				</section>
			</div>
			<div class="modal-footer">
				<button type="submit"  class="cerrar btn btn-primary btn-primary pull-rigth" data-dismiss="modal"></span> Aceptar</button>
			</div>  
		</div>  
	</div>  
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
											</td>
										</tr>
										<?php }//fin de while?>
									
									</tbody>
								</table>
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
						<?php //}?>

								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
			
			<?php //include('footer.php');?>
			
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[5]}]
        });
    });
    </script>
		
		<?php include('llamado_footer.php');?>