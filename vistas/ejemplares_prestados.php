<?php include_once("./../controladores/instancias_prestamo.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
<?php include('header.php'); ?>	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
			<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=2; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=2; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Prestamos</li>
					<li class="active">Ejemplares Prestados</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-th-list"></i> Listado de Ejemplares Prestados</h3>
<a href="devoluciones.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-save"></i> Devoluciones</a>
<a href="prestamos.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-share-alt"></i> Regresar</a>
								</div>
								<div class="box-body">
									<div class="col-xs-12 table-responsive">
		
          <table class="table table-striped" id="dataTables-example">
            <thead>
            <tr>
			  <th>ID</th>
			  <th>ISBN</th>
              <th>Titulo</th>
			  <th>&Aacute;rea</th>
              <th>Disponibles</th>
              <th>Prestados</th>
			  <th>Total</th>
            </tr>
            </thead>
            <tbody>
	<?php $i=0;?>			
	<?php while($result=pg_fetch_array($prestamo->ejemplares)){?>
	<?php $i++;?>
	<?php $id = $result['id_libro'];?>
	
	<?php $descontar = pg_query("SELECT count(*) from prestamo, prestamo_libro, libro 
	                             where prestamo.id_prestamo=prestamo_libro.id_prestamo and libro.id_libro=prestamo_libro.id_libro 
								 and prestamo_libro.prestamo_estatus!='Retornado' and prestamo_libro.id_libro='$id'");
		
						$row=pg_fetch_array($descontar);
						$cant_prestado=$row['0'];
						$cantidad=$result['ejemplares']-$result['cant_inactivos'];
						$disponibles= $cantidad-$cant_prestado;
	?>
            <tr>
			
			  <td><?php echo $i;?></td>
			  <td><?php echo $result['isbn'];?></td>
              <td><?php echo $result['titulo'];?></td>
			  <td><?php echo $result['area'];?></td>
              <td><span class="label label-success"><?php echo $disponibles;?></span></td>
              <td><?php if ($cant_prestado==0){echo '<span class="label label-info">---</span>';}else {echo '<span class="label label-danger">'.$cant_prestado;}?></span></td>
              <td><span class="label label-warning"><?php echo $cantidad;?></span></td>
			  </td>
            </tr>
	<?php }?>
            </tbody>
          </table>
        </div>
									</div>
								</address>
							  </div>
						<!-- /.col -->
					</div>
				</section>
			</div>			
								 <!-- /.box-body -->
								</div><!-- /.box-primary -->
								<!-- /.footer -->
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
						
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
			<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
    });
    </script>
		
		<?php include('llamado_footer.php');?>