<?php include_once("./../controladores/instancias_ejemplar.php"); ?>
<?php include_once("./../controladores/motor.php"); ?>
<?php include_once("./../controladores/seguridad.php"); ?>
	<?php include('header.php'); ?>
	
<body class="hold-transition skin-blue fixed sidebar-mini">
			<div class="wrapper">
<header class="main-header">

    <!-- Logo -->
    <a href="./" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>GA</b>B</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>GASBEB</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="datos_acceso.php?id=<?php echo $_SESSION['conectado'];?>" class="dropdown-toggle">
              <i class="glyphicon glyphicon-user"></i><strong><?php echo $_SESSION['perfil']?>: <?php echo $_SESSION['nombre']." ".$_SESSION['apellido'];?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i><strong><?php echo "Cariaco,  "."$dia"."  de  "."$mes"."  del  ".$ano;?></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i><strong><script>inicio()</script>
</strong>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>			<!-- Left side column. contains the logo and sidebar -->
				<aside class="main-sidebar">
<?php if($_SESSION['perfil']=='Bibliotecario'){
			$menu=5; include ('menu_bibliotecario.php');
		}
		else{ 
			$menu=5; include ('menu_administrador.php');
		}?>				</aside>
			
			<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
				<!-- Content Header (Page header) -->
					<section class="content-header">
					<h1>Biblioteca<small>&quot;Eduardo Blanco&quot;</small></h1>
					<ol class="breadcrumb">
					<li><i class="fa fa-user"></i> Categorias</li>
					<li class="active">Registrar Categoria</li>
					</ol>
					</section>
				<!-- Main seccion -->
					<section class="content">
					<!-- Info boxes -->
						<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- fix for small devices only -->
						<div class="clearfix visible-sm-block"></div>
						
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						<div class="col-md-3 col-sm-6 col-xs-12"></div>
						<!-- /.col -->
						</div>
					<!-- /.row -->
					
						<div class="row">
							<div class="col-md-12">
							<!-- /.box-body -->
							<div class="box">
								<div class="box-header with-border">
									<h3 class="box-title"><i class="glyphicon glyphicon-book"></i> Registrar Categoria</h3>
									<a href="areas.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Categorias</a>
									<a href="ejemplares.php" class="mb-xs mt-xs mr-xs btn btn-default"><i class="glyphicon glyphicon-ok-circle"></i> Ejemplares</a>
								</div>
						<form class="form-horizontal" action="" method="post" id="formulario_usuario" role="formulario_usuario">
							<input type="hidden" name="id" value="<?php echo $bibliotecario->reg->id_bibliotecario;?>">
							<input type="hidden" name="submit" value="area" />
              <div class="box-body">
				<?php if(!$ejemplar->mensaje){?>
			  <div class="alerta alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Por favor, Ingrese Los Datos del Nuevo Registro.
								</div>
								<?php }?>
			
								<?php if($ejemplar->mensaje==1){?>
								<div class="alert alert-success">
							<span class="glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Categoria Registrada Satisfactoriamente.
								</div>
								<?php }?>
								<?php if($ejemplar->mensaje==2){?>
								<div class="alert alert-danger">
							<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> Nombre de Categoria no Disponible.
						</div>
						<?php }?>
						
						<?php if($ejemplar->mensaje==3){?>
						<div class="alert alert-danger">
							<span class="glyphicon glyphicon-remove-sign"></span> <strong>Informaci&oacute;n:</strong> Codigo de Categoria no Disponible.
						</div>
						<?php }?>
						
						<?php if($ejemplar->mensaje==4){?>
						<div class="alert alert-warning">
							<span class="glyphicon glyphicon-question-sign"></span> <strong>Informaci&oacute;n:</strong> Existen Campos Obligatorios en Blancos, por Favor Verifique.
						</div>
						<?php }?>
				 <div class="form-group has-feedback">
                  <label for="text" class="col-sm-2 control-label">Codigo:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="codigo"  value="<?php echo $ejemplar->codigo;?>" onKeyPress="return solonumeros(event)">
					
                  </div>
                </div>
                <div class="form-group has-feedback">
                  <label class="col-sm-2 control-label">Nombre:</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="area"  value="<?php echo $ejemplar->area;?>" onKeyPress="return soloLetras(event)">
                  </div>
                </div>
				
				<div align="right">
					<a href="areas.php" class="mb-xs mt-xs mr-xs btn btn-default">Cancelar</a>
					<input type="submit" name="Registrar Categoria" value="Registrar Categoria" class="btn btn-primary" />
					</div>
              </div>
              <!-- /.box-body -->
                
              <!-- /.box-footer -->
            </form>
								
							</div><!-- /.col -->
						</div><!-- /.row -->
						</div>
					
						<!-- Main row -->
						<div class="row">
							<!-- Left col -->
							<div class="col-md-8"></div><!-- /.col -->
							
							<div class="row">
								<div class="col-md-6"></div><!-- /.col -->
							</div><!-- /.row -->
							
							<div class="col-md-6"></div><!-- /.col -->
							<div class="col-md-4"></div><!-- /.col -->
						</div><!-- /.row -->
					</section>
				<!-- /.content -->
				</div><!-- /.content-wrapper -->
						
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
			
			</div><!-- ./wrapper -->
		
		<?php include('llamado_footer.php');?>