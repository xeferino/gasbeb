<?php
if(isset($_POST['id']))
{
	include_once("./../modelos/conexion.php");
	include_once("./../controladores/seguridad.php");

	include("./../public/plugins/fpdf/fpdf.php");
	
		//clase para hacer el Dcocumento
		class PDF extends FPDF 
		{  
			function Header() 
			{ 		
				//Logo
				$this->Image('./../public/img/bibliotecanacional.jpg',85,10,40,15);
				//Arial bold 15
				$this->Ln(25);
				$this->SetFillColor(230,230,200);
				$this->SetFont('Arial','',10);
				$this->SetY(35); 
			} 
		
			//Pie de página 
			function Footer() 
			{ 
			   //$fecha=date("d/m/Y");
			  //Posicion
				$this->SetY(-35.8); 
				//Arial italic 8 
				$this->SetFont('Arial','I',7); 
				//Número de página 
				$this->Text(148,259.5,''); 
				$this->Text(180,259.5,'');
			} 
		
			function __construct() 
			{        
				//Llama al constructor de su clase Padre. 
				//Modificar aka segun la forma del papel del reporte 
				parent::__construct('P','mm','Letter'); 
			}
			
		}//fin de class
		
			$mes= date("m");
			$dia= date("d");
			$anio= date("Y");
			if($mes == 1){ $mes = "Enero";}
			if($mes == 2){ $mes = "Febrero";}
			if($mes == 3){ $mes = "Marzo";}
			if($mes == 4){ $mes = "Abril";}
			if($mes == 5){ $mes = "Mayo";}
			if($mes == 6){ $mes = "Junio";}
			if($mes == 7){ $mes = "Julio";}
			if($mes == 8){ $mes = "Agosto";}
			if($mes == 9){ $mes = "Septiembre";}
			if($mes == 10){ $mes = "Octubre";}
			if($mes == 11){ $mes = "Noviembre";}
			if($mes == 12){ $mes = "Diciembre";}
			
			$fecha_hoy="Cariaco, ".$dia." de ".$mes." del ".$anio;
		
			
				$id=$_POST['id'];
				$cons_usuario=pg_query("select *from usuario where id_usuario='$id'");
				$usuario=pg_fetch_object($cons_usuario);
		
			$pdf=new PDF();      
			$pdf->AliasNbPages(); 
			$pdf->SetFont('Arial','B',10);
			
			$j=1; 
			
			$pdf->AddPage();     
			$pdf->SetFillColor(229,229,229);
			$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(104,5,utf8_decode('Ficha de Usuario Circulante'),'','','L','');
			$pdf->Cell(73.5,5,utf8_decode($fecha_hoy),'','','R','');
			
			//DATOS DE LA DENUNCIA
			$pdf->Ln(10);
			$pdf->Cell(11,5,'',0); //Movernos a la derecha   
			$pdf->Cell(177.5,5,'Datos del Lector',1,'','L',1);
	
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 55); 
			$pdf->Cell(50,10,'Tipo de Lector: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,10,utf8_decode($usuario->tipo),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,15,'Nombres:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,15,utf8_decode($usuario->primer_nombre." ".$usuario->segundo_nombre),'','','L','');
			$pdf->SetFont('Arial','B',10);
			
			$pdf->SetXY(21, 50);  //Movernos a la derecha  
			$pdf->MultiCell(177.5, 40, '', 1, '', '');
			$pdf->SetXY(22, 65); //espacio entre los datos para bajarlos  
			$pdf->Cell(50,20,'Apellidos: ','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(127.5,20,utf8_decode($usuario->primer_apellido." ".$usuario->segundo_apellido),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,25,'Fecha de Nacimiento:','','','L','');
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(25,25,utf8_decode($usuario->fecha_nac),'','','L','');
			$pdf->SetFont('Arial','B',10);
			
			/*if($valido==1)
			{
			while($denunciante=pg_fetch_object($datos_personales))
			{
			//DATOS DE DENUNCIANTE
			$pdf->SetFont('Arial','B',10);
			$pdf->Ln(10);
			$pdf->SetXY(21, 95); 
			$pdf->Cell(177.5,5,'DATOS DEL DENUNCIANTE',1,'','L',1);
			$pdf->SetXY(21, 100);
			$pdf->MultiCell(177.5, 45, '', 1, '', '');
			$pdf->SetXY(22, 105);
			$pdf->SetFont('Arial','B',10);
			
			$pdf->Cell(50,5,'NOMBRES Y APELLIDOS:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($denunciante->nombre1." ".$denunciante->apellido1),'','
			','L','');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,5,utf8_decode('CÉDULA DE IDENTIDAD:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($denunciante->cedula_p),'','','L','');
			$pdf->SetFont('Arial','B',10);
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('FECHA DE NACIMIENTO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciante->fecha_nac,'','','L','');
			
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			
			$pdf->Cell(50,5,utf8_decode('PROFESIÓN:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciante->profesion,'','','L','');
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('DIRECCIÓN:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,utf8_decode($denunciante->direccion_p),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			$pdf->Cell(50,5,utf8_decode('TELEFONO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciante->telefono,'','','L','');
			}
			}
			
			if($valido1==1)
			{
			while($denunciado=pg_fetch_object($datos_personales1))
			{		
			//DATOS DEL DENUNCIADO
			$pdf->SetFont('Arial','B',10);
			$pdf->Ln(10);
			$pdf->SetXY(21, 150); 
			$pdf->Cell(177.5,5,'DATOS DEL DENUNCIADO',1,'','L',1);
			$pdf->SetXY(21, 150);
			$pdf->MultiCell(177.5, 45, '', 1, '', '');
			$pdf->SetXY(22, 160);
			$pdf->SetFont('Arial','B',10);
			
			$pdf->Cell(50,5,'NOMBRES Y APELLIDOS:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($denunciado->nombre1." ".$denunciado->apellido1),'','
			','L','');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,5,utf8_decode('CÉDULA DE IDENTIDAD:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($denunciado->cedula_p),'','','L','');
			$pdf->SetFont('Arial','B',10);
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('FECHA DE NACIMIENTO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciado->fecha_nac,'','','L','');
			
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			
			$pdf->Cell(50,5,utf8_decode('PROFESIÓN:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciado->profesion,'','','L','');
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('DIRECCIÓN:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,utf8_decode($denunciado->direccion_p),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			$pdf->Cell(50,5,utf8_decode('TELEFONO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$denunciado->telefono,'','','L','');
			}
			}
			
			if($valido2==1)
			{
			while($testigo=pg_fetch_object($datos_personales2))
			{
			
			//DATOS DE TESTIGO
			$pdf->SetFont('Arial','B',10);
			$pdf->Ln(10);
			$pdf->SetXY(21, 200); 
			$pdf->Cell(177.5,5,'DATOS DEL TESTIGO',1,'','L',1);
			$pdf->SetXY(21, 200);
			$pdf->MultiCell(177.5, 45, '', 1, '', '');
			$pdf->SetXY(22, 210);
			$pdf->SetFont('Arial','B',10);
			
			$pdf->Cell(50,5,'NOMBRES Y APELLIDOS:','','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($testigo->nombre1." ".$testigo->apellido1),'','
			','L','');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha  
			$pdf->Cell(50,5,utf8_decode('CÉDULA DE IDENTIDAD:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(25,5,utf8_decode($testigo->cedula_p),'','','L','');
			$pdf->SetFont('Arial','B',10);
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('FECHA DE NACIMIENTO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$testigo->fecha_nac,'','','L','');
			
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			
			$pdf->Cell(50,5,utf8_decode('DIRECCIÓN:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$testigo->direccion_p,'','','L','');
	
			$pdf->Ln(5);
			$pdf->SetFont('Arial','B',10);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->Cell(50,5,utf8_decode('TELEFONO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,utf8_decode($testigo->telefono),'','','L','');
			
			$pdf->Ln(5);
			$pdf->SetX(22); //Movernos a la derecha
			$pdf->SetFont('Arial','B',10); 
			$pdf->Cell(50,5,utf8_decode('TESTIMONIO:'),'','','L',''); $pdf->SetFont('Arial','',10);
			$pdf->Cell(60,5,$testigo->testimonio,'','','L','');
			
			}
			}*/
			
			$j=$j+1;
			$pdf->Output();
		}
		else
		{
			include_once("error_404.php");
			//header("Location:./");
		}
?>