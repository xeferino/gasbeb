<?php include_once("controladores/iniciar_sesion.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GASBEB|Login</title>
    <link rel="shortcut icon" type="image/x-icon" href="public/img/minilogo.ico">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="public/bootstrap/css/bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">

</head>
<body class="hold-transition login-page" style="background-image:url(public/img/fondo.jpg);">
<div class="login-box" style=" width:400px;">
  <div class="login-logo" style="font-size:24px;">
  		    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
	<p align="center"> 
	<img src="public/img/bibliotecanacional.jpg" width="150">
	</p>
	<p align="center" style="font-size:18px;"> 
	 <b>Biblioteca &quot;Eduardo Blanco&quot;</b>
	</p>

	<?php if(!$bibliotecario->mensaje){?>
	<div class="alerta alert alert-info" style="display:block;">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese sus Datos de Acceso.</strong> 
								</div>
	<?php }?>
	<?php if($bibliotecario->mensaje==1){?>
	<div class="alerta alert alert-warning" style="display:block;">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Usuario Temporalmente Inactivo.
								</div>
	<?php }?>
	<?php if($bibliotecario->mensaje==2){?>
	<div class="alerta alert alert-danger" style="display:block;">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Verifique, Usuario o Clave Invalidos.
								</div>
	<?php }?>
	
	<?php if($bibliotecario->mensaje==3){?>
	<div class="alerta alert alert-warning" style="display:block;">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Informaci&oacute;n:</strong> Verifique, Campos en Blancos.
								</div>
	<?php }?>
    <form action="" method="post">
	<input type="hidden" name="submit" value="login" />
      <div class="form-group has-feedback">
	  <label for="">Usuario:</label>
        <input type="text" name="usuario" value="<?php echo $bibliotecario->usuario;?>" class="form-control" placeholder="Usuario">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
	  <label for="">Contrase&ntilde;a:</label>
        <input type="password" name="clave" value="<?php echo $bibliotecario->clave;?>" class="form-control" placeholder="Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary pull-right">
				<i class="glyphicon glyphicon-log-in"></i> <b>Ingresar</b>
			  </button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
    <!-- /.social-auth-links -->

    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="public/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="public/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
